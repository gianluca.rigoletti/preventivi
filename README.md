## Note

### Cose da fare
* [x] In dettaglio oreventivoo mettere un bottone per andare al riepilogo. 
* [x] Aggiungere l'opportunità di cambiare il tipo e il numero di stanza per ogni
    dettaglio riga in ges_dettaglio_ordini.php
* Definire quali dati inserire nell'"oggetto" preventivo:
    * Descrizione 
    * Dati intestatario
    * Note
    * Data inizio e fine
    * Se confermato == sì si ciamerà programma di viaggio altrimenti preventivo
    * Righe con gli stessi dati di vis_dettaglio_ordini.php
    * Il prezzo di ogni riga può o può non essere visualizzato in base ad un 
    parametro arbitrario
    * In fondo il prezzo con markup
    * Per ogni città va preso il campo atlante e visualizzato sotto la riga del
    dettaglio che ha proprio quella città
    * I servizi liberi vanno sotto le città che hanno la stessa data
    * Se il servizio libero è di tipo `macchina` (definirsi un id fisso nel db),
    allora devo inserire le informazioni su come raggiungere le città adiacenti
    usando la macchina. Per farlo, bisogna controllare e usare il valore del campo 
    `atlante` nella tabella `citta_destinazione`, verificando una corrispondenza
    con `idcittafrom` e `idcittato` delle due città adiacenti.
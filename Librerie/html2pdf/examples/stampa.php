<?php
/**
 * HTML2PDF Librairy - example
 *
 * HTML => PDF convertor
 * distributed under the LGPL License
 *
 * @author      Laurent MINGUET <webmaster@html2pdf.fr>
 *
 * isset($_GET['vuehtml']) is not mandatory
 * it allow to display the result in the HTML format
 */
    // get the HTML
    ob_start();
?>
<page>
    <div style="rotate: 90; position: absolute; width: 50mm; height: 20mm; left: 90mm; top: 90mm; font-style: italic; font-weight: normal; text-align: center; font-size: 7mm;">
        <barcode type="C39E" value="9876"  style="width:30mm; height:8mm; font-size: 2mm"></barcode>
    </div>
    <div style="position: absolute; width: 100mm; height: 20mm; left: 13mm; top: 25mm; font-style: italic; font-weight: normal; text-align: center; font-size: 9mm;">
         Francisco Humberto
    </div>
    <div style="position: absolute; width: 100mm; height: 20mm; left: 13mm; top: 37mm; font-style: italic; font-weight: normal; text-align: center; font-size: 9mm;">          
         Maffei
    </div>
    <div style="position: absolute; width: 100mm; height: 20mm; left: 13mm; top: 52mm; font-style: italic; font-weight: normal; text-align: center; font-size: 6mm;">
         Lisbon  - Portugal
    </div>
    <div style="position: absolute; width: 85mm; height: 10mm; left:126mm; top: 126mm; font-style: italic; font-weight: normal; text-align: center; font-size: 6mm;">
         Francisco Humberto Maffei
    </div>    
    <div style="position: absolute; width: 110mm; height: 15mm; left:90mm; top: 217mm; font-style: italic; font-weight: normal; text-align: center; font-size: 9mm;">
         Francisco Humberto Maffei
    </div>    

</page>
<?php
     $content = ob_get_clean();

    // convert
    require_once(dirname(__FILE__).'/../html2pdf.class.php');
    try
    {
        $html2pdf = new HTML2PDF('P', 'A4', 'it', true, 'UTF-8', 0);
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content, isset($_GET['vuehtml']));
        $html2pdf->Output('stampa.pdf');
    }
    catch(HTML2PDF_exception $e) {
        echo $e;
        exit;
    }


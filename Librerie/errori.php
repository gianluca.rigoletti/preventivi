<?php


class errori
{

var $valori = array( 0=>"Sto Elaborando il file %1", 
                     1=>"File senza settori. File Scartato!!!",
                     2=>"Il Campo Quantit&agrave; deve essere un numero intero riga %1",
                     3=>"Il Campo Settore deve essere un numero intero riga %1",
                     4=>"Attenzione Settore vuoto : %1",
                     5=>"Attenzione Settore doppio : %1",
                     6=>"Attenzione Manca il Settore Iniziale" ,
                     7=>"Attenzione Settore Gi&agrave; caricato nel db %1. File Scartato!!!" ,
                     8=>"Lunghezza articolo non codificata nelle impostazioni. Elaborazione Interrotta!!!" ,
                     9=>"Errore Inserimento nel db %1. File Scartato!!!",
                     10=>"Errore Cancellazione db %1.",
                     11=>"Errore Estrazione Dati db %1.",
                     12=>"Manca il settore %1.",
                     13=>"Settore %1 con numero di articoli anomalo %2",
                     14=>"Inventario controllato! Non sono stati trovati errori",
                     15=>"Inventario controllato! Sono presenti alcuni warning",
                     16=>"Settore %1 non previsto.",
                     17=>"File Caricato %1.",
                     18=>"File Scartato %1.",
                     19=>"Attenzione File %1 appartenente al personale Kiabi.",
                     20=>"Attenzione File %1 NON appartenente al personale Kiabi.",
                     21=>"Controllo Settore %1",
                     22=>"Settore %1 verificato con successo!",
                     23=>"Quantit&agrave; diversa tra dati inventario ( %1 ) e dati lettura personale kiabi ( %2 )!",
                     24=>"Settore non ancora caricato nell'inventario, il controllo verr&agrave; rieseguito in seguito!",
                     25=>"Articolo di troppo in inventario %1!",
                     26=>"Articolo mancante in inventario %1!",
                     27=>"Errore Aggiornamento Dati db %1.",
                     28=>"Settore %1 verificato con esito negativo!",
                    );

     function get_errore ($id,$val1 =null,$val2 =null,$val3 =null,$val4 =null,$val5 =null,$val6 =null) {
     
              $errore = "";
              if ( isset($this->valori[$id])) $errore = $this->valori[$id];
              else  $errore = "Errore non codificato ". $id;
              
              $errore = str_replace('%1',$val1,$errore);
              $errore = str_replace('%2',$val2,$errore);
              $errore = str_replace('%3',$val3,$errore);
              $errore = str_replace('%4',$val4,$errore);
              $errore = str_replace('%5',$val5,$errore);
              $errore = str_replace('%6',$val6,$errore);
              
              return $errore;
     }
    
}

?>
<?php

  require_once("configurazione.php");



class files
{
  var $tipo = "I";
  var $dim_x = 100;
  var $dim_y = 100;
  var $perc_vis = 100;
  var $directory = "";
  var $dir_imm;// = "../../Immagini/";
  var $dir_file;// = "../../Download/";
  var $des = " Immagine ";

    function __construct()
    {
        // uso dati configurazione
       global  $conf_dir_imm,$conf_dir_file;
       $this->dir_imm = $conf_dir_imm;
       $this->dir_file = $conf_dir_file;   
                         
    }  
  
  function set_tipo($t) {
     if ($t != null && ($t == "F" || $t == "I")) $this->tipo = $t;

     if ($this->tipo == "I") {
         $this->dir = $this->dir_imm.$this->directory;
         $this->des = " Immagine ";
     }
     else {
         $this->dir = $this->dir_file.$this->directory;
         $this->des = " Allegato ";
     }     
  }
  
  function set_dim($x,$y) {
      $this->dim_x = $x;
      $this->dim_y = $y;
  }

  function set_perc($p) {
      if ($p != null && ($p > 0 && $p < 100)) $this->perc_vis = $p;
  }
  function set_directory($d) {
  
      if ($d != null && $d != "" ) $d .= "/";
      $this->directory = $d;

     if ($this->tipo == "I") {
         $this->dir = $this->dir_imm.$this->directory;
     }
     else {
         $this->dir = $this->dir_file.$this->directory;
     }     

  }

  
  function ges_file($nome_campo, $valore) {
       
       echo "<input type=\"hidden\" id=\"".$nome_campo."\" name=\"".$nome_campo."\" value=\"".$valore."\" >";
       
       echo "<table width=\"100%\"><tr><td width=\"1%\">";
       
       echo "<span id=\"s_".$nome_campo."\" name=\"s_".$nome_campo."\"> ";
       
       $vuoto = $this->vis_file($valore);

       echo "</span></td><td>";
       
       
       
       // bottoni di add e delete
       if ($vuoto) $stile_div =  "style=\"display:none\"";
       else $stile_div =  "style=\"display:block\"";
       echo "<div id=\"d_".$nome_campo."\" ".$stile_div." >";
       echo "<a href=\"javascript:ut_remove_file('".$nome_campo."','".$this->tipo."')\"><img src=\"../../Icons/remove.png\" title=\"Rimuovi".$this->des."\"> </a>";
       echo "</div>";

       if ($vuoto) $stile_div =  "style=\"display:block\"";
       else $stile_div =  "style=\"display:none\"";
       echo "<div id=\"a_".$nome_campo."\" ".$stile_div." >";
       echo "<a href=\"javascript:ut_add_file('".$nome_campo."','".$this->dir."','".$this->dim_x."','".$this->dim_y."','".$this->tipo."')\">
              <img src=\"../../Icons/add.png\" title=\"Aggiungi".$this->des."\"> </a>"; 
       echo "</div>";
       echo "</td></tr></table>";
              
  }

  function vis_file($valore) {
       if ($valore != null && $valore != " " && $valore != "") {
           $vuoto = false;
           if ($this->tipo == "I") {
               echo "<img src=\"".$this->dir.$valore."\"></img>";
           } else {
               echo "<a href=\"".$this->dir.$valore."\">".$valore."</a>";
           }
       }  else {
          $vuoto = true;
          echo "&nbsp;";
       }  
       
       return $vuoto;
  }
  
  function ges_gallery($tavola,$id,$tipo = "I") {
         
         $des = "";
         $this->set_tipo($tipo);
         if ($tipo == "I")  {
             $this->set_directory("gallery"); 
             $des = " alla galleria";
             echo "<div id=\"gallery\">";
         } else {
             $this->set_directory(""); 
             echo "<div id=\"allegati\">";
             $des = " agli allegati";
         } 
         echo $this->vis_gallery($tavola,$id);
         echo "</div>";
         if ($id == null && $id == "") {
             echo "Salvare il record per poter gestire la gallery";             
             return;
         }
         
         echo "<a href=\"javascript:ut_add_file_gallery('".$tavola."',".$id.",'".$tipo."')\"> &nbsp;&nbsp;Aggiungi file ".$des."</a></td>";
  }
  
  function vis_gallery($tavola,$id) {

         

         if ($id == null) {
             return;
         }
         $order = " ordine, id ";               
         $where = " 1 = 2 ";
         $quanti_per_riga = 7;
         if ( $tavola == "tipologie_cabine" ) {
             $where = " IDTipologiaCabina = ".$id;
         } elseif ( $tavola == "destinazioni" ) {
             $where = " IDViaggio = ".$id;
         }
         
         if ($this->tipo == "I") { 
            $risultato = db_query_generale("gallery",$where,$order);
         }  elseif ($this->tipo == "F") {
            $quanti_per_riga = 1; 
            $risultato = db_query_generale("allegati",$where,$order);
         }
         
         
         $tabella_ris = "";
         $tabella_ris .="<table width=\"100%\"><tr><td>";
         $i = 0;
         while ($cur_rec = mysql_fetch_assoc($risultato)) {
            $i++;
            if ($i%$quanti_per_riga == 0) {
               $tabella_ris .= "</td></tr><tr><td>";
            }
          
            if ($this->tipo == "I") {   
               $tabella_ris .= "<td><img src=\"".$this->dir."/thumbs/".$cur_rec['Img']."\"></img>";
            }
            else {
               $tabella_ris .= "<td><a href=\"".$this->dir.$cur_rec['Allegato']."\">".$cur_rec['Allegato']."</a>";
            }   
            $tabella_ris .= "<a href=\"javascript:delete_img_gallery('".$tavola."',".$id.",".$cur_rec['ID'].",'".$this->tipo."')\"><img src=\"../../Icons/remove.png\" title=\"Rimuovi".$this->des."\"> </a></td>";
            
         }
         if ($i==0) {
            $tabella_ris .= "</td>";
         }
         $tabella_ris .= "</tr></table><br></div>";
         return $tabella_ris;
  }
   
          
  
  
  
}
?>
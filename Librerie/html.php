<?php

  function CalendarPopup($name,$day,$month,$year,$class='',$refresh='',$disable=false)
    {
        if (!empty($refresh)){
            $refresh = " onchange=\"this.form.submit();\"";
        }
        $dis = ""; 
        if ($disable) $dis = "disabled";
        
        echo "\t <select name=\"".$name."_D\" id=\"".$name."_D\" ".$dis." class=\"$class\" $refresh>\n";
        for( $i = 1; $i <= 31; $i++ ) {
            $selected = "";
            if($i == $day) {
                $selected = "selected";
            }
            echo "\t\t <option value=\"$i\" $selected >$i</option>\n";
        }
        echo "\t </select>\n";

        CalendarPopupMA($name,$month,$year,$class,$refresh,false,$disable);
    }

  function CalendarPopupMA($name,$month,$year,$class='',$refresh='',$solo = true,$disable=false)
    {
        if (!empty($refresh)){
            $refresh = " onchange=\"this.form.submit();\"";
        }
        $dis = ""; 
        if ($disable) $dis = "disabled";
                
        if ($solo) {
           echo "\t <input type=\"hidden\" name=\"".$name."_D\" id=\"".$name."_D\" value=\"1\">\n";
        }
        
        echo "\t <select name=\"".$name."_M\" id=\"".$name."_M\"  ".$dis."  class=\"$class\" $refresh>\n";
        for( $i = 1; $i <= 12; $i++ ) {
            $selected = "";
            if($i == $month) {
                $selected = "selected";
            }
            $month_name = ucwords(strftime("%B", mktime (0,0,0,$i,1,0)));
            echo "\t\t <option value=\"$i\"  $selected >$month_name</option>\n";
        }
        echo "\t </select>\n";
        echo "\t <input type=\"text\" name=\"".$name."_Y\"  ".$dis."  id=\"".$name."_Y\" value=\"".$year."\" class=\"$class\"  maxlength=\"4\" size=\"4\" $refresh />\n ";
        if (!$disable) {
           echo "\t <A HREF=\"#\" onClick=\"setDate('$name'); return false;\" TITLE=\"Calendario\" NAME=\"anchor\" ID=\"anchor\">\n";
           echo "\t<img border=\"0\" src=\"../../Icons/cal.png\"></A>\n";
        }
    }

  function decodifica_valore($array,$valore) {
       if (array_key_exists($valore,$array))  return $array[$valore];
       return "";
  }
  
  function dec_selezioni($valore) {
      return lista_selezioni(null,$valore,null,null,true);
  }
  function lista_selezioni($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('I' => 'Inizia con',
                        'U' => 'Uguale a',
                        'C' => 'Contiene');
                        
        if ($dec) return decodifica_valore($valori,$valore);
         
        lista_gen($name,$valore,$class,$refresh,$valori);
     }

  function lista_magazzini($name,$valore,$class='',$refresh='',$dec=false,$disabled="")
    {
        $valori = array('' => '',
                        'MC7' => 'COLLEGNO (TO)',
                        'MTI' => 'MONCALIERI (TO)',
			'MTS' => 'SAN MAURO TORINESE (TO)',
			'MOA' => 'ORBASSANO (TO)',
			'MS4' => 'SERRAVALLE (AL)',
			'MCC' => 'CORSICO (MI)',
			'MOG' => 'OLGIATE OLONA (VA)',
			'MGO' => 'SAN GIULIANO MILANESE (MI)',
			'MGV' => 'SESTO SAN GIOVANNI (MI)',
			'MRJ' => 'RESCALDINA (MI)',
			'MCX' => 'CURNO (BG)',
			'MOL' => 'BOLOGNA (BO)',
			'MFE' => 'FERRARA (FE)',
			'MUD' => 'UDINE (UD)',
			'MM8' => 'MESTRE (VE)',
			'MDQ' => 'PADOVA (PD)',
			'MVZ' => 'VICENZA (VI)',
			'MRF' => 'ROMA FIUMICINO (Roma)',
			'MCW' => 'ROMA CASILINA (Roma)',
			'MFI' => 'ROMA TORRESINA (Roma)',
			'MRM' => 'ROMA ROMANINA (Roma)',
			'MDV' => 'VITERBO (VT)');
	                        
        if ($dec) return decodifica_valore($valori,$valore);
         
        lista_gen_mag($name,$valore,$class,$refresh,$valori,$disabled);
     }

  function dec_tipi_sezionali($valore) {
      return lista_tipi_sezionali(null,$valore,null,null,true);
  }
  function lista_tipi_sezionali($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(''=> '',
                        'V' => 'Vendita',
                        'A' => 'Acquisto');
        
        if ($dec) return decodifica_valore($valori,$valore);
                                
        lista_gen($name,$valore,$class,$refresh,$valori);
     }     

  function dec_tipologia_iva($valore) {
      return lista_tipologia_iva(null,$valore,null,null,true);
  }
  function lista_tipologia_iva($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(''=> '',
                        'I' => 'Imponibile',
                        'E' => 'Esente',
                        'N' => 'Non Soggetto',
                        'X' => 'Non Imponibile',
                        '7' => '74 Ter');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }  

  function dec_tipo_decorrenza($valore) {
      return lista_tipo_decorrenza(null,$valore,null,null,true);
  }
  function lista_tipo_decorrenza($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(''=> '',
                        'D' => 'Data Fattura',
                        'F' => 'D.F. Fine Mese',
                        'G' => 'Giorno Fisso');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }  

    function lista_box($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(  ''=> '',
                        '11'=> '1-1',
                        '12' => '1-2',
                        '21' => '2-1',
                        '22' => '2-2',
                        '31' => '3-1',
                        '32' => '3-2',
                        '41' => '4-1',
                        '42' => '4-2');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }  



  function dec_tipo_pagamento($valore) {
      return lista_tipo_pagamento(null,$valore,null,null,true);
  }
  function lista_tipo_pagamento($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(''=> '',
                        'D' => 'Rimessa Diretta',
                        'B' => 'Bonifico',
                        'R' => 'RiBa',
                        'A' => 'Assegno');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }       

  function lista_numero_rate($name,$valore,$class='',$refresh='')
    {
         $valori;
         $i= 0;
         $a="0";
         while ($i <= 9) {
             $a = sprintf("%02d",$i);
             $valori[$a] = $a;
             $i++;
          }          
        lista_gen($name,$valore,$class,$refresh,$valori);
     }

  function dec_vendita_rimborso($valore) {
      return lista_vendita_rimborso(null,$valore,null,null,true);
  }
  function lista_vendita_rimborso($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(''=> '',
                        'V' => 'Vendita',
                        'R' => 'Rimborso');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }   

  function dec_tipo_pratica($valore) {
      return lista_tipo_pratica(null,$valore,null,null,true);
  }     
  function lista_tipo_pratica($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(''=> '',
                        'I' => 'Intermediazione',
                        'P' => 'Propria');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }  

  function lista_segno($name,$valore,$class='',$refresh='')
    {
        $valori = array(''=> '',
                        '+' => '+',
                        '-' => '-');
        lista_gen($name,$valore,$class,$refresh,$valori);
     }       

  function dec_tipo_operazione($valore) {
      return lista_tipo_operazione(null,$valore,null,null,true);
  }    
  function lista_tipo_operazione($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(''=> '',
                        'I' => 'Incasso',
                        'S' => 'Saldo');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }         

  function lista_programmi($name,$valore,$class='',$refresh='')
    {
        $valori = array(''=> '',
                        'Air Spent by Month' => 'Air Spent by Month',
                        'Estrazione Dati' => 'Estrazione Dati',
                        'Top City Pairs' => 'Top City Pairs',
                        'Top Destinations' => 'Top Destinations',
                        'Airline Ticket Summary' => 'Airline Ticket Summary'                       
                        );                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }

  function lista_tipifile($name,$valore,$class='',$refresh='')
    {
        $valori = array(''=> '',
                        'csv' => 'Sequenziali',
                        'pdf' => 'PDF',                    
                        );
        lista_gen($name,$valore,$class,$refresh,$valori);
     }     

  function dec_sn($valore) {
      return lista_sn(null,$valore,null,null,true);
  }  
  function lista_sn($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('S' => 'Si',
                        'N' => 'No');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }

  function dec_01($valore) {
      return lista_01(null,$valore,null,null,true);
  }  
  function lista_01($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('1' => 'Si',
                        '0' => 'No');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }    
  function dec_MF($valore) {
      return lista_MF(null,$valore,null,null,true);
  }  
  function lista_MF($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array(' ' => '',
                        'M' => 'Maschio',
                        'F' => 'Femmina');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }       

  function dec_exchange($valore) {
      return lista_exchange(null,$valore,null,null,true);
  } 
  function lista_exchange($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('' => '',
                        'N' => 'No', 
                        'S' => 'Si');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }

  function dec_continenti($valore) {
      return lista_continenti(null,$valore,null,null,false,true);
  } 
  function lista_continenti($name,$valore,$class='',$refresh='',$selezione = false,$dec=false)
    { 
        $valori = array(' ' => ' ',
                        'EU' => 'Europa',
                        'AF' => 'Africa',
                        'AS' => 'Asia',
                        'NA' => 'Nord America',
                        'SA' => 'Sud America',
                        'OC' => 'Oceania'
                        );                        
        if (!$selezione) unset($valori[' ']);
        if ($dec) return decodifica_valore($valori,$valore);        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }     

  function lista_ore($name,$valore,$class='',$refresh='')
    {
         $valori;
         $i= 0;
         $a="0";
         while ($i <= 23) {
             $a = sprintf("%02d",$i);
             $valori[$a] = $a;
             $i++;
          }
        lista_gen($name,$valore,$class,$refresh,$valori);
     }

  function lista_order_by($name,$valore,$class='',$refresh='')
    {
         $valori;
         $i= 0;
         $a="0";
         while ($i <= 9) {
             $valori[$i] = $i;
             $i++;
          }
        lista_gen($name,$valore,$class,$refresh,$valori);
     }     

  function lista_minuti($name,$valore,$class='',$refresh='')
    {
         $valori;
         $i= 0;
         $a="0";
         while ($i <= 59) {
             $a = sprintf("%02d",$i);
             $valori[$a] = $a;
             $i++;
          }
        lista_gen($name,$valore,$class,$refresh,$valori);
     }

  function dec_durata($valore) {
      return lista_durata(null,$valore,null,null,true);
  }
  function lista_durata($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('In giornata' => 'In giornata',
                        'Giorno Successivo' => 'Giorno Successivo',
                        'Due giorni dopo' => 'Due giorni dopo',
                        'non disponibile' => 'non disponibile',
                        '' => ' ');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }

  function dec_int($valore) {
      return lista_int(null,$valore,null,null,true);
  }
  function lista_int($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('International' => 'International',
                        'Domestic' => 'Domestic',
                        'non disponibile' => 'non disponibile',
                        '' => ' ');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }

  function dec_tipo_data($valore) {
      return lista_tipo_data(null,$valore,null,null,true);
  }
  function lista_tipo_data($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('V' => 'Vendita',
                        'P' => 'Partenza'
                        );
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }      

  function dec_tipo_volo($valore) {
      return lista_tipo_volo(null,$valore,null,null,true);
  }
  function lista_tipo_volo($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('IN' => 'Internazionale',
                        'IC' => 'Intercontinentale',
                        'DO' => 'Domestic',
                        'T' => 'Tutti',
                        );
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     } 

  function dec_tipo_scalo($valore) {
      return lista_tipo_scalo(null,$valore,null,null,true);
  }
  function lista_tipo_scalo($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('A' => 'Arrivo',
                        'P' => 'Partenza',
                        'T' => 'Tutto'
                        );
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }  
     
  function lista_valore_perc($name,$valore,$class='',$refresh='',$dec=false)
    {
        $valori = array('P' => 'Percentuale','V' => 'Valore');
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);
     }  


function lista_colore($name,$valore,$class='',$refresh='',$dec=false)
    {
    
        $valori = array('' => '',
                        'Gray' => 'Grigio',
                        'Brixia' => 'Brixia',
                        'Silver' => 'Argento',
                        'VerdeMela' => 'Verde Mela',
                         'OrangeRed' => 'Arancio',
                         'Red' => 'Rosso',
                         'Yellow' => 'Giallo',
                         'LimeGreen'=> 'Lime', 
                         'Blue' => 'Blu',
                         'Violet' => 'Viola',
                         'Azzurro' => 'Azzurro'
                        );
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }   


  




function lista_classe($name,$valore,$class='',$refresh='',$dec=false)
    {
    
        $valori = array('' => '',
                        'BAN' => 'BAPS - Nursery',
                        'BAT' => 'BAPS - Transition',
                        'BAR' => 'BAPS - Reception',
                        'BEE' => 'BES - Elementari',
                         'BEM' => 'BES - Medie'
                         
                        );
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     } 


function lista_livello($name,$valore,$class='',$refresh='',$dec=false)
    {
    
        $valori = array('' => '',
                        '1' => 'PRIMA',
                        '2' => 'SECONDA',
                        '3' => 'TERZA',
                        '4' => 'QUARTA',
                         '5' => 'QUINTA'
                         
                        );
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     } 



function lista_cataloghi($name,$valore,$class='',$refresh='',$dec=false)
    {
    
        $valori = array('CapoVerde' => 'Capo Verde',
                        'Cuba' => 'Cuba',
                        'Ischia' => 'Ischia',
                        'MaltaCipro' => 'Malta e Cipro',
                        'MarsaAlam' => 'Marsa Alam',
                        'MarsaMatrouh' => 'Marsa Matrouh',
                        'Spagna' => 'Spagna',
                        'SpagnaGiovani'=> 'Spagna Giovani', 
                        'SpagnaCittaeTour' => 'Spagna Citt&agrave; e Tour',
                        'TermeBenessere' => 'Terme e Benessere'
                        );
        if ($dec) return decodifica_valore($valori,$valore);                        
        lista_gen($name,$valore,$class,$refresh,$valori);                               
     }           


     
  function lista_gen($name,$valore,$class='',$refresh='',$valori)
    {
        if (!empty($refresh)){
            $refresh = " onchange=\"this.form.submit();\"";
        }
        echo "\t <select name=\"".$name."\" id=\"".$name."\" class=\"$class\" $refresh>\n";
        foreach ($valori as $key => $value) {
              $selected = "";
              if ($key == $valore ) $selected = "selected";
              echo "\t\t <option value=\"$key\" $selected>$value</option>\n";
        } 
        echo "\t </select>\n";        
     }    

       function lista_gen_mag($name,$valore,$class='',$refresh='',$valori,$disabled)
    {
        if (!empty($refresh)){
            $refresh = " onchange=\"this.form.submit();\"";
        }
        echo "\t <select name=\"".$name."\" id=\"".$name."\" class=\"$class\" $refresh $disabled>\n";
        foreach ($valori as $key => $value) {
              $selected = "";
              if ($key == $valore ) $selected = "selected";
              echo "\t\t <option value=\"$key\" $selected>$key - $value</option>\n";
        } 
        echo "\t </select>\n";        
     }    

  function filtro_iniziale ( $pagina,$start, $ordinamento,$numeri = true,$campo = null) {
       echo "<table><tr><td class=\"px\" height=\"3\"></td></tr>";
       
       //if ($start == "%") echo "(attuale &nbsp;Tutti&nbsp;)";
       //elseif ($start != "@") echo "(attuale &nbsp;$start&nbsp;)";
       
       echo "<tr><td>";
       foreach (range('A', 'Z') as $l) {
              
              $val = $l;
              if ($l == $start) $val = "<b>$l</b>";        
       
           echo ("<a href=\"".$pagina."start=$l&order=$ordinamento\">$val</a>&nbsp;&nbsp;");      
       }
       if ($numeri ) {
         for ($i=0;$i<10;$i++) {
             $val = $i;
             if (((String)($i)) == $start) $val = "<b>$i</b>"; 
             echo ("<a href=\"".$pagina."start=$i&order=$ordinamento\">$val </a>&nbsp;&nbsp;");
         }
       }
      
       $val = "Tutti";
       if ($start == "%" || $start == "@"  ) $val = "<b>Tutti</b>";   
       echo ("<a href=\"".$pagina."start=%&order=$ordinamento\">$val</a>&nbsp;&nbsp;");
           
       echo "</td></tr><tr><td class=\"px\" height=\"15\"></td></tr></table>";
  }

  function filtro_ordimanento ( $pagina, $start,$ordinamento,$campo1="Codice",$campo2="Descrizione") {
       echo "<table><tr><td class=\"px\" height=\"5\"></td></tr><tr><td>";
       echo "Seleziona Ordinamento (attuale &nbsp;$ordinamento&nbsp;):</td></tr><tr><td>";
       
       echo ("<a href=\"".$pagina."start=$start&order=codice\">Codice</a>&nbsp;&nbsp;");
       echo ("<a href=\"".$pagina."start=$start&order=descrizione\">Descrizione</a>&nbsp;&nbsp;");      
       echo "</td></tr><tr><td class=\"px\" height=\"15\"></td></tr></table>";
  }

     
  function calendario_js() {
    echo "
    <script type=\"text/javascript\" src=\"../../Html/CalendarPopup.js\"></script>
    <script type=\"text/javascript\">
    var cal = new CalendarPopup();
    var calName = '';
    function setMultipleValues(y,m,d) {
         document.getElementById(calName+'_Y').value=y;
         document.getElementById(calName+'_M').selectedIndex=m*1-1;
         document.getElementById(calName+'_D').selectedIndex=d*1-1;
    }
    function setDate(name) {
      calName = name.toString();
      var year = document.getElementById(calName+'_Y').value.toString();
      var month = document.getElementById(calName+'_M').value.toString();
      var day = document.getElementById(calName+'_D').value.toString();
      var mdy = month+'/'+day+'/'+year;
      cal.setReturnFunction('setMultipleValues');
      cal.showCalendar('anchor', mdy);
    }
    </script>
    ";  
  }



  function editor_js() {
   echo" <!-- TinyMCE -->
    <script type=\"text/javascript\" src=\"../../Html/tiny_mce/tiny_mce.js\"></script>
    <script type=\"text/javascript\">
    
    	tinyMCE.init({
    		// General options
    		mode : \"specific_textareas\",
        editor_selector : \"ed\",
    		theme : \"advanced\",
    		plugins : \"pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount,advlist,autosave\",";
        
        if (isset($genera_offerta) && $genera_offerta == true)
         echo "width : \"115 mm\",
         theme_advanced_resizing : false,
         theme_advanced_resizing_min_width: \"115 mm\",
         theme_advanced_resizing_max_width: \"115 mm\",
         onchange_callback : \"myCustomOnChangeHandler\",";


        echo "
    		// Theme options
    	//	theme_advanced_buttons1 : \"save,newdocument,|bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect\",
          theme_advanced_buttons1 : \"bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,fontselect,fontsizeselect\",
    		theme_advanced_buttons2 : \"copy,paste,pastetext,pasteword,undo,redo,search,replace,bullist,numlist,|,outdent,indent,|cleanup,help,forecolor,backcolor,|,preview,fullscreen\",
        //theme_advanced_buttons2 : \"cut,copy,paste,pastetext,pasteword,|,bullist,numlist,|,outdent,indent,blockquote,|,link,unlink,anchor,image,cleanup,code,preview,|,forecolor,backcolor\",
    	//	theme_advanced_buttons3 : \"tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr\",
    	//	theme_advanced_buttons4 : \"insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,restoredraft\",
         theme_advanced_buttons3 : \"\",      
      
    		theme_advanced_toolbar_location : \"top\",
    		theme_advanced_toolbar_align : \"left\",
    		theme_advanced_statusbar_location : \"bottom\",
    		theme_advanced_resizing : true,
                theme_advanced_font_sizes : \"8px,10px,12px,14px,18px,24px,36px,42px,48px,56px,72px\",
                theme_advanced_fonts : \"Arial=arial,helvetica,sans-serif;\"+
                        \"Helvetica=helvetica;\"+
                        \"Times New Roman=times new roman,times;\",
  
    
    		// Example content CSS (should be your site CSS)
    		content_css : \"../../Html/content.css\",
    
    		// Drop lists for link/image/media/template dialogs
    		template_external_list_url : \"lists/template_list.js\",
    		external_link_list_url : \"lists/link_list.js\",
    		external_image_list_url : \"lists/image_list.js\",
    		media_external_list_url : \"lists/media_list.js\",
    
    		// Style formats
    		style_formats : [
    			{title : 'Bold text', inline : 'b'},
    			{title : 'Red text', inline : 'span', styles : {color : '#ff0000'}},
    			{title : 'Red header', block : 'h1', styles : {color : '#ff0000'}},
    			{title : 'Example 1', inline : 'span', classes : 'example1'},
    			{title : 'Example 2', inline : 'span', classes : 'example2'},
    			{title : 'Table styles'},
    			{title : 'Table row 1', selector : 'tr', classes : 'tablerow1'}
    		],
    
    		formats : {
    			alignleft : {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'left'},
    			aligncenter : {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'center'},
    			alignright : {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'right'},
    			alignfull : {selector : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', classes : 'full'},
    			bold : {inline : 'span', 'classes' : 'bold'},
    			italic : {inline : 'span', 'classes' : 'italic'},
    			underline : {inline : 'span', 'classes' : 'underline', exact : true},
    			strikethrough : {inline : 'del'}
    		},
    
    		// Replace values for the template plugin
    		template_replace_values : {
    			username : \"Some User\",
    			staffid : \"991234\"
    		}
    	});
    </script>
    <!-- /TinyMCE -->";
  }
  
  function ajaxcall() {
      echo "<!--script type=\"text/javascript\" src=\"../../Html/ajax.js\"></script-->";  
  }
  
  function datepickersetting() {
  
        return "numberOfMonths: 1,
        			showButtonPanel: true,
              showOtherMonths: true,
              selectOtherMonths: true,
              dateFormat: 'dd/mm/yy',
              changeYear: true,
              changeMonth: true,
              dayNames: ['Domenica', 'Lunedi', 'Martedi', 'Mercoledi', 'Giovedi', 'Venerdi', 'Sabato'],
              dayNamesMin: ['Do', 'Lu', 'Ma', 'Me', 'Gi', 'Ve', 'Sa'],
              dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mer', 'Gio', 'Ven', 'Sab'],
              monthNames: ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'],
              monthNamesShort: ['Gen','Feb','Mar','Apr','Mag','Giu','Lug','Ago','Set','Ott','Nov','Dic']"; 
  }
  
  /*function datepicker($nome) {
      echo "       <script>
              	$(function() {
              		$( \"#".$nome."\" ).datepicker({".datepickersetting().",
                  beforeShow: function (textbox, instance) {
                                  instance.dpDiv.css({
                                         marginTop: (-textbox.offsetHeight) + 'px',
                                         marginLeft: textbox.offsetWidth + 'px'
                                                     });
                                          },                  
              		});
              	});
              </script>";  
  } */
  
   function datepicker($nome) {
        echo "       <script>
              	$(function() {
              		$( \"#".$nome."\" ).daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                     autoUpdateInput: false,
                         timePicker: false,
                    \"locale\": {
                        \"format\": \"DD/MM/YYYY\",
                        \"separator\": \" - \",
                        \"applyLabel\": \"Ok\",
                        \"cancelLabel\": \"Cancella\",
                        \"fromLabel\": \"Da\",
                        \"toLabel\": \"a\",
                        \"customRangeLabel\": \"Libero\",
                        \"daysOfWeek\": [
                            'Do', 'Lu', 'Ma', 'Me', 'Gi', 'Ve', 'Sa'
                        ],
                        \"monthNames\": [
                            'Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre'
                        ],
                        \"firstDay\": 1
                    }
              });
              
              $( \"#".$nome."\").on('apply.daterangepicker', function(ev, picker) {
                  $(this).val(picker.startDate.format('DD/MM/YYYY'));
              });
              
              $( \"#".$nome."\").on('cancel.daterangepicker', function(ev, picker) {
                  $(this).val('');
              });
           });
           </script>";  
   }           

  function datepickerFromTo($nome,$nome1) {
      echo "       <script>
              	$(function() {
              	 var dates =	$( \"#".$nome.",#".$nome1."\" ).datepicker({defaultDate: \"+1w\",
                          ".datepickersetting()."
                    			, beforeShow: function (input, instance) {
                                  instance.dpDiv.css({
                                         marginTop: (-input.offsetHeight) + 'px',
                                         marginLeft: input.offsetWidth + 'px'
                                                     });                          
                                  return {
                                                    minDate: (input.id == \"".$nome."\" ? new Date(2008, 12 - 1, 1) : null),
                                                    minDate: (input.id == \"".$nome1."\" ? $(\"#".$nome."\").datepicker(\"getDate\") : null), 
                                                    maxDate: (input.id == \"".$nome."\" ? $(\"#".$nome1."\").datepicker(\"getDate\") : null),
                                                    defaultDate: (input.id == \"".$nome1."\" ? $(\"#".$nome."\").datepicker(\"getDate\") : null)
                                       };                                           
                          }
              		});
              	});
              </script>";  
  }  
?>

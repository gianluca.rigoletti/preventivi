<?php

function crea_menu() {
  global $m;
  if ( is_logged()  && !isset($_GET['rub']) )  {        
    $classemenu = "";
    if ($m=="home")  $classemenu="class=\"current-page\""; else $classemenu = "";
    echo '
    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <ul class="nav side-menu">
          <li '.$classemenu.'><a href="index.php"><i class="fa fa-home"></i> Home </a>
          </li>';
          $classemenu = "";
          /*if ($m=="rubrica")  $classemenu="class=\"current-page\""; else $classemenu = "";
          echo '<li '.$classemenu.'><a href="rubrica.php"><i class="fa fa-users"></i> Rubrica </a>';*/
        echo ' </li>
        <li><a><i class="fa fa-edit"></i> Gestione <span class="fa fa-chevron-down"></span></a>
          <ul class="nav child_menu">';
           $classemenu = "";

      
           if ($m=="ordini")  $classemenu="class=\"current-page\"";    else $classemenu = "";
           echo '<li '.$classemenu.'><a href="vis_ordini.php">Gestione Preventivi</a></li>';



           echo ' </ul> ';

           if ($_SESSION['amministratore']) {

            echo '  <li><a><i class="fa fa-gear"></i> Amministrazione <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">';
            
           if ($m=="citta")  $classemenu="class=\"current-page\"";    else $classemenu = "";
           echo '<li '.$classemenu.'><a href="vis_citta.php">Gestione Città</a></li>';

           if ($m=="fornitori")  $classemenu="class=\"current-page\"";    else $classemenu = "";
           echo '<li '.$classemenu.'><a href="vis_fornitori.php">Gestione Fornitori</a></li>';

           if ($m=="ordine")  $classemenu="class=\"current-page\"";    else $classemenu = "";
           echo '<li '.$classemenu.'><a href="vis_ordini.php">Gestione Ordini</a></li>';

           if ($m=="pax")  $classemenu="class=\"current-page\"";    else $classemenu = "";
           echo '<li '.$classemenu.'><a href="vis_pax.php">Gestione Pax</a></li>';

           if ($m=="servizi_hotel")  $classemenu="class=\"current-page\"";    else $classemenu = "";
           echo '<li '.$classemenu.'><a href="vis_servizi_hotel_tariffe.php">Gestione Servizi Hotel</a></li>';

           if ($m=="servizi_hotel_tariffe")  $classemenu="class=\"current-page\"";    else $classemenu = "";
           echo '<li '.$classemenu.'><a href="vis_servizi_hotel_tariffe.php">Gestione Tariffe Servizi Hotel</a></li>';

           if ($m=="zone")  $classemenu="class=\"current-page\"";    else $classemenu = "";
           echo '<li '.$classemenu.'><a href="vis_zone.php">Gestione Zone</a></li>';

           if ($m=="nazioni")  $classemenu="class=\"current-page\"";    else $classemenu = "";
           echo '<li '.$classemenu.'><a href="vis_nazioni.php">Gestione Nazioni</a></li>';

           if ($m=="province")  $classemenu="class=\"current-page\"";    else $classemenu = "";
           echo '<li '.$classemenu.'><a href="vis_province.php">Gestione Province</a></li>';

           if ($m=="tipi_servizi")  $classemenu="class=\"current-page\"";    else $classemenu = "";
           echo '<li '.$classemenu.'><a href="vis_tipi_servizi.php">Gestione Tipi Servizi</a></li>';

           if ($m=="servizi_liberi")  $classemenu="class=\"current-page\"";    else $classemenu = "";
           echo '<li '.$classemenu.'><a href="vis_anagrafica_servizi_liberi.php">Gestione Servizi Liberi</a></li>';
                       if ($m=="carica_file")  $classemenu="class=\"current-page\"";    else $classemenu = "";
           echo '<li '.$classemenu.'><a href="vis_carica_file.php">Carica File</a></li>';
            
             $classemenu = "";

             if ($m=="amministratori")  $classemenu="class=\"current-page\""; else $classemenu = "";
             if ($_SESSION['amministratore']) echo '<li '.$classemenu.'><a href="vis_amministratori.php">Gestione Amministratori</a></li>';

             echo '              
           </li>
         </ul>';
       }
         echo '
         <!--li><a><i class="fa fa-desktop"></i> UI Elements <span class="fa fa-chevron-down"></span></a>
         <ul class="nav child_menu">
          <li><a href="general_elements.html">General Elements</a>
          </li>
          <li><a href="media_gallery.html">Media Gallery</a>
          </li>
          <li><a href="typography.html">Typography</a>
          </li>
          <li><a href="icons.html">Icons</a>
          </li>
          <li><a href="glyphicons.html">Glyphicons</a>
          </li>
          <li><a href="widgets.html">Widgets</a>
          </li>
          <li><a href="invoice.html">Invoice</a>
          </li>
          <li><a href="inbox.html">Inbox</a>
          </li>
          <li><a href="calendar.html">Calendar</a>
          </li>
        </ul>
      </li>
      <li><a><i class="fa fa-table"></i> Tables <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="tables.html">Tables</a>
          </li>
          <li><a href="tables_dynamic.html">Table Dynamic</a>
          </li>
        </ul>
      </li>
      <li><a><i class="fa fa-bar-chart-o"></i> Data Presentation <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="chartjs.html">Chart JS</a>
          </li>
          <li><a href="chartjs2.html">Chart JS2</a>
          </li>
          <li><a href="morisjs.html">Moris JS</a>
          </li>
          <li><a href="echarts.html">ECharts </a>
          </li>
          <li><a href="other_charts.html">Other Charts </a>
          </li>
        </ul>
      </li>
    </ul>
  </div>
  <div class="menu_section">
    <h3>Live On</h3>
    <ul class="nav side-menu">
      <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="e_commerce.html">E-commerce</a>
          </li>
          <li><a href="projects.html">Projects</a>
          </li>
          <li><a href="project_detail.html">Project Detail</a>
          </li>
          <li><a href="contacts.html">Contacts</a>
          </li>
          <li><a href="profile.html">Profile</a>
          </li>
        </ul>
      </li>
      <li><a><i class="fa fa-windows"></i> Extras <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="page_404.html">404 Error</a>
          </li>
          <li><a href="page_500.html">500 Error</a>
          </li>
          <li><a href="plain_page.html">Plain Page</a>
          </li>
          <li><a href="login.html">Login Page</a>
          </li>
          <li><a href="pricing_tables.html">Pricing Tables</a>
          </li>
        </ul>
      </li>
      <li><a><i class="fa fa-sitemap"></i> Multilevel Menu <span class="fa fa-chevron-down"></span></a>
        <ul class="nav child_menu">
          <li><a href="#level1_1">Level One</a>
            <li><a>Level One<span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <li class="sub_menu"><a href="level2.html">Level Two</a>
                </li>
                <li><a href="#level2_1">Level Two</a>
                </li>
                <li><a href="#level2_2">Level Two</a>
                </li>
              </ul>
            </li>
            <li><a href="#level1_2">Level One</a>
            </li>
          </ul>
        </li>
        <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a>
        </li-->
      </ul>
    </div>

  </div>
  <!-- /sidebar menu -->

  <!-- /menu footer buttons -->
  <!--div class="sidebar-footer hidden-small">
  <a data-toggle="tooltip" data-placement="top" title="Settings">
    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
  </a>
  <a data-toggle="tooltip" data-placement="top" title="FullScreen">
    <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
  </a>
  <a data-toggle="tooltip" data-placement="top" title="Lock">
    <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
  </a>
  <a data-toggle="tooltip" data-placement="top" title="Logout" >
    <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
  </a>
  </div-->
  <!-- /menu footer buttons -->';
  }
}
?>

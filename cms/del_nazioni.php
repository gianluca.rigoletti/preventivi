<?php

require '../Librerie/connect.php';

$Titolo = "Annullamento Nazione";
$Tavola= "nazioni";

$indietro = "vis_nazioni.php";

if (isset($_GET['id'])) {
    db_delete($Tavola, $_GET['id']);
    header("Location: $indietro");
    exit;
}
header("Location: $indietro");
exit;

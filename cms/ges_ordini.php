<?php
$m = 'ordini';

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';
require 'utilities_preventivo.php';

$c_files = new files();
$tavola = 'ordine';
$indietro = 'vis_ordini.php';
$array_camere = array(
    1 => 'Singola',
    2 => 'Doppia',
    3 => 'Tripla',
    4 => 'Quadrupla'
);

if (isset($_GET['back'])) {
    $indietro = $_GET['back'].'.php';
}
if ($_GET['p_upd'] == 1) {
    $funzione = 'Update';
    $disabilita_chiave = 'disabled';
    $titolo = 'Modifica Preventivo';
} else {
    $funzione = 'Insert';
    $titolo = 'Nuovo Preventivo';
}
if ($_GET['p_upd'] == 1) {
    $risultato = db_query_mod($tavola, $_GET['p_id']);
    $cur_rec = mysql_fetch_assoc($risultato);
}
if (isset($_POST['Insert']) || isset($_POST['Update'])) {
    $cur_rec['ID'] = $_POST['ID'];
    $cur_rec['DESCRIZIONE'] = $_POST['DESCRIZIONE'];
    $cur_rec['DATAIN'] = $_POST['DATAIN'];
    $cur_rec['DATAOUT'] = $_POST['DATAOUT'];
    $cur_rec['NUMEROPAX'] = $_POST['NUMEROPAX'];
    $cur_rec['NOME_INT'] = $_POST['NOME_INT'];
    $cur_rec['MAIL_INT'] = $_POST['MAIL_INT'];
    $cur_rec['TEL_INT'] = $_POST['TEL_INT'];
    $cur_rec['MARKUP'] = $_POST['MARKUP'];
    $cur_rec['CAMBIO'] = $_POST['CAMBIO'];
    $cur_rec['MARKUP_TYPE'] = $_POST['MARKUP_TYPE'];
    $cur_rec['CONFERMATO'] = $_POST['CONFERMATO'];
    $cur_rec['NOTE'] = $_POST['NOTE'];

    if (isset($_POST['Insert']) && db_dup_key($tavola, $_POST) > 0) {
         $c_err->add("Preventivo Gi&agrave; Codificato", "ID");
    }
    if (!$c_err->is_errore()) {
        if (isset($_POST['Insert'])) {
            db_insert($tavola, $_POST);
        } else {
            db_update($tavola, $cur_rec['ID'], $_POST);

        }
        header('Location: vis_ordini.php');
        exit;
    }
}
if (isset($_POST['Return'])) {
    header('Location: '.$indietro);
    exit;
}

require '../Librerie/ges_html_top.php';
$c_err->mostra();
?>

<?php if (isset($_GET['p_upd']) && $_GET['p_upd'] == 1) {
    $location = 'ges_ordini.php';
    $post_value = '<input type="hidden" name="Update" value="Update">';
} else {
    $location = 'ges_dett_ordini.php?page=1';
    $post_value = '';
}

?>
<form id="formG" class="form-horizontal form-label-left preventivo" data-parsley-validate  action="<?php echo $location; ?>" method="post" novalidate>
<input type="hidden" name="ID" value="<?php if (isset($cur_rec)) echo $cur_rec['ID']; ?>" >
<input type="hidden" name="New" value="New">
<?php echo $post_value; ?>
<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Descrizione <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <textarea required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("DESCRIZIONE");?> name="DESCRIZIONE"  id="DESCRIZIONE" maxlength="200"><?php if (isset($cur_rec)) echo $cur_rec['DESCRIZIONE'];?></textarea>
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Nome Intestatario </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("NOME_INT");?> name="NOME_INT"  id="NOME_INT" value="<?php if (isset($cur_rec)) echo $cur_rec['NOME_INT'];?>" size="55" maxlength="200"><br />
  </div>
</div>
<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Mail Intestatario </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="email" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("MAIL_INT");?> name="MAIL_INT"  id="MAIL_INT" value="<?php if (isset($cur_rec)) echo $cur_rec['MAIL_INT'];?>" size="55" maxlength="200"><br />
  </div>
</div>
<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Telefono Intestatario </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("TEL_INT");?> name="TEL_INT"  id="TEL_INT" value="<?php if (isset($cur_rec)) echo $cur_rec['TEL_INT'];?>" size="55" maxlength="200"><br />
  </div>
</div>
<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Cambio </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" cambio="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("CAMBIO");?> name="CAMBIO"  id="CAMBIO" value="<?php if (isset($cur_rec)) echo db_visnumeric($cur_rec['CAMBIO'],4);?>" size="55" maxlength="10"><br />
  </div>
</div>
<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Markup </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" importoeuro="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("MARKUP");?> name="MARKUP"  id="MARKUP" value="<?php if (isset($cur_rec)) echo db_visimporti($cur_rec['MARKUP']);?>" size="55" maxlength="10"><br />
  </div>
</div>
<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Tipo Markup
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php $valori = array(
      '1' => 'Percentuale',
      '2' => 'Valore assoluto'
       );
      $valore = "";
      if (isset($cur_rec)) $valore =  $cur_rec['MARKUPTYPE'];
      lista_gen('MARKUPTYPE',$valore,"form-control col-md-7 col-xs-12",null,$valori);  ?>        
    </div>
</div>

<div class="item form-group">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Confermato
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <?php $valori = array(
      '0' => 'No',
      '1' => 'Si'
       );
      $valore = "";
      if (isset($cur_rec)) $valore =  $cur_rec['CONFERMATO'];
      lista_gen('CONFERMATO',$valore,"form-control col-md-7 col-xs-12",null,$valori);  ?>        
    </div>
</div>


<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Note 
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <textarea  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("NOTE");?> name="NOTE"  id="NOTE" maxlength="2000"><?php if (isset($cur_rec)) echo $cur_rec['NOTE'];?></textarea>
  </div>
</div>

<div class="item form-group control-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Periodo  <span class="required">*</span>
  </label>
    <div class="controls col-md-6 col-sm-6 col-xs-12">
        <div class="input-prepend input-group  ">
            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
            <input required="true" type="text"  name="reservation" id="reservation" class="form-control" value="" />
        </div>
    </div>
</div>
<?php
    if (!empty($_GET['p_id'])) {
        $query_camere = db_query_generale('ordine_camere', 'IDORDINE = '.$cur_rec['ID'], 'ID');
        $numero_camere = mysql_num_rows($query_camere);
?>
        <h2 class="text-center">Numero Camere: <?php echo $numero_camere; ?></h2>
<?php
        while ($camere = mysql_fetch_assoc($query_camere)) {
?>
        <div class="row item text-center">
                <h2>Tipo: <small> <?php echo $array_camere[$camere['TIPO']]; ?> </small></h2>
        </div>
<?php
        }
    } else {
?>
    <div class="item form-group">
        <label for="numero-camere" class="control-label col-md-3">Numero camere <span class="required">*</span></label>
        <div class="col-xs-12 col-md-6">
            <select required="true" id="numero-camere" name="numero-camere" class="form-control" required="">
                <option value="">Seleziona...</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
            </select>
        </div>
    </div>
    <div class="item form-group col-xs-12 col-md-9"></div>
    <div class="item form-group tipo-camere col-xs-12"></div>
    <div class="item form-group numero-bambini col-xs-12"></div>
<?php
    }
?>
<div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 ">
      <button class="cancel btn btn-primary pull-right" type="submit" name="Return" value="Return">Indietro</button>
  </div>
      <div class="col-xs-12 col-md-6">
          <button type="submit" class="btn btn-success">
              <?php
                if (isset($_GET['p_upd'])) {
                    echo "Salva";
                } else {
                    echo "Cerca città";
                }
              ?>
          </button>
      </div>
</div>
</form>

<script>
    $(document).ready(function () {
        $('#reservation').daterangepicker({
            'autoupdateinput': false,
            'opens': 'left',
            <?php
                if (!empty($_GET['p_id'])) {
                    echo "
                    startDate: '".$cur_rec['DATAIN']."',
                    endDate: '".$cur_rec['DATAOUT']."',
                    ";
                } else {          
                    echo " 
                    startDate: '2017-01-01',
                    endDate: '2017-01-02',
                    "; 
                }
            ?>
            locale: {
                format: 'YYYY-MM-DD',
                cancelLabel: 'clear'
            }
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });

        var numeroCamere, colLength;
        // Genera il numero di camere
        $('#numero-camere').change(function () {
            numeroCamere = $('#numero-camere').val();
            $('.camera-form-group').remove();
            $('.bambini-form-group').remove();
            for (var i = 1; i <= numeroCamere; i++) {
                colLength = 12/numeroCamere;
                var templateCamere = "\
                <div class=\"camera-form-group form-group col-xs-12 col-md-4\">\
                    <label for=\"camera-" + i + "\">\
                        Tipo camera\
                    </label>\
                    <select id=\"camera-" + i + "\" name=\"camera-" + i + "\" class=\"camera-select form-control\" required=\"\">\
                        <option value=\"1\"> singola </option>\
                        <option value=\"2\"> doppia </option>\
                        <option value=\"3\"> tripla </option>\
                        <option value=\"4\"> quadrupla </option>\
                    </select>\
                </div>\
                <div class=\"col-xs-12 col-md-4 bambini-form-group form-group\" id=\"bambini-camera-" + i + "\">\
                </div>\
                <div class=\"col-xs-12 col-md-4 bambini-form-group form-group\" id=\"eta-bambini-camera-camera-" + i + "\">\
                </div>\
                <div class=\"clearfix\"></div>\
                ";
                $('.tipo-camere').append(templateCamere);
            }
        });

        $('.preventivo').on('change', '.camera-select', function (event) {
            var idCamera = event.target.id;
            var tipoCamera = $(event.target).val();

            $('#bambini-' + idCamera).empty();

            var templateBambini = "\
                <label for=\"bambini-camera-" + i + "\">\
                    Numero bambini\
                </label>\
                <select id=\"bambini-camera-" + idCamera + "\" name=\"bambini-camera-" + idCamera + "\" class=\"bambini-select form-control\" required=\"\">";
            for (var i = 0; i <= tipoCamera - 1; i++) {
                templateBambini += "<option value=\"" + i + "\"> " + i + " </option>";
            }
            templateBambini += "\
                </select>";

            $('#bambini-' + idCamera).append(templateBambini);
        });

        $('.preventivo').on('change', '.bambini-select', function (event) {
            var idBambini = event.target.id;
            var numeroBambini = $(event.target).val();
            $('#eta-' + idBambini).empty();
            colLength = 12 / numeroBambini;

            for (var i = 1; i <= numeroBambini ; i++) {
                var templateEta = "\
                <div class=\"col-xs-" + colLength + "\">\
                    <label for=\"" + i + "-" + idBambini + "\">\
                        Età\
                    </label>\
                    <select id=\"eta-" + i + "-" + idBambini + "\" name=\"eta-" + i + "-" + idBambini + "\" class=\"bambini form-control\" required=\"\">";
                for (var eta = 1; eta <= 18; eta++) {
                    templateEta += "<option value=\"" + eta + "\"> " + eta + " </option>";
                }
                templateEta += "\
                    </select>\
                </div>";
                $('#eta-' + idBambini).append(templateEta);
            }
        });
    });
</script>

<?php require '../Librerie/ges_html_bot.php'; ?>

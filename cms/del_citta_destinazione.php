<?php

require '../Librerie/connect.php';

$Titolo = "Annullamento Città";
$Tavola= "citta_destinazione";

$indietro = "vis_citta_destinazione.php?id=".$_GET['id_padre'];

if (isset($_GET['id'])) {
    db_delete($Tavola, $_GET['id']);
    header("Location: $indietro");
    exit;
}
header("Location: $indietro");
exit;

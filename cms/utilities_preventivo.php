<?php
/**
 * Crea un array con tutti i
 * dati presi dalla POST request
 * @param  array $data POST data
 * @return array       the well formed array
 */
/* Esempio:
{
  "int": "Gianluca Rigoletti",
  "mail": "gianluca@www.com",
  "phone": "1234567890",
  "desc": "Something",
  "markup": "100",
  "markup_type": "1"
  "begin_date": "Invalid date",
  "end_date": "Invalid date",
  "rooms_number": "4",
  "rooms": [
    {
      "type": "1",
      "child_number": null,
      "childs": []
    },
    {
      "type": "2",
      "child_number": "0",
      "childs": []
    },
    {
      "type": "3",
      "child_number": "1",
      "childs": [
        "4"
      ]
    },
    {
      "type": "4",
      "child_number": "3",
      "childs": [
        "3",
        "5",
        "7"
      ]
    }
  ]
}
*/
function parse_post_data($data)
{
    $prettified_data = array();
    $handle = fopen('log.txt', 'a');

    $prettified_data['int'] = $data['NOME_INT'];
    $prettified_data['mail'] = $data['MAIL_INT'];
    $prettified_data['phone'] = $data['TEL_INT'];
    $prettified_data['desc'] = $data['DESCRIZIONE'];
    $prettified_data['markup'] = db_convnum($data['MARKUP']);
    $prettified_data['markup_type'] = $data['MARKUPTYPE'];
    $prettified_data['confermato'] = $data['CONFERMATO'];
    $prettified_data['note'] = $data['NOTE'];
    $prettified_data['cambio'] = db_convnum($data['CAMBIO']);
    

    $dates = $data['reservation'];
    $dates_array = explode(' ', $dates);
    $begin_date_string = trim($dates_array[0]);
    $end_date_string   = trim($dates_array[2]);
    $prettified_data['begin_date'] = date('Y-m-d', strtotime($begin_date_string));
    $prettified_data['end_date'] = date('Y-m-d', strtotime($end_date_string));

    $rooms_number = $data['numero-camere'];
    $prettified_data['rooms_number'] = $rooms_number;

    $rooms = array();
    for ($i=1; $i <= $rooms_number; $i++) {
        $room = array();
        $room_type = $data["camera-$i"]; // 1, 2, 3, 4 : singola, doppia, etc.
        $room['type'] = $room_type;

        $child_number = isset($data["bambini-camera-camera-$i"]) ? $data["bambini-camera-camera-$i"] : 0;
        $room['child_number'] = $child_number;

        $childs = array();
        for ($c=1; $c <= $child_number; $c++) {
            $childs[] = $data["eta-$c-bambini-camera-camera-$i"];
        }
        $room['childs'] = $childs;

        $rooms[] = $room;
    }
    $prettified_data['rooms'] = $rooms;
    fwrite($handle, json_encode($prettified_data));
    fclose($handle);

    return $prettified_data;
}

/**
 * Crea un array come la funzione precedente
 * ma a partire dall'id dell'ordine
 * @param   $id     id dell'ordine
 * @param   $tavola ordine
 * @return array    array beautificato
 */
function parse_post_data_from_db($id, $tavola='ordine')
{
    $prettified_data = array();
    $ordine = mysql_fetch_assoc(db_query_mod($tavola, $id));

    $prettified_data['int'] = $ordine['NOME_INT'];
    $prettified_data['mail'] = $ordine['MAIL_INT'];
    $prettified_data['phone'] = $ordine['TEL_INT'];
    $prettified_data['desc'] = $ordine['DESCRIZIONE'];
    $prettified_data['markup'] = $ordine['MARKUP'];
    $prettified_data['markup_type'] = $ordine['MARKUPTYPE'];
    $prettified_data['begin_date'] = $ordine['DATAIN'];
    $prettified_data['end_date'] = $ordine['DATAOUT'];
    $prettified_data['confermato'] = $ordine['CONFERMATO'];
    $prettified_data['note'] = $ordine['NOTE'];

    $query_camere = db_query_generale('ordine_camere', 'IDORDINE = '.$id, 'ID');
    $camere = array();
    $numero_camere = 0;
    while ($camere = mysql_fetch_assoc($query_camere)) {
        $numero_camere++;
        $camera = array();
        $camera['type'] = $camere['TIPO'];
        $childs = (!empty($camere['CHILDAGE'])) ? explode(';', $camere['CHILDAGE']) : array();
        $child_number = sizeof($childs);
        foreach ($childs as $key => $child_age) {
            $childs[] = $child_age;
        }
        $camera['child_number'] = $child_number;
        $camera['childs'] = $childs;
        $prettified_data['rooms'][] = $camera;

    }
    $prettified_data['rooms_number'] = $numero_camere;

    return $prettified_data;
}

/**
 * Imposta la query per la ricerca dei vari giorni.
 * @param array $data dati che vengono presi elaborati
 * dal form vis_preventivo
 *
 * @return string query da eseguire
 */
function set_query($data)
{
    $sql = "";
    $data; // Genera un array

    // Controllo quali prezzi devo prendere
    $prezzo_1 = '';
    $prezzo_2 = '';
    $prezzo_3 = '';
    $prezzo_4 = '';
    foreach ($data['rooms'] as $room) {
        switch ($room['type']) {
            case '1':
            $prezzo_1 = 'PREZZO_1,';
                break;
            case '2':
            $prezzo_2 = 'PREZZO_2,';
                break;
            case '3':
            $prezzo_3 = 'PREZZO_3,';
                break;
            case '4':
            $prezzo_4 = 'PREZZO_4,';
                break;

            default:
                break;
        }
    }
    $prezzi = "$prezzo_1 $prezzo_2 $prezzo_3 $prezzo_4,";
    $prezzi = substr($prezzi, 0, -1);

    $sql .= "
    SELECT t.ID as IDTARIFFA, h.ID as IDSERVIZIO, h.STARRANK as STARRANK, t.FORNITORE as FORNITORE, t.DESCRIZIONE, h.CODICE, h.NOME, $prezzi c.DESCRIZIONE
    FROM servizi_hotel AS h, servizi_hotel_tariffe AS t, citta AS c
    WHERE t.IDHOTEL = h.ID AND h.IDCITTA = c.ID AND
    ";

    return $sql;
}

/**
 * Crea un array di date e giorni
 * @param  string $dates intervallo di date preso da daterangepicker
 * @return array  array con date e giorni
 */
function split_into_pages($dates = '2014-06-04T22:00:00.000Z 2014-06-10T21:59:59.999Z')
{
    // Esempio di date che si aspetta:
    // 2014-06-04T22:00:00.000Z 2014-06-10T21:59:59.999Z
    // Assumo che la data di begin sia sempre minore
    // di quella di end
    $days_and_pages = array();
    $days_array = explode(' ', $dates);
    $begin_date_string = $days_array[0];
    $end_date_string   = $days_array[2];

    $begin_date_object = date_create($begin_date_string);
    $end_date_object   = date_create($end_date_string);

    $days_number = date_diff($begin_date_object, $end_date_object)->format('%a');

    for ($i=0; $i <= $days_number; $i++) {
        $date = $begin_date_object->format('Y-m-d');
        $day_and_page = array(
            'day'  => $i+1,
            'date' => $date
        );
        $days_and_pages[] = $day_and_page;
        $begin_date_object->modify("+1 day");
    }

    return $days_and_pages;
}

/**
 * Restituisce il numero di persone per un
 * determinato ordine
 * @param  array $post_data dati parsati
 * @return int            numero di persone
 */
function get_number_pax($post_data)
{
    $number = 0;
    foreach ($post_data['rooms'] as $room) {
        $number += $room['type'];
    }

    return $number;
}

/**
 * Calcola il prezzo comprensivo di markup
 * @param  int $id_ordine l'id dell'ordine
 * @return double         prezzo calcolato
 */
function calcola_prezzo($id_ordine)
{
    $ordine = mysql_fetch_assoc(db_query_mod('ordine', $id_ordine));
    $camere = array();
    $query_camera = db_query_generale('ordine_dett', 'IDORDINE = '.$id_ordine);
    $prezzo = 0;
    $prezzo_totale = 0;

    while ($camera = mysql_fetch_assoc($query_camera)) {
        $camere[] = $camera;
        //Controllo se la riga del dettaglio è un servizio libero
        if ($camera['IDTARIFFA'] == NULL || $camera['IDTARIFFA'] == 'NULL' || db_is_null($camera['IDTARIFFA'])) {
            $quantita = $camera['PAX_SISTEMATI'];
            $prezzo += $camera['PAX_SISTEMATI'] * $camera['PREZZO'];
        } else {
            $prezzo += $camera['PREZZO'];            
        }
    }

    if ($ordine['MARKUPTYPE'] == 1) { //percentuale
        $prezzo_totale += $prezzo + $ordine['MARKUP']/100 * $prezzo;
    } else {
        $prezzo_totale += $prezzo + $ordine['MARKUP'];
    }

    $prezzi = array(
        'prezzo' => $prezzo,
        'prezzo_totale' => $prezzo_totale
    );

    return $prezzi;
}

/**
 * Restituisce un array con data di inizio
 * e data di fine a partire da una stringa
 * del tipo 'YYYY-MM-DD - YYYY-MM-DD'
 * @param  string $date stringa date
 * @return array
 */
function parse_date_from_post($date)
{
    $dates = array();
    $dates_array = explode(' ', $date);
    $dates['BEGIN'] = $dates_array[0];
    $dates['END'] = $dates_array[2];

    return $dates; 

}

/**
 * Restituisce, se presente, un oggetto
 * DatePeriodo contentente le date che si 
 * accavallano tra due intervalli.
 * @param  string $start_date_1 data inizio primo intervallo
 * @param  string $end_date_1   data fine primo intervallo
 * @param  string $start_date_2 data inizio secondo intervallo
 * @param  string  $end_date_2   data fine secondo intervallo
 * @return DatePeriod               intervallo di date
 */
function get_overlap_dates($start_date_1, $end_date_1, $start_date_2, $end_date_2)
{
    // http://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
    if ( ($start_date_1  >= $end_date_2) || ($end_date_1 <=  $start_date_2)) { // non si intersecano
        return false;
    }

    $date_interval = new DateInterval('P1D'); // Stabilisco un periodo di un giorno Period1Day
    $date_overlap_interval = null;
    // Stabilisco l'ordine degli intervalli
    if ($start_date_1 < $start_date_2) {
        $start_date_object_1 = new DateTime($start_date_1);
        $end_date_object_1 = new DateTime($end_date_1);
        $start_date_object_2 = new DateTime($start_date_2);
        $end_date_object_2 = new DateTime($end_date_2);
    } else {
        $start_date_object_1 = new DateTime($start_date_2);
        $end_date_object_1 = new DateTime($end_date_2);
        $start_date_object_2 = new DateTime($start_date_1);
        $end_date_object_2 = new DateTime($end_date_1);
    }
    // Trovo il periodo di intersezione
    $date_overlap_interval = new DatePeriod($start_date_object_2, $date_interval, $end_date_object_1);
    return $date_overlap_interval;

}

/**
 * Restituisce, se presente, l'insieme
 * dei giorni mancanti compresi tra due
 * intervalli di date.
 * @param  string $start_date_1 data inizio primo intervallo
 * @param  string $end_date_1   data fine primo intervallo
 * @param  string $start_date_2 data inizio secondo intervallo
 * @param  string  $end_date_2   data fine secondo intervallo
 * @return DatePeriod               intervallo di date
 */
function get_missing_dates($start_date_1, $end_date_1, $start_date_2, $end_date_2)
{
    // http://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
    if ( ($start_date_1 < $end_date_2) && ($end_date_1 >= $start_date_2)) { // si intersecano
        return false;
    }

    $date_interval = new DateInterval('P1D'); // Stabilisco un periodo di un giorno Period1Day
    $date_overlap_interval = null;
    // Stabilisco l'ordine degli intervalli
    if ($start_date_1 < $start_date_2) {
        $start_date_object_1 = new DateTime($start_date_1);
        $end_date_object_1 = new DateTime($end_date_1);
        $start_date_object_2 = new DateTime($start_date_2);
        $end_date_object_2 = new DateTime($end_date_2);
    } else {
        $start_date_object_1 = new DateTime($start_date_2);
        $end_date_object_1 = new DateTime($end_date_2);
        $start_date_object_2 = new DateTime($start_date_1);
        $end_date_object_2 = new DateTime($end_date_1);
    }
    // Trovo il periodo di intersezione
    $date_overlap_interval = new DatePeriod($end_date_object_1, $date_interval, $start_date_object_2);
    return $date_overlap_interval;

}

/**
 * Funzione che controlla se ci sono errori
 * nelle date e restituisce un booleano
 * corrispondete.
 * @param  int $order_id 
 * @return boolean true se è ok, false altrimenti
 */
function dates_ok($order_id)
{
    // Controllo per vedere se è tutto a posto
    if (check_valid_dates($order_id)) {
        return true;
    }
    return false;
}

// Funzione che restituisce true se tutto è
// a posto, altrimenti resituisce un array con
// type 'missing' o 'overlap' e content un
// array di oggetti datetime 
function manage_mismatching_dates($order_id)
{
    $errors = array();
    $ordine = mysql_fetch_assoc(db_query_mod('ordine', $order_id));
    $interval =  new DateInterval('P1D');   
    $begin_date = new DateTime($ordine['DATAIN']);
    $end_date = new DateTime($ordine['DATAOUT']);
    $dettagli_date = get_dett_date($order_id);
    sort_dates($dettagli_date);
    if (empty($dettagli_date)) {
        $period = new DatePeriod($begin_date, $interval, $end_date);
        $errors[] = array(
            'type' => 'missing',
            'content' => $period
        );
        return $errors;
    }
     $end = end($dettagli_date); 
    $begin_dett_date = new DateTime($dettagli_date[0]['begin']);
    $end_dett_date = new DateTime($end['end']);
    // Determino il tipo di errore
    $rows_number = count($dettagli_date);
    if ($begin_date != $begin_dett_date) {
        $period = new DatePeriod($begin_date, $interval, $begin_dett_date);
        $errors[] = array(
            'type' => 'missing',
            'content' => $period
        );
    }
    if ($end_date != $end_dett_date) {
        $period = new DatePeriod($end_dett_date, $interval, $end_date);
        $errors[] = array(
            'type' => 'missing',
            'content' => $period
        );
    }
    for ($i=1; $i < $rows_number; $i++) { 
        $date1_begin = $dettagli_date[$i-1]['begin'];
        $date1_end = $dettagli_date[$i-1]['end'];
        $date2_begin = $dettagli_date[$i]['begin'];
        $date2_end = $dettagli_date[$i]['end'];
        $is_overlapping = get_overlap_dates($date1_begin, $date1_end, $date2_begin, $date2_end);
        if ($is_overlapping) {
            $errors[] = array(
                'type' => 'overlap',
                'content' => $is_overlapping
            );
        }
        $is_missing = get_missing_dates($date1_begin, $date1_end, $date2_begin, $date2_end);
        if ($is_missing) {
            $errors[] = array(
                'type' => 'missing',
                'content' => $is_missing
            );
        }
    }

    return $errors;
}

/**
 * Funzione che dato un array con le date
 * di inizio e fine dei dettagli ordine
 * restituisce un array ordinato secondo
 * le date di inizio di ogni riga del 
 * dettaglio
 * @param  array $dates 
 * @return array
 */
function sort_dates(&$dates)
{
    usort($dates, function($date1, $date2) {
        $t1 = strtotime($date1["begin"]);
        $t2 = strtotime($date2["begin"]);

        return ($t1 - $t2);
    });
}

/**
 * Funzione che prende l'id dell'ordine
 * e restiuisce un booleano per determinare
 * se è tutto a posto con le date dei dettagli
 *             
 * @param  int $order_id l'id dell'ordine
 * @return boolean true se tutto è ok. False altrimenti
 */
function check_valid_dates($order_id)
{
    $dettagli_date = get_dett_date($order_id);
    $date_range = get_dates_in_order($order_id);
    $ordine = mysql_fetch_assoc(db_query_mod('ordine', $order_id));
    $begin_date = new DateTime($ordine['DATAIN']);
    $end_date = new DateTime($ordine['DATAOUT']);
    // Controllo se c'è almeno una riga di dettaglio
    if (empty($dettagli_date)) {
        return false;
    }
    // Ordino l'array dei dettagli:
    sort_dates($dettagli_date);
    // Controllo se la prima data coincide con la data di inizio 
    // e analogamente per la fine
    if ($begin_date != (new DateTime($dettagli_date[0]['begin']))) {
        return false;
    }
    $end = end($dettagli_date); 
    if ($end_date != (new DateTime($end['end']))) {
        return false;
    }
    // Controllo i giorni intermedi se compaiono 0 o 2  volte
    foreach ($date_range as $date) {
        $dates_found = 0;
        foreach ($dettagli_date as $dettaglio_date) {
            $date_begin = new DateTime($dettaglio_date['begin']);
            $date_end = new DateTime($dettaglio_date['end']);
            if ($date == $date_begin || $date == $date_end) {
                // Controllo di non contare le date agli estremi
                if ($date != $begin_date && $date != $end_date) {
                    $dates_found++;
                }
            }
        }
        if ($dates_found != 2 && $dates_found != 0) {
            return false;
        }
    }

    return true;
}

/**
 * Funzione che restituisce un array con
 * le date di inizio e fine delle righe
 * dettaglio per un determinato ordine
 * @param  int $order_id 
 * @return array
 */
function get_dett_date($order_id)
{
    // Prendo tutte le righe del dettaglio e le metto in un array
    $ordine_dettagli_date = array();
    $query_dett_ordine = db_query_generale('ordine_dett', 'IDORDINE = '.$order_id.' AND IDTARIFFA IS NOT NULL');
    while ($ordine = mysql_fetch_assoc($query_dett_ordine)) {
        $ordine_dettagli_date[] = array(
            'begin' => $ordine['BEGIN'],
            'end'   => $ordine['END']
        );
    }

    return $ordine_dettagli_date;
}

/**
 * Funzione che restituisce un oggetto 
 * DateRange contenente tutte le date
 * comprese in un determinato ordine.
 * @param  int $order_id
 * @return DateRange intervallo delle date.
 */
function get_dates_in_order($order_id)
{
    // Prendo l'intervallo di date
    $ordine = mysql_fetch_assoc(db_query_mod('ordine', $order_id));
    $date_interval = new DateInterval('P1D');
    $date_begin = new DateTime($ordine['DATAIN']);
    $date_end = new DateTime($ordine['DATAOUT']);
    $date_range = new DatePeriod($date_begin, $date_interval, $date_end);

    return $date_range;
}

/**
 * Funzione che dato un oggetto DatePeriod
 * resitituisce la prima data o ultima data. Questa 
 * funzione è un fix per la versione 5.3 di PHP
 * @param  DatePeriod $date intervallo di date
 * @param $type stringa 'begin' o 'end'
 * @return Date data iniziale
 */         
function get_begin_end_date(DatePeriod  $date_interval, $type)
{
    $dates_array = array();
    $i = 0;
    foreach ($date_interval as $date) {
        $dates_array[] = $date;
        $i++;
    }

    if ($type == 'begin') {
        return $dates_array[0];
    }
     $a = $dates_array[$i-1];

    return $a->modify('+1 day');
}

function get_begin_end_date_arr(DatePeriod  $date_interval)
{
    $dates_array = array();
    $i = 0;
    foreach ($date_interval as $date) {
        $dates_array[] = $date;
        $i++;
    } 
    $out = array();
    $out[0]=$dates_array[0];
    $out[1]=$dates_array[$i-1]->modify('+1 day');

    return $out;
}


/**
 * Restituisce un array ben formattato dei dettagli per il
 * preventivo
 * @param  integer  $id_ordine
 * @param  integer $id_servizio_libero_macchina
 * @return array
 */
function crea_array_preventivo($id_ordine, $id_servizio_libero_macchina = 2)
{
    /*
    * Definire quali dati inserire nell'"oggetto" preventivo:
        * Descrizione
        * Dati intestatario
        * Note
        * Data inizio e fine
        * Se confermato == sì si ciamerà programma di viaggio altrimenti preventivo
        * Righe con gli stessi dati di vis_dettaglio_ordini.php
        * Il prezzo di ogni riga può o può non essere visualizzato in base ad un
        parametro arbitrario
        * In fondo il prezzo con markup
        * Per ogni città va preso il campo atlante e visualizzato sotto la riga del
        dettaglio che ha proprio quella città
        * I servizi liberi vanno sotto le città che hanno la stessa data
     */
    $ordine = mysql_fetch_assoc(db_query_mod('ordine', $id_ordine));
    $descrizione = $ordine['DESCRIZIONE'];
    $nome_int = $ordine['NOME_INT'];
    $mail_int = $ordine['MAIL_INT'];
    $tel_int = $ordine['TEL_INT'];
    $note = $ordine['NOTE'];
    $data_in = $ordine['DATAIN'];
    $data_out = $ordine['DATAOUT'];
    $id = $ordine['ID'];
    $cambio = $ordine['CAMBIO'];

    if ($ordine['CONFERMATO'] == 1) {
        $tipo = 'Preventivo';
    } else {
        $tipo = 'Programma di viaggio';
    }

    // Calcolo le righe e le metto in un array
    $dettagli_ordine = array();
    $servizi_liberi = array();
    $res_dettagli_ordine = db_query_generale('ordine_dett', ' IDORDINE = '.$id_ordine, 'BEGIN');
    while ($res_dettaglio_ordine = mysql_fetch_assoc($res_dettagli_ordine)) {

        // Prendo la citta e l'atlante corrispondente alla riga in questione:
        $servizio_hotel = mysql_fetch_assoc(db_query_mod('servizi_hotel', $res_dettaglio_ordine['IDSERVIZIO']));
        $res_citta = mysql_fetch_assoc(db_query_mod('citta', $servizio_hotel['IDCITTA']));
        
        $servizio_libero = array();
        $dettaglio_ordine = array();
        $is_servizio_libero = ($res_dettaglio_ordine['IDTARIFFA'] == NULL || $res_dettaglio_ordine['IDTARIFFA'] == 'NULL' || db_is_null($res_dettaglio_ordine['IDTARIFFA']));

        // Sistemo il prezzo a seconda che il markup sia percentuale o assoluto
        $prezzi = calcola_prezzo($id_ordine);
        $prezzo_no_markup = $prezzi['prezzo'];
        $prezzo_riga = 0;
        if ($ordine['MARKUPTYPE'] == 0) { // assoluto
            $prezzo_riga = $res_dettaglio_ordine['PREZZO'];
            $prezzo_relativo = $prezzo_riga/$prezzo_no_markup;
            $markup_riga = $prezzo_relativo * $ordine['MARKUP'];
            $prezzo_riga += $markup_riga;
        } else {
            $prezzo_riga = $res_dettaglio_ordine['PREZZO'] +
                $ordine['MARKUP']/100 * $res_dettaglio_ordine['PREZZO'];
        }

        // Controllo se la riga è un servizio libero
        if ($is_servizio_libero) {
            $quantita = $res_dettaglio_ordine['PAX_SISTEMATI'];
            $servizio_libero = array(
                'descrizione' => $res_dettaglio_ordine['DESCRIZIONE'],
                'begin' => $res_dettaglio_ordine['BEGIN'],
                'end' => $res_dettaglio_ordine['END'],
                'prezzo' => $prezzo_riga,
                'id_servizio' => $res_dettaglio_ordine['IDSERVIZIO'],
                'quantita' => $quantita
            );
            $servizi_liberi[] = $servizio_libero;
        } else {
            $dettaglio_ordine = array();
            $dettaglio_ordine['idcitta'] = $res_citta['ID'];
            $dettaglio_ordine['citta'] = $res_citta['DESCRIZIONE'];
            $dettaglio_ordine['atlante'] = $res_citta['ATLANTE'];
            $dettaglio_ordine['descrizione'] = $res_dettaglio_ordine['DESCRIZIONE'];
            $dettaglio_ordine['begin'] = $res_dettaglio_ordine['BEGIN'];
            $dettaglio_ordine['end'] = $res_dettaglio_ordine['END'];
            $dettaglio_ordine['note'] = $res_dettaglio_ordine['NOTE'];
            $dettaglio_ordine['idservizio'] = $res_dettaglio_ordine['IDSERVIZIO'];
            $dettaglio_ordine['prezzo'] = $prezzo_riga;
            $dettagli_ordine[] = $dettaglio_ordine;
        }
    }

    // Aggiungo alle righe del dettaglio i relativi servizi liberi
    foreach($servizi_liberi as $servizio_libero) {
        /*
        Se il servizio libero è di tipo `macchina` (definirsi un id fisso nel db),
        allora devo inserire le informazioni su come raggiungere le città adiacenti
        usando la macchina. Per farlo, bisogna controllare e usare il valore del campo
        `atlante` nella tabella `citta_destinazione`, verificando una corrispondenza
        con `idcittafrom` e `idcittato` delle due città adiacenti.
         */
        if ($servizio_libero['id_servizio'] == $id_servizio_libero_macchina) {
            // Creo un array di tutte le date per cui l'auto è stata selezionata
            $date_begin = new DateTime($servizio_libero['begin']);
            $date_end = new DateTime($servizio_libero['end']);
            $date_end->modify('+1 day');
            $date_interval = new DateInterval('P1D');
            $date_period = new DatePeriod($date_begin, $date_interval, $date_end);
            $dates_array = array();
            foreach($date_period as $date) {
                $dates_array[] = $date;
            }
            // Itero su tutte le date per cui l'auto è stata selezionata.
            // Poi per ogni data devo cercare la città attuale e la
            // città del giorno dopo
            foreach ($dates_array as $date) {
                // Cerco una corrispondenza tra la data del servizio su cui sto iterando
                // e le date in cui ho le righe degli ordini
                foreach ($dettagli_ordine as $key => $dettaglio_ordine) {
                    $date_string = $date->format('Y-m-d');
                    $found = array_search($date_string, $dettaglio_ordine);
                    // Se trovo la corrispondenza eseguo la query sulle città adiacenti
                    if ($found) {
                        // Controllo qual è l'ultima riga e prendo l'ultima citta.
                        // Lo faccio perchè sennò usando $dettagli_ordine[$key +1];
                        // rischio di avere qualche errore php
                        $keys_dettagli_ordine = array_keys($dettagli_ordine);
                        $last_key_dettaglio = array_pop($keys_dettagli_ordine);
                        if ($key != $last_key_dettaglio) {
                            $id_citta_from = $dettaglio_ordine['idcitta'];
                            $id_citta_to = $dettagli_ordine[$key + 1]['idcitta'];
                            $res_citta_destinazione = mysql_fetch_assoc(db_query_generale('citta_destinazione',
                                " IDCITTAFROM = $id_citta_from AND IDCITTATO = $id_citta_to ", 'id'));
                            // Appendo le informazioni contenute nel campo atlante
                            $dettagli_ordine[$key]['atlante'] = $res_citta_destinazione['ATLANTE'];
                        }
                    }
                }
            }
        }

        // Aggiunto i servizi liberi alle rispettive righe di prenotazione
        // basandomi sulla data di inizio
        foreach ($dettagli_ordine as $key => $riga_dettaglio) {
            $id_dettaglio = array_search($servizio_libero['begin'], $riga_dettaglio);
            if ($id_dettaglio && $riga_dettaglio['end'] != $servizio_libero['begin']) {
                $dettagli_ordine[$key]['servizi_liberi'][] = $servizio_libero;
            }
        }
    }

    // Calcolo il prezzo totale
    $prezzo_totale = calcola_prezzo($id_ordine);
    // restituisco l'array relativo all'ordine
    return  array(
        'id'          => $id,
        'tipo'        => $tipo,
        'descrizione' => $descrizione,
        'nome_int'    => $nome_int,
        'mail_int'    => $mail_int,
        'tel_int'     => $tel_int,
        'note'        => $note,
        'datain'      => $data_in,
        'dataout'     => $data_out,
        'prezzo'      => $prezzo_totale,
        'cambio'      => $cambio,
        'dettaglio'   => $dettagli_ordine
    );

}

<?php
$m="citta";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';

$titolo    = "Gestione Città";
$tavola    = "citta";
$risultato = db_query_vis($tavola, 'ID');


require '../Librerie/ges_html_top.php';
?>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Gestione Città </h2>
      <ul class="nav navbar-right panel_toolbox">
        <button class="btn btn-round btn-primary" type="button" onclick="location.href='ges_citta.php?p_upd=0'">Nuovo</button>
      </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
          <tr>
          <th width="20%"> Codice </th>
          <th width="20%"> Descrizione </th>
          <th width="20%"> Nazione </th>
          <th  width="5%"> &nbsp;</th>
          <th  width="5%"> &nbsp;</th>
          <th  width="5%"> &nbsp;</th>
        </thead>  
        <tbody> 
        <?php
        while ($cur_rec = mysql_fetch_assoc($risultato)) {
            if (!db_is_null($cur_rec['IDNAZIONE'])) 
            $nazione = mysql_fetch_assoc(db_query_generale('nazioni', 'ID = '.$cur_rec['IDNAZIONE']));
            echo " 
            <tr >
              <td >".$cur_rec['CODICE']." </td>
              <td >".$cur_rec['DESCRIZIONE']." </td>
              <td >".$nazione['CODICE']." </td>
              <td ><a href=\"ges_citta.php?p_upd=1&p_id=".$cur_rec['ID']."\"><i class=\"fa fa-edit\"></i></a></td>
              <td ><a href=\"vis_citta_destinazione.php?id=".$cur_rec['ID']."\"><i class=\"fa fa-car\"></i></a></td>
              <td ><a href=\"Javascript:ut_delete_rec(".$cur_rec['ID'].",'del_citta')\"><i class=\"fa fa-trash\"></i></a>
            </tr> ";
        }
        ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#datatable-responsive').DataTable({
      "bFilter":true,
      "iDisplayLength": 50,
      "aaSorting": [[ 2, "asc" ]], 
      "bStateSave":true,                 
      "aoColumns": [
        { "bSortable": false },
        { "bSortable": false }, 
        { "bSortable": false },
        null,
        null,
        null
      ]         
    });
  });
</script>  

<?php require '../Librerie/ges_html_bot.php'; ?>

<?php

require '../Librerie/connect.php';

$Titolo = "Annullamento Città";
$Tavola= "citta";

$indietro = "vis_citta.php";

if (isset($_GET['id'])) {
    db_delete($Tavola, $_GET['id']);
    header("Location: $indietro");
    exit;
}
header("Location: $indietro");
exit;

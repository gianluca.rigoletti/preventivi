<?php
$m="pax";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';

$titolo    = "Gestione Pax";
$tavola    = "pax";
$risultato = db_query_vis($tavola, 'ID');


require '../Librerie/ges_html_top.php';
?>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Gestione Pax </h2>
      <ul class="nav navbar-right panel_toolbox">
        <button class="btn btn-round btn-primary" type="button" onclick="location.href='ges_pax.php?p_upd=0'">Nuovo</button>
      </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
          <tr>
          <th width="20%"> Nome </th>
          <th width="20%"> Cognome </th>
          <th width="20%"> Età </th>
          <th  width="5%"> &nbsp;</th>
          <th  width="5%"> &nbsp;</th>
        </thead>  
        <tbody> 
        <?php
        while ($cur_rec = mysql_fetch_assoc($risultato)) {
            echo " 
            <tr >
              <td >".$cur_rec['NOME']." </td>
              <td >".$cur_rec['COGNOME']." </td>
              <td >".$cur_rec['ETA']." </td>
              <td ><a href=\"ges_pax.php?p_upd=1&p_id=".$cur_rec['ID']."\"><i class=\"fa fa-edit\"></i></a></td>
              <td ><a href=\"Javascript:ut_delete_rec(".$cur_rec['ID'].",'del_pax')\"><i class=\"fa fa-trash\"></i></a>
            </tr> ";
        }
        ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#datatable-responsive').DataTable({
      "bFilter":true,
      "iDisplayLength": 50,
      "aaSorting": [[ 2, "asc" ]], 
      "bStateSave":true,                 
      "aoColumns": [
        { "bSortable": false },
        { "bSortable": false },
        { "bSortable": false },
        null,
        null
      ]         
    });
  });
</script>  

<?php require '../Librerie/ges_html_bot.php'; ?>


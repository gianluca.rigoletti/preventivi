<?php

require '../Librerie/connect.php';

$Titolo = "Annullamento Pax";
$Tavola= "pax";

$indietro = "vis_pax.php";

if (isset($_GET['id'])) {
    db_delete($Tavola, $_GET['id']);
    header("Location: $indietro");
    exit;
}
header("Location: $indietro");
exit;

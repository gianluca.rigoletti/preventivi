<?php

require '../Librerie/connect.php';

header('content-type:application/json');


$messages = array();
uploadFile();
echo json_encode($messages);

/**
 * Scrive in status.json il valore 
 * 
 * @param integer $value percentuale di completamento
 */
function setStatusOfRequest($value)
{
    if ($value === 100) {
        $value = 'done';
    }
    $request_progress = fopen('status.json', 'w');
    fwrite($request_progress, json_encode($value));
    fclose($request_progress);
}

/**
 * Aggiunge le slashes le escape dei caratteri
 * per ogni valore dell'array
 * @param  array $array l'array a cui aggiungere gli slas
 *
 * @return array 
 */
function addSlashesToArray($array)
{
    $slashed_array = array();
    foreach ($array as $key => $value) {
        $slashed_array[] = addslashes($value);
    }
    return $slashed_array;
}

/**
 * Inserisce un messaggio nell'array di messaggi.
 * L'array viene modificato epr referenza.
 * 
 * @param string $type     il tipo di messaggio: error, success, info
 * @param string $content  il contenuto del messaggio
 *
 * @return json oggetto json di risposta
 */
function setMessage($type, $content)
{
    global $messages;
    $push = array(
        'type' => $type, 
        'content' => $content
    );
    array_push($messages, $push);
}

function sanitizePhone($phone)
{
    $sanitized_phone = preg_replace('/[^0-9]/', '', $phone);
    return $sanitized_phone;
}


/**
 * Restituisce una stringa con i numeri separati da ;
 * data la stringa dei giorni. La stringa dei giorni
 * può essere formattata nei seguenti due modi
 * 'start_day - end_day' che indica un intervallo
 * 'start_day & end_day' che indica una coppia
 * @param string $days_string la stringa con i giorni
 */
function setDays($days_string)
{
    $days = array(
        'SUN' => 0,
        'MON' => 1,
        'TUE' => 2,
        'WED' => 3,
        'THURS' => 4,
        'FRI' => 5,
        'SAT' => 6
    );
    // Controllo il tipo di separatore dei giorni
    $days_string_array = array();
    $days_parsed_string = '';

    if (strpos($days_string, '&')) {
        $days_string_array = array_map('strtoupper',array_map('trim', explode('&', $days_string)));

        foreach ($days_string_array as $key => $value) {
            $days_parsed_string .= "{$days[$value]};";
        }
    } else {
        $days_string_array = array_map('strtoupper',array_map('trim', explode('-', $days_string)));
        // Assumo che l'intervallo sia uno soltanto
        $start_day = $days_string_array[0];
        $end_day = $days_string_array[1];
        for ($i=0; $i < 7; $i++) { 
            if ($days[$start_day] > $days[$end_day]) {
                // Intervallo esterno
                if ($i >= $days[$start_day] || $i <= $days[$end_day]) {
                    $days_parsed_string .= "{$i};";
                }
            } else {
                // Intervallo interno
                if ($i >= $days[$start_day] && $i <= $days[$end_day]) {
                    $days_parsed_string .= "{$i};";
                }
            }
        }
    }

    return $days_parsed_string;

}

/**
 * Aggiunge i ";" ad una sequenza di numeri
 * inviati come stringa
 * @param string $days i giorni senza spazi
 * @return string la stringa formattata
 */
function addCommaToDays($days)
{
    $days_array = str_split($days, "1");
    $modified_string = implode(";", $days_array);
    return $modified_string;
}

/**
 * Prende una colonna, toglie gli 
 * spazi bianchi da ogni riga.
 *
 * @param  array $data  I dati su cui filtrare
 * @param  integer $number_of_column la colonna da filtrare
 * @param boovlean $removev_duplicates se trvue rimuove i valori 
 * @return array  la colonna filtrata
 */
function filterColumn($data, $number_of_column, $remove_empty_values = false)
{
    if ($remove_empty_values) {
        return array_filter(array_map('trim', array_column($data, $number_of_column)));
    }
    return array_map('trim', array_column($data, $number_of_column));
}

/**
 * Inserisce i valori non duplicati delle zone 
 * e delle citta nel db
 * @param  array $column colonna di valori unici da inserire
 * @return boolean
 */
function uploadNazioniZone($column, $type)
{

    // Ottimizzazione

    $chunk_size = 100;
    $column = array_chunk($column, $chunk_size);

    for ($i=0; $i < sizeof($column); $i++) { 
        $sql = ' INSERT INTO '.$type.' (ID, CODICE, DESCRIZIONE) VALUES ';
        for ($c=0; $c < sizeof($column[$i]); $c++) { 
           $sql .= "( null, '".$column[$i][$c]."', '".$column[$i][$c]."' ),";
        }
        $sql = substr($sql, 0, -1);
        $sql .= ' ON DUPLICATE KEY UPDATE
        ID = VALUES(ID), CODICE =  VALUES(CODICE),DESCRIZIONE = VALUES(DESCRIZIONE)';
        mysql_query($sql);
        if (mysql_error()) {
            setMessage('error', 'Problema nella query di '.$type.': '.$sql.' \n '.mysql_error());
            return false;
        }

    }   
    return true;
}

/**
 * Carica nel db i fornitori nella relativa tabella
 * @param  array $id_fornitori valori unici degli id da caricare
 * @param boolean $id_null se impostato a falsew  inserisce null nell'id del vendor
 * @return boolean             success/fail
 */
function uploadFornitori($fornitori, $id_null = true)
{
    $count = 0;
    $chunk_size = 100;
    $chunk_fornitori = array_chunk($fornitori, $chunk_size, true);

    for ($i=0; $i < sizeof($chunk_fornitori); $i++) { 
        $sql = ' INSERT INTO fornitori (ID, CODICE, DESCRIZIONE, IDTIPOSERVIZIO) VALUES ';
        foreach ($chunk_fornitori[$i] as $vendor_id => $vendor_name) {
            $vendor_id = ($id_null) ? $vendor_id : 'null' ;
            $sql .= "( $vendor_id , '$vendor_name', '$vendor_name', 1 ),";
        }
        $sql = substr($sql, 0, -1);
        $sql .= ' ON DUPLICATE KEY UPDATE 
        ID             = VALUES(ID),
        CODICE         = VALUES(CODICE),
        DESCRIZIONE    = VALUES(DESCRIZIONE),
        IDTIPOSERVIZIO = VALUES(IDTIPOSERVIZIO)';
        mysql_query($sql);
        if (mysql_error()) {
            setMessage('error', 'Problema nella query dei fornitori: '.$sql.' \n '.mysql_error());
            return false;
        }
    }
    return true;
}

/**
 * Ritorna i primi $n_characters. Utile per array map.    
 * @param  string $item l'elemento
 * @param string $n_characters il numero di caratteri
 * @return string l'elemento senza i primi n caratteri    
 */
function string_substr($item, $n_charaters = 5)
{
    return strtoupper(substr($item, 0, $n_charaters));
}

/**
 * Carica in servizi_hotel il file associato.
 *
 * @param  array &$data I dati da caricare
 * @return boolean  Se il caricamento e' avvenuto correttamente
 */
function uploadServizi(&$data)
{
    $chunk_size = 100;
    // Prima cosa devo filtrare le colonne:
    $id_fornitore_column     = array_chunk(array_column($data, 0),$chunk_size);
    $vendor_id               = array_column($data, 0); // Servono per ifornitori
    $vendor_name             = addSlashesToArray(array_column($data, 1));
    $vendor_id_column        = array_chunk($vendor_id, $chunk_size); // Servono per l'inserimento in servizi_hotel
    $vendor_name_column      = array_chunk($vendor_name, $chunk_size);
    //$starrank_column         = array_chunk(array_column($data, 2),$chunk_size);
    $codice_column           = array_chunk(addSlashesToArray(array_column($data, 3)),$chunk_size);
    $nome_column             = array_chunk(addSlashesToArray(array_column($data, 4)),$chunk_size);
    $descrizione_column      = array_chunk(addSlashesToArray(array_column($data, 5)),$chunk_size);
    $nazioni_column          = array_chunk(array_column($data, 6),$chunk_size);
    $citta_column            = array_chunk(addSlashesToArray(array_column($data, 7)),$chunk_size);
    $indirizzo_1_column      = array_chunk(addSlashesToArray(array_column($data, 8)),$chunk_size);
    $indirizzo_2_column      = array_chunk(addSlashesToArray(array_column($data, 9)),$chunk_size);
    $zip_column              = array_chunk(array_column($data, 10) ,$chunk_size);
    $phone_column            = array_chunk(array_column($data, 11) ,$chunk_size);
    $fax_column              = array_chunk(array_column($data, 12) ,$chunk_size);
    $img_column              = array_chunk(array_column($data, 13) ,$chunk_size);
    $lat_column              = array_chunk(array_column($data, 14) ,$chunk_size);
    $lang_column             = array_chunk(array_column($data, 15) ,$chunk_size);
    $childage_column         = array_chunk(array_column($data, 16) ,$chunk_size);
    $roomtype_column         = array_chunk(addSlashesToArray(array_column($data, 17)),$chunk_size);
    $mealplan_column         = array_chunk(addSlashesToArray(array_column($data, 18)),$chunk_size);
    $familyplan_column       = array_chunk(array_column($data, 19) ,$chunk_size);
    $maxocc_column           = array_chunk(array_column($data, 20) ,$chunk_size);
    $product_type_column     = array_chunk(array_column($data, 21) ,$chunk_size);
    $state_column            = array_chunk(array_column($data, 21) ,$chunk_size);
    $state_column            = array_chunk(array_column($data, 22) ,$chunk_size);
    $perm_room_code_column   = array_chunk(array_column($data, 23) ,$chunk_size);
    $non_rimborsabile_column = array_chunk(array_column($data, 24) ,$chunk_size);
    $fee_column              = array_chunk(array_column($data, 25) ,$chunk_size);
    $fee_type_column         = array_chunk(array_column($data, 25) ,$chunk_size);

    // Carico le nazioni nuove che trovo nei record in caso non esistano
    $nazioni_filtered = array_unique(array_values(filterColumn($nazioni_column, 1, true))); // Array con valori unici
    $upload_nazioni_ok = uploadNazioniZone($nazioni_filtered, 'nazioni');
    setMessage('success', 'Nazioni caricate correttamente');

    // Carico i fornitori che non sono già stati caricati. Devo passare un array associativo id => codice
    $fornitori = array_unique(array_map('trim', array_combine($vendor_id, $vendor_name)));
    $upload_fornitori_ok = uploadFornitori($fornitori);
    setMessage('success', 'Fornitori caricati correttamente');

    // Metodo alternativo di ottimizzazione:
    // Creo un chunk di array, da 100 alla volta. Poi
    // Creo la query in cui utilizzo on duplicate key update
    // Poi tante query quanti sono i chunks
    $column_size = sizeof($codice_column);

    for ($i=0; $i < $column_size; $i++) { 
        $percentage = 1/$column_size * 48;
        setStatusOfRequest($i * $percentage + 52);
        $sql = 'INSERT INTO servizi_hotel (ID, IDFORNITORE, IDVENDOR, VENDORNAME, CODICE, NOME, DESCRIZIONE,
            STARRANK, IDNAZIONE, IDCITTA, INDIRIZZO, INDIRIZZO2, ZIP, PHONE, FAX, MAIL, IMG, LAT, LANG,
            CHILDAGE, ROOMTYPE, MEALPLAN, FAMILYPLAN, MAXOCC, NONRIMBORSABILE, FEE, FEETYPE) VALUES ';
        for ($c=0; $c < sizeof($codice_column[$i]); $c++) {
            $sql_nazioni = " SELECT ID from nazioni WHERE CODICE = '".$nazioni_column[$i][$c]."'";
            $id_nazione = mysql_fetch_assoc(mysql_query($sql_nazioni))['ID'];
            $sql_citta = " SELECT ID from citta WHERE upper(REPLACE(CODICE, ' ', '')) 
                            LIKE upper(REPLACE('%{$citta_cod}%', ' ', ''))
                            OR upper(REPLACE(DESCRIZIONE, ' ', ''))
                            LIKE upper(REPLACE('%{$citta_desc}%')) LIMIT 1";
            $id_citta = mysql_fetch_assoc(mysql_query($sql_citta))['ID'];

            $id_nazione = (empty($id_nazione)) ? '0' : $id_nazione;
            $id_citta = (empty($id_citta)) ? '0' : $id_citta;

            $sql .= "( null, ".$id_fornitore_column[$i][$c].", ".$vendor_id_column[$i][$c].", '".$vendor_name_column[$i][$c]."', '".$codice_column[$i][$c]."', 
            '".$nome_column[$i][$c]."', '".$descrizione_column[$i][$c]."', '".$starrank_column[$i][$c]."', ".$id_nazione.", ".$id_citta.", 
            '".$indirizzo_1_column[$i][$c]."', '".$indirizzo_2_column[$i][$c]."', '".$zip_column[$i][$c]."', '".$phone_column[$i][$c]."', '".$fax_column[$i][$c]."',
            '', '".$img_column[$i][$c]."', '".$lat_column[$i][$c]."', '".$lang_column[$i][$c]."', '".$childage_column[$i][$c]."', '".$roomtype_column[$i][$c]."',
            '".$mealplan_column[$i][$c]."', '".$familyplan_column[$i][$c]."', '".$maxocc_column[$i][$c]."', '".$non_rimborsabile_column[$i][$c]."', '".$fee_column[$i][$c]."',
            '".$fee_type_column[$i][$c]."' ),";
        }
        $sql = substr($sql, 0, -1);
        $sql .= ' ON DUPLICATE KEY UPDATE 
        ID = VALUES(ID), IDFORNITORE = VALUES(IDFORNITORE), IDVENDOR = VALUES(IDVENDOR), 
        VENDORNAME = VALUES(VENDORNAME), CODICE = VALUES(CODICE), NOME = VALUES(NOME), 
        DESCRIZIONE = VALUES(DESCRIZIONE), STARRANK = VALUES(STARRANK), 
        IDNAZIONE = VALUES(IDNAZIONE), IDCITTA = VALUES(IDCITTA), INDIRIZZO = VALUES(INDIRIZZO), 
        INDIRIZZO2 = VALUES(INDIRIZZO2), ZIP = VALUES(ZIP), PHONE = VALUES(PHONE), 
        FAX = VALUES(FAX), MAIL = VALUES(MAIL), IMG = VALUES(IMG), LAT = VALUES(LAT), LANG = VALUES(LANG), 
        CHILDAGE = VALUES(CHILDAGE), ROOMTYPE = VALUES(ROOMTYPE), MEALPLAN = VALUES(MEALPLAN), 
        FAMILYPLAN = VALUES(FAMILYPLAN), MAXOCC = VALUES(MAXOCC), NONRIMBORSABILE = VALUES(NONRIMBORSABILE), FEE = VALUES(FEE), FEETYPE = VALUES(FEETYPE)';

        mysql_query($sql);
        if (mysql_error()) {
            setMessage('error', 'Problema nella query: '.$sql.' \n '.mysql_error());
            return false;
        }
    }   

    setMessage('success', 'Servizi caricati correttamente');
    setStatusOfRequest(100);
    return true;

}

function uploadTariffeTravalco(&$data)
{
    $data = array_filter($data);
    $chunk_size = 100;

    $h_codice_column              = (array_column($data, 0));
    $h_codice_column_chunk        = array_chunk($h_codice_column, $chunk_size);
    $h_nome_column                = addSlashesToArray(array_column($data, 1));
    $h_nome_column_chunk          = array_chunk($h_nome_column,$chunk_size);
    $h_indirizzo_column           = addSlashesToArray(array_column($data, 2));
    $h_indirizzo_column_chunk     = array_chunk($h_indirizzo_column, $chunk_size);
    $h_indirizzo_2_column         = addSlashesToArray(array_column($data, 3));
    $h_indirizzo_2_column_chunk   = array_chunk($h_indirizzo_2_column, $chunk_size);
    $c_nome_column                = addSlashesToArray(array_column($data, 4));
    $c_nome_column_chunk          = array_chunk($c_nome_column, $chunk_size);
    $n_state_column               = array_chunk(addSlashesToArray(array_column($data, 5)),$chunk_size);
    $h_zip_column                 = array_column($data, 6);
    $h_zip_column_chunk           = array_chunk($h_zip_column, $chunk_size);
    $n_nome_column                = array_column($data, 7);
    $n_nome_column_chunk          = array_chunk($h_zip_column, $chunk_size);
    $c_codice_column              = array_column($data, 8);
    $c_codice_column_chunk        = array_chunk($h_zip_column, $chunk_size);
    $h_phone_column               = addSlashesToArray(array_column($data, 10));
    // $h_phone_column_sanitizied               = array_map('sanitizePhone', $h_phone_column);
    $h_phone_column_chunk         = array_chunk($h_phone_column, $chunk_size);
    $h_fax_column                 = array_chunk(array_column($data, 11) ,$chunk_size);
    $h_starrank_column            = array_column($data, 12);
    $h_starrank_column_chunk      = array_chunk($h_starrank_column ,$chunk_size);
    $h_desc_column                = array_column($data, 15);
    $h_desc_column_chunk          = array_chunk($h_zip_column, $chunk_size);
    $t_begin_date_column          = array_chunk(array_column($data, 16), $chunk_size);
    $t_end_date_column            = array_chunk(array_column($data, 17), $chunk_size);
    $t_desc_column                = array_chunk(array_column($data, 18), $chunk_size);
    $t_childage_column            = array_column($data, 20);
    $t_childage_column_chunk      = array_chunk($childage_column ,$chunk_size);
    $t_daysappl_column            = array_chunk(array_column($data, 21) ,$chunk_size);
    $t_price_1_column             = array_chunk(array_column($data, 22) ,$chunk_size);
    $t_price_2_column             = array_chunk(array_column($data, 23) ,$chunk_size);
    $t_price_3_column             = array_chunk(array_column($data, 24) ,$chunk_size);
    $t_price_4_column             = array_chunk(array_column($data, 25) ,$chunk_size);
    $n_cod_column                 = array_column($data, 30);
    $n_cod_column_chunk           = array_chunk($childage_column ,$chunk_size);
    $z_cod_column                 = array_column($data, 34);
    $z_cod_column_chunk           = array_chunk($childage_column ,$chunk_size);
    $z_desc_column                = array_column($data,  35);
    $z_desc_column_chunk          = array_chunk($childage_column ,$chunk_size);

    // Carico le citta che non sono state trovate
    // 
    $array_citta = array(
        $c_codice_column,
        $$c_nome_column,
        $n_cod_column,
        $z_cod_column
    );

    array_unshift($array_citta, null);
    $array_citta = call_user_func_array('array_map', $array_citta);
    $upload_citta_ok = uploadCitta($array_citta);

    // Il fornitore è unico, quindi non serve caricarlo
    $id_fornitore = 20000;
    // Gestisco il caricamento dell'hotel in servizi_hotel
    $array_hotel = array(
        1 => array_fill(0, count($h_codice_column), 'Travalco'),
        2 => $h_starrank_column,
        3 => $h_codice_column,
        4 => $h_nome_column,
        6 => $n_cod_column,
        7 => $c_nome_column,
        8 => $h_indirizzo_column,
        10 => $h_zip_column,
        11 => $h_phone_column,
        16 => $t_childage_column
    );

    uploadServiziTravalco($array_hotel);

    for ($i=0; $i < sizeof($h_codice_column_chunk); $i++) { 
        $sql = 'INSERT INTO servizi_hotel_tariffe (ID, IDHOTEL, CHILDAGE, FAMILYPLAN, DATABEGIN, DATAEND, BOOKINGBEGIN,
            BOOKINGEND, DAYAPPL, DESCRIZIONE, CANCPOLICY, PREZZO_1, PREZZO_2, PREZZO_3, PREZZO_4, FORNITORE) VALUES ';
        for ($c=0; $c < sizeof($h_codice_column_chunk[$i]); $c++) {
            $sql_hotel = " SELECT ID, IDCITTA FROM servizi_hotel WHERE CODICE ='".$h_codice_column_chunk[$i][$c]."'";
            $res_hotel = mysql_fetch_assoc(mysql_query($sql_hotel)); 
            $id_hotel = $res_hotel['ID'];
            $citta_hotel = $res_hotel['IDCITTA'];
            // Se non è presente un hotel dalla ricerca allora inserisco nel db
            // Altrimenti continuo e inserisco la città prendendola dalle tariffe
            if ($id_hotel == 0) {
                // Significa che neanche caricando prima gli hotel sono riuscito.
                // In questo caso c'è qualcosa che non va. Salvo nel log
                $handle = fopen('log.txt', 'a');
                $h_nome_column_chunk[$i][$c] = '###';        
            } else {
                // Controllo se la città è stata valorizzata correttamente
                if ($citta_hotel == 0) {
                    // Log:                    
                    // Devo caricare la citta dal campo delle tariffe
                    // Devo togliere anche gli spazi perchè alcuni record
                    // delle tariffe hanno uno o più spazi rispetto al file
                    // delle città.
                    $citta_cod = string_substr($c_nome_column[$i][$c]);
                    $sql_citta = " SELECT ID from citta WHERE upper(REPLACE(CODICE, ' ', '')) 
                                    LIKE upper(REPLACE('%{$citta_cod}%', ' ', '')) LIMIT 1";
                    $res_citta = mysql_fetch_assoc(mysql_query($sql_citta));
                    $id_citta_tariffe = $res_citta['ID'];
                    $sql_update_citta = "UPDATE servizi_hotel SET IDCITTA = $id_citta_tariffe
                                WHERE ID = $id_hotel";
                    mysql_query($sql_update_citta);
                    if (mysql_error()) {
                        $handle = fopen('log.txt', 'a');
                        fwrite($handle, 'Problema nella query inserimento citta nelle
                            tariffe: '.mysql_error().'. Query: '.$sql_update_citta.'. \n
                            sql citta: '.$sql_citta);
                        fclose($handle);
                    }
                }
            }

            $t_booking_begin_column[$i][$c] = (!empty($t_booking_begin_column[$i][$c])) ? date_format(date_create($t_booking_begin_column[$i][$c]), "Y-m-d") : 'NULL' ;
            $t_booking_end_column[$i][$c] = (!empty($t_booking_end_column[$i][$c])) ? date_format(date_create($t_booking_end_column[$i][$c]), "Y-m-d") : 'NULL' ;
            $t_begin_date_column[$i][$c] = (!empty($t_begin_date_column[$i][$c])) ? date_format(date_create($t_begin_date_column[$i][$c]), "Y-m-d") : 'NULL' ;
            $t_end_date_column[$i][$c] = (!empty($t_end_date_column[$i][$c])) ? date_format(date_create($t_end_date_column[$i][$c]), "Y-m-d") : 'NULL' ;

            $t_price_1_column[$i][$c] = number_format($t_price_1_column[$i][$c], 2, '.', '');
            $t_price_2_column[$i][$c] = number_format($t_price_2_column[$i][$c], 2, '.', '');
            $t_price_3_column[$i][$c] = number_format($t_price_3_column[$i][$c], 2, '.', '');
            $t_price_4_column[$i][$c] = number_format($t_price_4_column[$i][$c], 2, '.', '');

            $days = addCommaToDays($t_daysappl_column);

            $sql .= "( null, '".$id_hotel."', '".$t_childage_column_chunk[$i][$c]."', '1', '".$t_begin_date_column[$i][$c]."', 
            '".$t_end_date_column[$i][$c]."', 'null', 'null', '".$days."', 
            '".$t_desc_column[$i][$c]."', '', '".$t_price_1_column[$i][$c]."', '".$t_price_2_column[$i][$c]."', 
            '".$t_price_3_column[$i][$c]."','".$t_price_4_column[$i][$c]."', 'Travalco'),";
        }
        $sql = substr($sql, 0, -1);
        $sql .= ' ON DUPLICATE KEY UPDATE 
        ID=VALUES(ID), IDHOTEL=VALUES(IDHOTEL), CHILDAGE=VALUES(CHILDAGE), FAMILYPLAN=VALUES(FAMILYPLAN), DATABEGIN=VALUES(DATABEGIN), DATAEND=VALUES(DATAEND), BOOKINGBEGIN=VALUES(BOOKINGBEGIN),
                    BOOKINGEND=VALUES(BOOKINGEND), DAYAPPL=VALUES(DAYAPPL), DESCRIZIONE=VALUES(DESCRIZIONE), CANCPOLICY=VALUES(CANCPOLICY), PREZZO_1=VALUES(PREZZO_1), PREZZO_2=VALUES(PREZZO_2), 
                    PREZZO_3=VALUES(PREZZO_3), PREZZO_4=VALUES(PREZZO_4), FORNITORE=VALUES(FORNITORE)';

        $handle = fopen('log.txt', 'a');
        fwrite($handle, $sql);
        fclose($handle);
        mysql_query($sql);
        if (mysql_error()) {
            setMessage('error', 'Problema nella query delle tariffe: '.$sql.' \n '.mysql_error());
            return false;
        }
    }

}

function uploadServiziTravalco(&$data)
{
    $chunk_size = 100;
    // Prima cosa devo filtrare le colonne:
    // $id_fornitore_column     = array_chunk(array_column($data, 0),$chunk_size);
    // $vendor_id               = array_column($data, 0); // Servono per ifornitori
    $vendor_name             = addSlashesToArray($data[1]);
    // $vendor_id_column        = array_chunk($vendor_id, $chunk_size); // Servono per l'inserimento in servizi_hotel
    $vendor_name_column      = array_chunk($vendor_name, $chunk_size);
    $starrank_column         = array_chunk($data[2], $chunk_size);
    $codice_column           = array_chunk(addSlashesToArray($data[3]), $chunk_size);
    $nome_column             = array_chunk(addSlashesToArray($data[4]), $chunk_size);
    $nazioni_column          = array_chunk(addSlashesToArray($data[6]), $chunk_size);
    // $id_citta_column         =
    $citta_column            = array_chunk(addSlashesToArray($data[7]), $chunk_size);
    $indirizzo_1_column      = array_chunk(addSlashesToArray($data[8]), $chunk_size);
    $zip_column              = array_chunk($data[10], $chunk_size);
    $phone_column            = array_chunk($data[11], $chunk_size);
    $childage_column         = array_chunk($data[16], $chunk_size);
    
    // Metodo alternativo di ottimizzazione:
    // Creo un chunk di array, da 100 alla volta. Poi
    // Creo la query in cui utilizzo on duplicate key update
    // Poi tante query quanti sono i chunks
    $column_size = sizeof($codice_column);

    for ($i=0; $i < $column_size; $i++) { 
        $percentage = 1/$column_size * 48;
        setStatusOfRequest($i * $percentage + 52);
        $sql = 'INSERT INTO servizi_hotel (ID, IDFORNITORE, IDVENDOR, VENDORNAME, CODICE, NOME, DESCRIZIONE,
            STARRANK, IDNAZIONE, IDCITTA, INDIRIZZO, INDIRIZZO2, ZIP, PHONE, FAX, MAIL, IMG, LAT, LANG,
            CHILDAGE, ROOMTYPE, MEALPLAN, FAMILYPLAN, MAXOCC, NONRIMBORSABILE, FEE, FEETYPE) VALUES ';
        for ($c=0; $c < sizeof($codice_column[$i]); $c++) {
            // Prendo l'ìd della città
            // La nazione è sempre stati uniti è sempre unica: prendo quella degli stati uniti
            $sql_nazioni = " SELECT ID from nazioni WHERE CODICE = '".$nazioni_column[$i][$c]."'";
            $res_nazione = mysql_fetch_assoc(mysql_query($sql_nazioni)); 
            $id_nazione = $res_nazione['ID'];

            $citta_cod = string_substr($citta_column[$i][$c]);
            $sql_citta = " SELECT ID from citta WHERE upper(REPLACE(CODICE, ' ', '')) 
                            LIKE upper(REPLACE('%{$citta_cod}%', ' ', '')) LIMIT 1";
            $id_citta = mysql_fetch_assoc(mysql_query($sql_citta))['ID'];
            $id_citta = (empty($id_citta)) ? '0' : $id_citta;
            $star_rank_array = str_split($starrank_column[$i][$c], "1");
            $star_rank = $star_rank_array[0];

            // Il fornitore è unico: Travalco
            $sql_vendor = " SELECT ID from fornitori WHERE upper(DESCRIZIONE) LIKE upper('%Travalco%') LIMIT 1";
            $id_vendor = mysql_fetch_assoc(mysql_query($sql_vendor))['ID'];

            $child_age_column[$i][$c] = (is_int($childage_column[$i][$c])) ? $childage_column[$i][$c] : 0 ;
            $familyplan = (db_is_null($childage_column[$i][$c])) ? 0 : 1 ;

            $sql .= "( null, '20000', '$id_vendor', '".$vendor_name_column[$i][$c]."', '".$codice_column[$i][$c]."', 
            '".$nome_column[$i][$c]."', '".$nome_column[$i][$c]."', '$star_rank', '".$id_nazione."', '".$id_citta."', 
            '".$indirizzo_1_column[$i][$c]."', '', '".$zip_column[$i][$c]."', '".$phone_column[$i][$c]."', '',
            '', '', '', '', '".$childage_column[$i][$c]."', '',
            '', '".$familyplan."', '', '', '',
            '0' ),";
        }
        $sql = substr($sql, 0, -1);
        $sql .= ' ON DUPLICATE KEY UPDATE 
        ID = VALUES(ID), IDFORNITORE = VALUES(IDFORNITORE), IDVENDOR = VALUES(IDVENDOR), 
        VENDORNAME = VALUES(VENDORNAME), CODICE = VALUES(CODICE), NOME = VALUES(NOME), 
        DESCRIZIONE = VALUES(DESCRIZIONE), STARRANK = VALUES(STARRANK), 
        IDNAZIONE = VALUES(IDNAZIONE), IDCITTA = VALUES(IDCITTA), INDIRIZZO = VALUES(INDIRIZZO), 
        INDIRIZZO2 = VALUES(INDIRIZZO2), ZIP = VALUES(ZIP), PHONE = VALUES(PHONE), 
        FAX = VALUES(FAX), MAIL = VALUES(MAIL), IMG = VALUES(IMG), LAT = VALUES(LAT), LANG = VALUES(LANG), 
        CHILDAGE = VALUES(CHILDAGE), ROOMTYPE = VALUES(ROOMTYPE), MEALPLAN = VALUES(MEALPLAN), 
        FAMILYPLAN = VALUES(FAMILYPLAN), MAXOCC = VALUES(MAXOCC), NONRIMBORSABILE = VALUES(NONRIMBORSABILE), FEE = VALUES(FEE), FEETYPE = VALUES(FEETYPE)';
        mysql_query($sql);
        if (mysql_error()) {
            setMessage('error', 'Problema nella query: '.$sql.' \n '.mysql_error());
            return false;
        }
    }   

    setMessage('success', 'Servizi caricati correttamente');
    setStatusOfRequest(100);
    return true;
}

function uploadTariffeRMHT(&$data)
{
    $data = array_filter($data);
    $chunk_size = 100;
    // h = codice
    // t = tariffe
    // n = nazioni
    // c = città
    $h_codice_column              = addSlashesToArray(array_column($data, 0));
    $h_nome_column                = addSlashesToArray(array_column($data, 1));
    $h_codice_column_chunk              = array_chunk($h_codice_column, $chunk_size);
    $h_nome_column_chunk                = array_chunk($h_nome_column,$chunk_size);
    $h_indirizzo_column           = addSlashesToArray(array_column($data, 2));
    $h_indirizzo_column_chunk           = array_chunk($h_indirizzo_column, $chunk_size);
    $c_nome_column                = addSlashesToArray(array_column($data, 3));
    $c_nome_column_chunk                = array_chunk($c_nome_column, $chunk_size);
    $n_nome_column                = array_chunk(addSlashesToArray(array_column($data, 4)),$chunk_size);
    $h_zip_column                 = array_column($data,                   5);
    $h_zip_column_chunk                 = array_chunk($h_zip_column, $chunk_size);
    $h_phone_column               = array_column($data,                   6);
    $h_phone_column_chunk               = array_chunk(array_column($data,                   6) ,$chunk_size);
    $h_fax_column                 = array_chunk(array_column($data,                   7) ,$chunk_size);
    $h_category_column            = array_chunk(array_column($data,                   8) ,$chunk_size);
    $h_roomtype_column            = array_chunk(addSlashesToArray(array_column($data, 9)),$chunk_size);
    $t_begin_date_column          = array_chunk(array_column($data,                   10), $chunk_size);
    $t_end_date_column            = array_chunk(array_column($data,                   11), $chunk_size);
    $t_daysappl_column            = array_chunk(array_column($data,                   12) ,$chunk_size);
    $t_price_1_column             = array_chunk(array_column($data,                   23) ,$chunk_size);
    $t_price_2_column             = array_chunk(array_column($data,                   24) ,$chunk_size);
    $t_price_3_column             = array_chunk(array_column($data,                   25) ,$chunk_size);
    $t_price_4_column             = array_chunk(array_column($data,                   26) ,$chunk_size);
    $t_childage_column            = array_column($data,                   27);
    $t_childage_column_chunk            = array_chunk($childage_column ,$chunk_size);
    $t_cancellation_policy_column = array_chunk(addSlashesToArray(array_column($data, 28)),$chunk_size);
    $h_mealplan_column            = array_chunk(addSlashesToArray(array_column($data, 29)),$chunk_size);
    $h_note_column                = array_chunk(addSlashesToArray(array_column($data, 30)),$chunk_size);

    // Carico le citta che non sono state trovate
    $citta_filtered = array_values(array_unique((array_column($data, 3))));
    $array_citta = array(
        array_map("string_substr", $citta_filtered),
        $citta_filtered,
        array_fill(0, count($citta_filtered), 'USA'),
        array_fill(0, count($citta_filtered), 'null')
    );
    array_unshift($array_citta, null);
    $array_citta = call_user_func_array('array_map', $array_citta);
    $upload_citta_ok = uploadCitta($array_citta);

    // Il fornitore è unico, quindi non serve caricarlo
    $id_fornitore = 19206;

    // Gestisco il caricamento dell'hotel in servizi_hotel
    $array_hotel = array(
        1 => array_fill(0, count($h_codice_column), 'RMHT'),
        3 => $h_codice_column,
        4 => $h_nome_column,
        6 => 'USA',
        7 => $c_nome_column,
        8 => $h_indirizzo_column,
        10 => $h_zip_column,
        11 => $h_phone_column,
        16 => $t_childage_column
    );
    uploadServiziRMHT($array_hotel);


    for ($i=0; $i < sizeof($h_codice_column_chunk); $i++) { 
        $sql = 'INSERT INTO servizi_hotel_tariffe (ID, IDHOTEL, CHILDAGE, FAMILYPLAN, DATABEGIN, DATAEND, BOOKINGBEGIN,
            BOOKINGEND, DAYAPPL, DESCRIZIONE, CANCPOLICY, PREZZO_1, PREZZO_2, PREZZO_3, PREZZO_4, FORNITORE) VALUES ';
        for ($c=0; $c < sizeof($h_codice_column_chunk[$i]); $c++) {
            $sql_hotel = " SELECT ID, IDCITTA FROM servizi_hotel WHERE CODICE = '".$h_codice_column_chunk[$i][$c]."'";
            $res_hotel = mysql_fetch_assoc(mysql_query($sql_hotel)); 
            $id_hotel = $res_hotel['ID'];
            $citta_hotel = $res_hotel['IDCITTA'];
            // Se non è presente un hotel dalla ricerca allora inserisco nel db
            // Altrimenti continuo e inserisco la città prendendola dalle tariffe
            if ($id_hotel == 0) {
                // Significa che neanche caricando prima gli hotel sono riuscito.
                // In questo caso c'è qualcosa che non va. Salvo nel log
                // $handle = fopen('log.txt', 'a');
                // fwrite($handle, "Errore nell'inserimento della tariffa: \n");
                // fwrite($handle, $sql_hotel."\n");
                // fwrite($handle, $h_codice_column_chunk[$i][$c]."\n");
                // fclose($handle);
                $h_nome_column_chunk[$i][$c] = '###';        
            } else {
                // Controllo se la città è stata valorizzata correttamente
                if ($citta_hotel == 0) {
                    // Log:
                    // $handle = fopen('log.txt', 'a');
                    // fwrite($handle, "Id citta nulla. codice tariffa: {$codice_column[$i][$c]}  \n");
                    // fclose($handle);
                    
                    // Devo caricare la citta dal campo delle tariffe
                    // Devo togliere anche gli spazi perchè alcuni record
                    // delle tariffe hanno uno o più spazi rispetto al file
                    // delle città.
                    $citta_cod = string_substr($c_nome_column[$i][$c]);
                    $sql_citta = " SELECT ID from citta WHERE upper(REPLACE(CODICE, ' ', '')) 
                                    LIKE upper(REPLACE('%{$citta_cod}%', ' ', '')) LIMIT 1";
                    $res_citta = mysql_fetch_assoc(mysql_query($sql_citta));
                    $id_citta_tariffe = $res_citta['ID'];
                    $sql_update_citta = "UPDATE servizi_hotel SET IDCITTA = $id_citta_tariffe
                                WHERE ID = $id_hotel";
                    mysql_query($sql_update_citta);
                    if (mysql_error()) {
                        $handle = fopen('log.txt', 'a');
                        fwrite($handle, 'Problema nella query inserimento citta nelle
                            tariffe: '.mysql_error().'. Query: '.$sql_update_citta.'. \n
                            sql citta: '.$sql_citta);
                        fclose($handle);
                    }
                }
            }

            $t_booking_begin_column[$i][$c] = (!empty($t_booking_begin_column[$i][$c])) ? date_format(date_create($t_booking_begin_column[$i][$c]), "Y-m-d") : 'NULL' ;
            $t_booking_end_column[$i][$c] = (!empty($t_booking_end_column[$i][$c])) ? date_format(date_create($t_booking_end_column[$i][$c]), "Y-m-d") : 'NULL' ;
            $t_begin_date_column[$i][$c] = (!empty($t_begin_date_column[$i][$c])) ? date_format(date_create($t_begin_date_column[$i][$c]), "Y-m-d") : 'NULL' ;
            $t_end_date_column[$i][$c] = (!empty($t_end_date_column[$i][$c])) ? date_format(date_create($t_end_date_column[$i][$c]), "Y-m-d") : 'NULL' ;

            $t_price_1_column[$i][$c] = number_format($t_price_1_column[$i][$c], 2, '.', '');
            $t_price_2_column[$i][$c] = number_format($t_price_2_column[$i][$c], 2, '.', '');
            $t_price_3_column[$i][$c] = number_format($t_price_3_column[$i][$c], 2, '.', '');
            $t_price_4_column[$i][$c] = number_format($t_price_4_column[$i][$c], 2, '.', '');

            $days = setDays($t_daysappl_column);

            // $handle = fopen('log.txt', 'a');
            // fwrite($handle, "Id Hotel: {$id_hotel} Prezzo: {$price_1_column[$i][$c]} \n");
            // fclose($handle);

            $sql .= "( null, '".$id_hotel."', '".$t_childage_column_chunk[$i][$c]."', '1', '".$t_begin_date_column[$i][$c]."', 
            '".$t_end_date_column[$i][$c]."', 'null', 'null', '".$days."', 
            '".$h_nome_column_chunk[$i][$c]."', '".$t_cancellation_policy_column[$i][$c]."', '".$t_price_1_column[$i][$c]."', '".$t_price_2_column[$i][$c]."', 
            '".$t_price_3_column[$i][$c]."','".$t_price_4_column[$i][$c]."', 'RMHT'),";
        }
        $sql = substr($sql, 0, -1);
        $sql .= ' ON DUPLICATE KEY UPDATE 
        ID=VALUES(ID), IDHOTEL=VALUES(IDHOTEL), CHILDAGE=VALUES(CHILDAGE), FAMILYPLAN=VALUES(FAMILYPLAN), DATABEGIN=VALUES(DATABEGIN), DATAEND=VALUES(DATAEND), BOOKINGBEGIN=VALUES(BOOKINGBEGIN),
                    BOOKINGEND=VALUES(BOOKINGEND), DAYAPPL=VALUES(DAYAPPL), DESCRIZIONE=VALUES(DESCRIZIONE), CANCPOLICY=VALUES(CANCPOLICY), PREZZO_1=VALUES(PREZZO_1), PREZZO_2=VALUES(PREZZO_2), 
                    PREZZO_3=VALUES(PREZZO_3), PREZZO_4=VALUES(PREZZO_4), FORNITORE=VALUES(FORNITORE)';

        $handle = fopen('log.txt', 'a');
        fwrite($handle, $sql);
        fclose($handle);
        mysql_query($sql);
        if (mysql_error()) {
            setMessage('error', 'Problema nella query delle tariffe: '.$sql.' \n '.mysql_error());
            return false;
        }
    }

}

function uploadServiziRMHT(&$data)
{
    $chunk_size = 100;
    // Prima cosa devo filtrare le colonne:
    // $id_fornitore_column     = array_chunk(array_column($data, 0),$chunk_size);
    // $vendor_id               = array_column($data, 0); // Servono per ifornitori
    $vendor_name             = addSlashesToArray($data[1]);
    // $vendor_id_column        = array_chunk($vendor_id, $chunk_size); // Servono per l'inserimento in servizi_hotel
    $vendor_name_column      = array_chunk($vendor_name, $chunk_size);
    //$starrank_column         = array_chunk(array_column($data, 2),$chunk_size);
    $codice_column           = array_chunk(addSlashesToArray($data[3]),$chunk_size);
    $nome_column             = array_chunk(addSlashesToArray($data[4]),$chunk_size);
    $nazione_column          = $data[6];
    // $id_citta_column         =
    $citta_column            = array_chunk(addSlashesToArray($data[7]),$chunk_size);
    $indirizzo_1_column      = array_chunk(addSlashesToArray($data[8]),$chunk_size);
    $zip_column              = array_chunk($data[10] ,$chunk_size);
    $phone_column            = array_chunk($data[11] ,$chunk_size);
    $childage_column         = array_chunk($data[16] ,$chunk_size);

    // La nazione è sempre stati uniti è sempre unica: prendo quella degli stati uniti
    $sql_nazioni = " SELECT ID from nazioni WHERE CODICE = '".$nazione_column."'";
    $res_nazione = mysql_fetch_assoc(mysql_query($sql_nazioni)); 
    $id_nazione = $res_nazione['ID'];
    
    // Metodo alternativo di ottimizzazione:
    // Creo un chunk di array, da 100 alla volta. Poi
    // Creo la query in cui utilizzo on duplicate key update
    // Poi tante query quanti sono i chunks
    $column_size = sizeof($codice_column);

    for ($i=0; $i < $column_size; $i++) { 
        $percentage = 1/$column_size * 48;
        setStatusOfRequest($i * $percentage + 52);
        $sql = 'INSERT INTO servizi_hotel (ID, IDFORNITORE, IDVENDOR, VENDORNAME, CODICE, NOME, DESCRIZIONE,
            STARRANK, IDNAZIONE, IDCITTA, INDIRIZZO, INDIRIZZO2, ZIP, PHONE, FAX, MAIL, IMG, LAT, LANG,
            CHILDAGE, ROOMTYPE, MEALPLAN, FAMILYPLAN, MAXOCC, NONRIMBORSABILE, FEE, FEETYPE) VALUES ';
        for ($c=0; $c < sizeof($codice_column[$i]); $c++) {
            // Prendo l'ìd della città
            $citta_cod = string_substr($citta_column[$i][$c]);
            $sql_citta = " SELECT ID from citta WHERE upper(REPLACE(CODICE, ' ', '')) 
                            LIKE upper(REPLACE('%$citta_cod%', ' ', ''))
                            OR upper(REPLACE(DESCRIZIONE, ' ', '')) 
                            LIKE upper(REPLACE('%$citta_cod%', ' ', ''))  LIMIT 1";
            $fetch_citta = mysql_fetch_assoc(mysql_query($sql_citta));
            $id_citta = $fetch_citta['ID'];
            $id_citta = (empty($id_citta)) ? '0' : $id_citta;

            // Il fornitore è unico: RMHT
            $sql_vendor = " SELECT ID from fornitori WHERE upper(DESCRIZIONE) LIKE upper('%RMHT%') LIMIT 1";
            $id_vendor = mysql_fetch_assoc(mysql_query($sql_vendor))['ID'];

            $child_age_column[$i][$c] = (is_int($childage_column[$i][$c])) ? $childage_column[$i][$c] : 0 ;
            //$phone_column[$i][$c] = (is_int($phone_column[$i][$c])) ? $phone_column[$i][$c] : 0 ;
            $familyplan = (db_is_null($childage_column[$i][$c])) ? 0 : 1 ;

            if (!db_is_null($codice_column[$i][$c])) {
                $sql .= "( null, '20000', '$id_vendor', '".$vendor_name_column[$i][$c]."', '".$codice_column[$i][$c]."', 
                '".$nome_column[$i][$c]."', '".$nome_column[$i][$c]."', '', '".$id_nazione."', '".$id_citta."', 
                '".$indirizzo_1_column[$i][$c]."', '', '".$zip_column[$i][$c]."', '".$phone_column[$i][$c]."', '',
                '', '', '', '', '".$childage_column[$i][$c]."', '',
                '', '".$familyplan."', '', '', '',
                '0' ),";
            }
        }
        $sql = substr($sql, 0, -1);
        $sql .= ' ON DUPLICATE KEY UPDATE 
        STARRANK = VALUES(STARRANK), 
        INDIRIZZO = VALUES(INDIRIZZO), 
        INDIRIZZO2 = VALUES(INDIRIZZO2), ZIP = VALUES(ZIP), PHONE = VALUES(PHONE), 
        FAX = VALUES(FAX), MAIL = VALUES(MAIL), IMG = VALUES(IMG), LAT = VALUES(LAT), LANG = VALUES(LANG), 
        CHILDAGE = VALUES(CHILDAGE), ROOMTYPE = VALUES(ROOMTYPE), MEALPLAN = VALUES(MEALPLAN), 
        FAMILYPLAN = VALUES(FAMILYPLAN), MAXOCC = VALUES(MAXOCC), NONRIMBORSABILE = VALUES(NONRIMBORSABILE), FEE = VALUES(FEE), FEETYPE = VALUES(FEETYPE)';
        mysql_query($sql);
        if (mysql_error()) {
            setMessage('error', 'Problema nella query: '.$sql.' \n '.mysql_error());
            return false;
        }
    }   

    setMessage('success', 'Servizi caricati correttamente');
    setStatusOfRequest(100);
    return true;
}


/**
 * Carica le tariffe con l'alaska. Se non sono presenti
 * fornitori e hotel chiama le funzioni per caricare anche quelli.
 * @param  array &$data i dati letti
 * @return boolean true in caso di successo. false altreimenti
 */
function uploadTariffeAlaska(&$data)
{
    $data = array_filter($data);
    $chunk_size = 1003;
    // Prima cosa devo filtrare le colonne:
    $citta_column               = array_chunk((array_column($data, 0)),$chunk_size);
    $codice_column              = array_chunk((array_column($data, 1)),$chunk_size);
    $vendor_name                = addSlashesToArray(array_column($data, 2));
    $descrizione_column         = array_chunk(addSlashesToArray(array_column($data, 3)),$chunk_size);
    $familyplan_column          = array_chunk(array_column($data,                   4) ,$chunk_size);
    $childage_column            = array_chunk(array_column($data,                   5) ,$chunk_size);
    $begin_date_column          = array_chunk(array_column($data,                   6), $chunk_size);
    $end_date_column            = array_chunk(array_column($data,                   7),$chunk_size);
    $booking_begin_column       = array_chunk(array_column($data,                   8) ,$chunk_size);
    $booking_end_column         = array_chunk(array_column($data,                   9) ,$chunk_size);
    //$promo_message_column       = array_chunk(addSlashesToArray(array_column($data, 10)),$chunk_size);
    $cancellation_policy_column = array_chunk(addSlashesToArray(array_column($data, 11)),$chunk_size);
    $days_column                = array_chunk(array_column($data,                   12) ,$chunk_size);
    $price_1_column             = array_chunk(array_column($data,                   13) ,$chunk_size);
    $price_2_column             = array_chunk(array_column($data,                   14) ,$chunk_size);
    $price_3_column             = array_chunk(array_column($data,                   15) ,$chunk_size);
    $price_4_column             = array_chunk(array_column($data,                   16) ,$chunk_size);
    //$pricing_type_column        = array_chunk(addSlashesToArray(array_column($data, 17)),$chunk_size);
    $street_address_column      = array_chunk(addSlashesToArray(array_column($data, 18)),$chunk_size);
    $citta_column               = array_chunk(addSlashesToArray(array_column($data, 19)),$chunk_size);
    $state_column               = array_chunk(addSlashesToArray(array_column($data, 20)),$chunk_size);
    $zip_column                 = array_chunk(array_column($data,                   21) ,$chunk_size);
    $phone_column                 = array_chunk(array_column($data,                   22) ,$chunk_size);

    // Carico le citta che non sono state trovate
    $citta_filtered = array_values(array_unique((array_column($data, 0))));
    $array_citta = array(
        array_map("string_substr", $citta_filtered),
        $citta_filtered,
        array_fill(0, count($citta_filtered), 'USA'),
        array_fill(0, count($citta_filtered), 'Alaska Area')
    );
    array_unshift($array_citta, null);
    $array_citta = call_user_func_array('array_map', $array_citta);
    $upload_citta_ok = uploadCitta($array_citta);

    // Carico i nuovi fornitori che non sono stati trovati nel db
    $fornitori = array_values(array_unique(array_map('trim', array_column($data, 2))));
    $handle = fopen('log.txt', 'a');
    fwrite($handle, print_r($fornitori, true));
    fclose($handle);
    $upload_fornitori_ok = uploadFornitori($fornitori, true);
    setMessage('success', 'Fornitori caricati correttamente');

    // Gestisco il caricamento dell'hotel in servizi_hotel
    $array_hotel = array(
        1 => $vendor_name,
        3 => array_column($data, 1),
        4 => array_column($data, 3),
        6 => 'USA',
        7 => array_column($data, 0),
        8 => array_column($data, 18),
        10 => array_column($data, 21),
        11 => array_column($data, 22),
        16 => array_column($data, 5)
    );
    uploadServiziAlaska($array_hotel);

    // Metodo di ottimizzazione:
    // Creo un chunk di array, da 100 alla volta. Poi
    // Creo la query in cui utilizzo on duplicate key update
    // Poi tante query quanti sono i chunks
    
    $days = array(
        'Sun' => 0,
        'Mon' => 1,
        'Tue' => 2,
        'Wed' => 3,
        'Thu' => 4,
        'Fri' => 5,
        'Sat' => 6
    );

    for ($i=0; $i < sizeof($codice_column); $i++) { 
        $sql = 'INSERT INTO servizi_hotel_tariffe (ID, IDHOTEL, CHILDAGE, FAMILYPLAN, DATABEGIN, DATAEND, BOOKINGBEGIN,
            BOOKINGEND, DAYAPPL, DESCRIZIONE, CANCPOLICY, PREZZO_1, PREZZO_2, PREZZO_3, PREZZO_4, FORNITORE) VALUES ';
        for ($c=0; $c < sizeof($codice_column[$i]); $c++) {
            $sql_hotel = " SELECT ID, IDCITTA FROM servizi_hotel WHERE CODICE ='".$codice_column[$i][$c]."'";
            $res_hotel = mysql_fetch_assoc(mysql_query($sql_hotel)); 
            $id_hotel = $res_hotel['ID'];
            $citta_hotel = $res_hotel['IDCITTA'];
            // Se non è presente un hotel dalla ricerca allora inserisco nel db
            // Altrimenti continuo e inserisco la città prendendola dalle tariffe
            if ($id_hotel == 0) {
                // Significa che neanche caricando prima gli hotel sono riuscito.
                // In questo caso c'è qualcosa che non va. Salvo nel log
                $handle = fopen('log.txt', 'a');
                // fwrite($handle, "Errore nell'inserimento della tariffa: \n");
                // fwrite($handle, $sql_hotel);
                // fwrite($handle, $citta_hotel);
                // fwrite($handle, $descrizione_column[$i][$c]);
                $descrizione_column[$i][$c] = '###';        
            } else {
                // Controllo se la città è stata valorizzata correttamente
                if ($citta_hotel == 0) {
                    // Log:
                    // $handle = fopen('log.txt', 'a');
                    // fwrite($handle, "Id citta nulla. codice tariffa: {$codice_column[$i][$c]}  \n");
                    // fclose($handle);
                    
                    // Devo caricare la citta dal campo delle tariffe
                    // Devo togliere anche gli spazi perchè alcuni record
                    // delle tariffe hanno uno o più spazi rispetto al file
                    // delle città.
                    $citta_cod = string_substr($citta_column[$i][$c]);
                    $sql_citta = " SELECT ID from citta WHERE upper(REPLACE(CODICE, ' ', '')) 
                                    LIKE upper(REPLACE('%{$citta_cod}%', ' ', '')) LIMIT 1";
                    $res_citta = mysql_fetch_assoc(mysql_query($sql_citta));
                    $id_citta_tariffe = $res_citta['ID'];
                    $sql_update_citta = "UPDATE servizi_hotel SET IDCITTA = $id_citta_tariffe
                                WHERE ID = $id_hotel";
                    mysql_query($sql_update_citta);
                    if (mysql_error()) {
                        $handle = fopen('log.txt', 'a');
                        fwrite($handle, 'Problema nella query inserimento citta nelle
                            tariffe: '.mysql_error().'. Query: '.$sql_update_citta.'. \n
                            sql citta: '.$sql_citta);
                        fclose($handle);
                    }
                }
            }

            $booking_begin_column[$i][$c] = (!empty($booking_begin_column[$i][$c])) ? date_format(date_create($booking_begin_column[$i][$c]), "Y-m-d") : 'NULL' ;
            $booking_end_column[$i][$c] = (!empty($booking_end_column[$i][$c])) ? date_format(date_create($booking_end_column[$i][$c]), "Y-m-d") : 'NULL' ;
            $begin_date_column[$i][$c] = (!empty($begin_date_column[$i][$c])) ? date_format(date_create($begin_date_column[$i][$c]), "Y-m-d") : 'NULL' ;
            $end_date_column[$i][$c] = (!empty($end_date_column[$i][$c])) ? date_format(date_create($end_date_column[$i][$c]), "Y-m-d") : 'NULL' ;

            $price_1_column[$i][$c] = number_format($price_1_column[$i][$c], 2, '.', '');
            $price_2_column[$i][$c] = number_format($price_2_column[$i][$c], 2, '.', '');
            $price_3_column[$i][$c] = number_format($price_3_column[$i][$c], 2, '.', '');
            $price_4_column[$i][$c] = number_format($price_4_column[$i][$c], 2, '.', '');

            // $handle = fopen('log.txt', 'a');
            // fwrite($handle, "Id Hotel: {$id_hotel} Prezzo: {$price_1_column[$i][$c]} \n");
            // fclose($handle);

            $sql .= "( null, '".$id_hotel."', '".$childage_column[$i][$c]."', '".$familyplan_column[$i][$c]."', '".$begin_date_column[$i][$c]."', 
            '".$end_date_column[$i][$c]."', '".$booking_begin_column[$i][$c]."', '".$booking_end_column[$i][$c]."', '".$days_query."', 
            '".$descrizione_column[$i][$c]."', '".$cancellation_policy_column[$i][$c]."', '".$price_1_column[$i][$c]."', '".$price_2_column[$i][$c]."', 
            '".$price_3_column[$i][$c]."','".$price_4_column[$i][$c]."', 'Alaska'),";
        }
        $sql = substr($sql, 0, -1);
        $sql .= ' ON DUPLICATE KEY UPDATE 
        ID=VALUES(ID), IDHOTEL=VALUES(IDHOTEL), CHILDAGE=VALUES(CHILDAGE), FAMILYPLAN=VALUES(FAMILYPLAN), DATABEGIN=VALUES(DATABEGIN), DATAEND=VALUES(DATAEND), BOOKINGBEGIN=VALUES(BOOKINGBEGIN),
                    BOOKINGEND=VALUES(BOOKINGEND), DAYAPPL=VALUES(DAYAPPL), DESCRIZIONE=VALUES(DESCRIZIONE), CANCPOLICY=VALUES(CANCPOLICY), PREZZO_1=VALUES(PREZZO_1), PREZZO_2=VALUES(PREZZO_2), 
                    PREZZO_3=VALUES(PREZZO_3), PREZZO_4=VALUES(PREZZO_4), FORNITORE=VALUES(FORNITORE)';

        $handle = fopen('log.txt', 'a');
        fwrite($handle, $sql);
        fclose($handle);
        mysql_query($sql);
        if (mysql_error()) {
            setMessage('error', 'Problema nella query delle tariffe: '.$sql.' \n '.mysql_error());
            return false;
        }
    }
}

/**
 * Carica in servizi_hotel il file associato.
 *
 * @param  array &$data I dati da caricare
 * @return boolean  Se il caricamento e' avvenuto correttamente
 */
function uploadServiziAlaska(&$data)
{
    $chunk_size = 100;
    // Prima cosa devo filtrare le colonne:
    // $id_fornitore_column     = array_chunk(array_column($data, 0),$chunk_size);
    // $vendor_id               = array_column($data, 0); // Servono per ifornitori
    $vendor_name             = addSlashesToArray($data[1]);
    // $vendor_id_column        = array_chunk($vendor_id, $chunk_size); // Servono per l'inserimento in servizi_hotel
    $vendor_name_column      = array_chunk($vendor_name, $chunk_size);
    //$starrank_column         = array_chunk(array_column($data, 2),$chunk_size);
    $codice_column           = array_chunk(addSlashesToArray($data[3]),$chunk_size);
    $nome_column             = array_chunk(addSlashesToArray($data[4]),$chunk_size);
    $nazione_column          = $data[6];
    // $id_citta_column         =
    $citta_column            = array_chunk(addSlashesToArray($data[7]),$chunk_size);
    $indirizzo_1_column      = array_chunk(addSlashesToArray($data[8]),$chunk_size);
    $zip_column              = array_chunk($data[10] ,$chunk_size);
    $phone_column            = array_chunk($data[11] ,$chunk_size);
    $childage_column         = array_chunk($data[16] ,$chunk_size);

    // La nazione per l'alaska è sempre unica: prendo quella degli stati uniti
    $sql_nazioni = " SELECT ID from nazioni WHERE CODICE = '".$nazioni_column."'";
    $res_nazione = mysql_fetch_assoc(mysql_query($sql_nazioni)); 
    $id_nazione = $res_nazione['ID'];
    // Metodo alternativo di ottimizzazione:
    // Creo un chunk di array, da 100 alla volta. Poi
    // Creo la query in cui utilizzo on duplicate key update
    // Poi tante query quanti sono i chunks
    $column_size = sizeof($codice_column);

    for ($i=0; $i < $column_size; $i++) { 
        $percentage = 1/$column_size * 48;
        setStatusOfRequest($i * $percentage + 52);
        $sql = 'INSERT INTO servizi_hotel (ID, IDFORNITORE, IDVENDOR, VENDORNAME, CODICE, NOME, DESCRIZIONE,
            STARRANK, IDNAZIONE, IDCITTA, INDIRIZZO, INDIRIZZO2, ZIP, PHONE, FAX, MAIL, IMG, LAT, LANG,
            CHILDAGE, ROOMTYPE, MEALPLAN, FAMILYPLAN, MAXOCC, NONRIMBORSABILE, FEE, FEETYPE) VALUES ';
        for ($c=0; $c < sizeof($codice_column[$i]); $c++) {
            $citta_cod = string_substr($citta_column[$i][$c]);
            $sql_citta = " SELECT ID from citta WHERE upper(REPLACE(CODICE, ' ', '')) 
                            LIKE upper(REPLACE('%{$citta_cod}%', ' ', '')) LIMIT 1";
            $id_citta = mysql_fetch_assoc(mysql_query($sql_citta))['ID'];
            $id_citta = (empty($id_citta)) ? '0' : $id_citta;

            $sql_vendor = " SELECT ID from fornitori WHERE upper(DESCRIZIONE) LIKE upper('%".$vendor_name_column[$i][$c]."%') LIMIT 1";
            $id_vendor = mysql_fetch_assoc(mysql_query($sql_vendor))['ID'];

            $child_age_column[$i][$c] = (is_int($childage_column[$i][$c])) ? $childage_column[$i][$c] : 0 ;
            $phone_column[$i][$c] = (is_int($phone_column[$i][$c])) ? $phone_column[$i][$c] : 0 ;
            $familyplan = (db_is_null($childage_column[$i][$c])) ? 0 : 1 ;

            $sql .= "( null, '0', '$id_vendor', '".$vendor_name_column[$i][$c]."', '".$codice_column[$i][$c]."', 
            '".$nome_column[$i][$c]."', '".$nome_column[$i][$c]."', '', '".$id_nazione."', '".$id_citta."', 
            '".$indirizzo_1_column[$i][$c]."', '', '".$zip_column[$i][$c]."', '".$phone_column[$i][$c]."', '',
            '', '', '', '', '".$childage_column[$i][$c]."', '',
            '', '".$familyplan."', '', '', '',
            '0' ),";
        }
        $sql = substr($sql, 0, -1);
        $sql .= ' ON DUPLICATE KEY UPDATE 
        ID = VALUES(ID), IDFORNITORE = VALUES(IDFORNITORE), IDVENDOR = VALUES(IDVENDOR), 
        VENDORNAME = VALUES(VENDORNAME), CODICE = VALUES(CODICE), NOME = VALUES(NOME), 
        DESCRIZIONE = VALUES(DESCRIZIONE), STARRANK = VALUES(STARRANK), 
        IDNAZIONE = VALUES(IDNAZIONE), IDCITTA = VALUES(IDCITTA), INDIRIZZO = VALUES(INDIRIZZO), 
        INDIRIZZO2 = VALUES(INDIRIZZO2), ZIP = VALUES(ZIP), PHONE = VALUES(PHONE), 
        FAX = VALUES(FAX), MAIL = VALUES(MAIL), IMG = VALUES(IMG), LAT = VALUES(LAT), LANG = VALUES(LANG), 
        CHILDAGE = VALUES(CHILDAGE), ROOMTYPE = VALUES(ROOMTYPE), MEALPLAN = VALUES(MEALPLAN), 
        FAMILYPLAN = VALUES(FAMILYPLAN), MAXOCC = VALUES(MAXOCC), NONRIMBORSABILE = VALUES(NONRIMBORSABILE), FEE = VALUES(FEE), FEETYPE = VALUES(FEETYPE)';
        mysql_query($sql);
        if (mysql_error()) {
            setMessage('error', 'Problema nella query: '.$sql.' \n '.mysql_error());
            return false;
        }
    }   

    setMessage('success', 'Servizi caricati correttamente');
    setStatusOfRequest(100);
    return true;

}

/**
 * Carica in servizi_hotel_tariffe il file associato
 * @param  boolean &$data I dati da caricare
 * @return boolean  Se il caricamento è avvenuto correttamente
 */
function uploadTariffe(&$data)
{
    $data = array_filter($data);
    $chunk_size = 100;
    // Prima cosa devo filtrare le colonne:
    $citta_column               = array_chunk(addSlashesToArray(array_column($data, 0)),$chunk_size);
    $codice_column              = array_chunk(addSlashesToArray(array_column($data, 1)),$chunk_size);
    $vendor_name                = addSlashesToArray(array_column($data, 2));
    $descrizione_column         = array_chunk(addSlashesToArray(array_column($data, 3)),$chunk_size);
    $familyplan_column          = array_chunk(array_column($data,                   4) ,$chunk_size);
    $childage_column            = array_chunk(array_column($data,                   5) ,$chunk_size);
    $begin_date_column          = array_chunk(array_column($data,                   6), $chunk_size);
    $end_date_column            = array_chunk(array_column($data,                   7),$chunk_size);
    $booking_begin_column       = array_chunk(array_column($data,                   8) ,$chunk_size);
    $booking_end_column         = array_chunk(array_column($data,                   9) ,$chunk_size);
    //$promo_message_column       = array_chunk(addSlashesToArray(array_column($data, 10)),$chunk_size);
    $cancellation_policy_column = array_chunk(addSlashesToArray(array_column($data, 11)),$chunk_size);
    $days_column                = array_chunk(array_column($data,                   12) ,$chunk_size);
    $price_1_column             = array_chunk(array_column($data,                   13) ,$chunk_size);
    $price_2_column             = array_chunk(array_column($data,                   14) ,$chunk_size);
    $price_3_column             = array_chunk(array_column($data,                   15) ,$chunk_size);
    $price_4_column             = array_chunk(array_column($data,                   16) ,$chunk_size);
    //$pricing_type_column        = array_chunk(addSlashesToArray(array_column($data, 17)),$chunk_size);
    //$street_address_column      = array_chunk(addSlashesToArray(array_column($data, 18)),$chunk_size);
    //$citta_column                = array_chunk(addSlashesToArray(array_column($data, 19)),$chunk_size);
    $state_column               = array_chunk(addSlashesToArray(array_column($data, 20)),$chunk_size);
    $zip_column                 = array_chunk(array_column($data,                   21) ,$chunk_size);

    // Carico le nazioni nuove che trovo nei record in caso non esistano
    $nazioni_filtered = array_values(filterColumn($state_column, 1, true)); // Array con valori unici
    $upload_nazioni_ok = uploadNazioniZone($nazioni_filtered, 'nazioni');

    // Carico i nuovi fornitori che non sono stati trovati nel db
    $fornitori = array_values(array_unique(array_map('trim', array_column($data, 2))));
    $upload_fornitori_ok = uploadFornitori($fornitori, true);
    setMessage('success', 'Fornitori caricati correttamente');

    // Metodo di ottimizzazione:
    // Creo un chunk di array, da 100 alla volta. Poi
    // Creo la query in cui utilizzo on duplicate key update
    // Poi tante query quanti sono i chunks
    
    $days = array(
        'Sun' => 0,
        'Mon' => 1,
        'Tue' => 2,
        'Wed' => 3,
        'Thu' => 4,
        'Fri' => 5,
        'Sat' => 6
    );

    for ($i=0; $i < sizeof($codice_column); $i++) { 
        $sql = 'INSERT INTO servizi_hotel_tariffe (ID, IDHOTEL, CHILDAGE, FAMILYPLAN, DATABEGIN, DATAEND, BOOKINGBEGIN,
            BOOKINGEND, DAYAPPL, DESCRIZIONE, CANCPOLICY, PREZZO_1, PREZZO_2, PREZZO_3, PREZZO_4) VALUES ';
        for ($c=0; $c < sizeof($codice_column[$i]); $c++) {
            $sql_hotel = " SELECT ID, IDCITTA FROM servizi_hotel WHERE CODICE ='".$codice_column[$i][$c]."'";
            $res_hotel = mysql_fetch_assoc(mysql_query($sql_hotel)); 
            $id_hotel = $res_hotel['ID'];
            $citta_hotel = $res_hotel['IDCITTA'];
            // Se non è presente un hotel dalla ricerca allora inserisco nel db
            // una riga con descrizione ### così sappiamo che lo abbiamo caricato
            // noi e c'è stato un problema. Altrimenti continuo e inserisco la 
            // città prendendola dalle tariffe
            if ($id_hotel == 0) {
                // Carico il servizio hotel
                $sql_citta = " SELECT ID from citta WHERE upper(REPLACE(CODICE, ' ', '')) 
                                LIKE upper(REPLACE('%{$citta_cod}%', ' ', '')) LIMIT 1";
                $res_citta = mysql_fetch_assoc(mysql_query($sql_citta));
                $id_citta = $res_citta['ID'];
                $id_nazione = $res_citta['IDNAZIONE'];
                $sql_hotel = " INSERT INTO servizi_hotel VALUES (";
                $sql_hotel += "
                null, '1', '', '{$vendor_name[$i][$c]}', '{$codice_column[$i][$c]}',
                '{$vendor_name[$i][$c]}', '$id_citta', '$id_nazione', '{$indirizzo_1_column[$i][$c]}',
                '{$indirizzo_2_column[$i][$c]}', '{$zip_column[$i][$c]}', '0',
                '', '', '', '', '', '{$child_age_column[$i][$c]}', '', '', '',
                '', '', '', '0'
                ";
                $sql_hotel += ")";
                mysql_query($sql_hotel);
                $id_hotel = db_get_id();
            } else {
                // Controllo se la città è stata valorizzata correttamente
                if ($citta_hotel == 0) {
                    // Log:
                    // $handle = fopen('log.txt', 'a');
                    // fwrite($handle, "Id citta nulla. codice tariffa: {$codice_column[$i][$c]}  \n");
                    // fclose($handle);
                    
                    // Devo caricare la citta dal campo delle tariffe
                    // Devo togliere anche gli spazi perchè alcuni record
                    // delle tariffe hanno uno o più spazi rispetto al file
                    // delle città.
                    $sql_citta = " SELECT ID from citta WHERE upper(REPLACE(DESCRIZIONE, ' ', '')) 
                                    LIKE upper(REPLACE('%{$citta_column[$i][$c]}%', ' ', '')) LIMIT 1";
                    $res_citta = mysql_fetch_assoc(mysql_query($sql_citta));
                    $id_citta_tariffe = $res_citta['ID'];
                    $sql_update_citta = "UPDATE servizi_hotel SET IDCITTA = $id_citta_tariffe
                                WHERE ID = $id_hotel";
                    mysql_query($sql_update_citta);
                    if (mysql_error()) {
                        $handle = fopen('log.txt', 'a');
                        fwrite($handle, 'Problema nella query inserimento citta nelle
                            tariffe: '.mysql_error().'. Query: '.$sql_update_citta.'. \n
                            sql citta: '.$sql_citta);
                        fclose($handle);
                    }
                }
            }

            $booking_begin_column[$i][$c] = (!empty($booking_begin_column[$i][$c])) ? date_format(date_create($booking_begin_column[$i][$c]), "Y-m-d") : 'NULL' ;
            $booking_end_column[$i][$c] = (!empty($booking_end_column[$i][$c])) ? date_format(date_create($booking_end_column[$i][$c]), "Y-m-d") : 'NULL' ;
            $begin_date_column[$i][$c] = (!empty($begin_date_column[$i][$c])) ? date_format(date_create($begin_date_column[$i][$c]), "Y-m-d") : 'NULL' ;
            $end_date_column[$i][$c] = (!empty($end_date_column[$i][$c])) ? date_format(date_create($end_date_column[$i][$c]), "Y-m-d") : 'NULL' ;

            $price_1_column[$i][$c] = number_format($price_1_column[$i][$c], 2, '.', '');
            $price_2_column[$i][$c] = number_format($price_2_column[$i][$c], 2, '.', '');
            $price_3_column[$i][$c] = number_format($price_3_column[$i][$c], 2, '.', '');
            $price_4_column[$i][$c] = number_format($price_4_column[$i][$c], 2, '.', '');

            // $handle = fopen('log.txt', 'a');
            // fwrite($handle, "Id Hotel: {$id_hotel} Prezzo: {$price_1_column[$i][$c]} \n");
            // fclose($handle);

            $sql .= "( null, '".$id_hotel."', '".$childage_column[$i][$c]."', '".$familyplan_column[$i][$c]."', '".$begin_date_column[$i][$c]."', 
            '".$end_date_column[$i][$c]."', '".$booking_begin_column[$i][$c]."', '".$booking_end_column[$i][$c]."', '".$days_query."', 
            '".$descrizione_column[$i][$c]."', '".$cancellation_policy_column[$i][$c]."', '".$price_1_column[$i][$c]."', '".$price_2_column[$i][$c]."', '".$price_3_column[$i][$c]."','".$price_4_column[$i][$c]."'),";
        }
        $sql = substr($sql, 0, -1);
        $sql .= ' ON DUPLICATE KEY UPDATE 
        ID=VALUES(ID), IDHOTEL=VALUES(IDHOTEL), CHILDAGE=VALUES(CHILDAGE), FAMILYPLAN=VALUES(FAMILYPLAN), DATABEGIN=VALUES(DATABEGIN), DATAEND=VALUES(DATAEND), BOOKINGBEGIN=VALUES(BOOKINGBEGIN),
                    BOOKINGEND=VALUES(BOOKINGEND), DAYAPPL=VALUES(DAYAPPL), DESCRIZIONE=VALUES(DESCRIZIONE), CANCPOLICY=VALUES(CANCPOLICY), PREZZO_1=VALUES(PREZZO_1), PREZZO_2=VALUES(PREZZO_2), PREZZO_3=VALUES(PREZZO_3), PREZZO_4=VALUES(PREZZO_4)';

        mysql_query($sql);
        if (mysql_error()) {
            setMessage('error', 'Problema nella query delle tariffe: '.$sql.' \n '.mysql_error());
            return false;
        }
    }   
    
    setStatusOfRequest(100);
    return true;

}

/**
 * Carica le nazioni, le zone e le citta nel db.
 * Se già presente un record lo aggiorna
 * @param  array &$data i dati letti dal csv
 * @return boolean        se il caricamento è avvenuto o meno
 */
function uploadCitta(&$data)
{
    // Gestione delle colonne delle zone e delle nazioni
    $nazioni_column =filterColumn($data, 2);
    $nazioni_filtered = array_values(array_filter(array_map('trim', array_unique($nazioni_column)))); // Array con valori unici

    $zone_column = filterColumn($data, 3);
    $zone_filtered = array_values(array_filter(array_map('trim', array_unique($zone_column))));

    $cod_citta_column = filterColumn($data, 0);
    $desc_citta_column = filterColumn($data, 1);

    // Se riesce a caricare i valori prosegue

    $upload_nazioni_ok = uploadNazioniZone($nazioni_filtered, 'nazioni');
    setStatusOfRequest(25);
    $upload_zone_ok  = uploadNazioniZone($zone_filtered, 'zone'); 
    setStatusOfRequest(50);

    if ($upload_nazioni_ok && $upload_zone_ok) {
        // Inserimento nella tabella citta
        for ($i=0; $i < sizeof($data); $i++) {
            // Recupero id_zona e id_nazione per la riga

            if (abs($i - sizeof($data)/2) < 1) setStatusOfRequest(75);

            $sql_zone = " SELECT ID from zone WHERE CODICE = '".$zone_column[$i]."'";
            $id_zona = mysql_fetch_assoc(mysql_query($sql_zone))['ID'];

            $sql_nazioni = " SELECT ID from nazioni WHERE CODICE = '".$nazioni_column[$i]."'";
            $id_nazione = mysql_fetch_assoc(mysql_query($sql_nazioni))['ID'];

            $id_zona = (empty($id_zona)) ? 'null' : $id_zona;
            $id_nazione = (empty($id_nazione)) ? 'null' : $id_nazione;

            // Controllo presenza di un codice esistente
            $sql = "
                SELECT ID from citta WHERE upper(REPLACE(CODICE, ' ', '')) 
                            LIKE upper(REPLACE('%{$data[$i][0]}%', ' ', ''))
                            OR upper(REPLACE(DESCRIZIONE, ' ', ''))
                            LIKE upper(REPLACE('%{$data[$i][0]}%')) LIMIT 1
            ";
            $query = mysql_query($sql);
            if (mysql_num_rows($query) > 0) {
                $id = mysql_fetch_assoc($query)['ID'];
                $sql_update = "UPDATE citta SET 
                CODICE = '".$cod_citta_column[$i]."', 
                DESCRIZIONE ='".$desc_citta_column[$i]."',
                IDNAZIONE = ".$id_nazione.",
                IDZONA = ".$id_zona."
                WHERE ID = ".$id;
                
                mysql_query($sql_update);
                if (mysql_error()) {
                    setMessage('error', 'Errore aggiornamento del record in citta: '.mysql_error());
                    return false;
                }
            } else {
                $sql = "INSERT INTO citta (ID, CODICE, DESCRIZIONE, IDNAZIONE, IDZONA)
                VALUES ( null, '".$cod_citta_column[$i]."', '".$desc_citta_column[$i]."', ".$id_nazione.", ".$id_zona." )";
                $query = mysql_query($sql);
                if (mysql_error()) {
                    setMessage('error', 'Errore inserimento del record in citta: '.mysql_error());
                    return false;
                }
            }
        }
        return true;
    }

    return false;
}

/**
 * Inserisce o aggiorna i file nel database
 * @param  string $type citta, servizi, tariffe
 * @param  file $file file di lettura da caricare
 * @return boolean se tutto è andato a buon fine
 */
function updateDatabase($type, $filename)
{
    $header  = array();
    $data    = array();
    $success = false;
    $i       = 0;

    switch ($type) {
        case 'citta':
            // Lettura dei file con intestazione
            $file = fopen($filename, 'r');
            while(!feof($file)) {
                if ($i === 0) {
                    $header = fgetcsv($file, 1024);
                }
                $data[] = fgetcsv($file, 1024);
                $i++;
            }
            fclose($file);
            // Controllo minimamente l'intestazione per vedere se è corretta
            if (sizeof($header) !== 4) {
                setMessage('error', 'Il file caricato non ha un\' intestazione corretta');
                return $success;
            }
            $success = uploadCitta($data);
            break;

        case 'servizi':
           $file = fopen($filename, 'r');
            // Lettura dei file con intestazione
            while(!feof($file)) {
                if ($i === 0) {
                    $header = fgetcsv($file, 1024);
                }
                $data[] = fgetcsv($file, 1024);
                $i++;
            }
            fclose($file);
            // Controllo sull'intestazione
            if (sizeof($header) !== 27) {
                setMessage('error', 'Il file caricato non ha un\' intestazione corretta');
                return $success;
            }
            $success = uploadServizi($data);
            break;

        case 'tariffe':
            // Controllo sulle tariffe: devo utilizzare
            // un chunk per leggere i dati visto che il
            // file è molto grande. Di conseguenza carico
            $line_chunk_size = 20000;
            $chunk_pointer = 1;
            $fileObject = new SplFileObject($filename);
            while (!$fileObject->eof()) {
                $data = array();
                $fileObject->seek($chunk_pointer);
                for ($i=$chunk_pointer; $i < $line_chunk_size + $chunk_pointer; $i++) { 
                    $data[] = $fileObject->fgetcsv();
                }
                $chunk_pointer = $fileObject->key();
                $success = uploadTariffe($data);
                if (!$success) {
                    $fileObject = null;
                    return $success;
                }
            }
            $fileObject = null;
            break;

        case 'tariffe-alaska':
            // Controllo sulle tariffe: devo utilizzare
            // un chunk per leggere i dati visto che il
            // file è molto grande. Di conseguenza carico
            $line_chunk_size = 1000;
            $chunk_pointer = 1;
            $fileObject = new SplFileObject($filename);
            while (!$fileObject->eof()) {
                $data = array();
                $fileObject->seek($chunk_pointer);
                for ($i=$chunk_pointer; $i < $line_chunk_size + $chunk_pointer; $i++) { 
                    $data[] = $fileObject->fgetcsv();
                }
                $chunk_pointer = $fileObject->key();
                $success = uploadTariffeAlaska($data);
                if (!$success) {
                    $fileObject = null;
                    return $success;
                }
            }
            $fileObject = null;
            break;

        case 'tariffe-travalco':
            // Controllo sulle tariffe: devo utilizzare
            // un chunk per leggere i dati visto che il
            // file è molto grande. Di conseguenza carico
            $line_chunk_size = 20000;
            $chunk_pointer = 1;
            $fileObject = new SplFileObject($filename);
            $fileObject->setFlags(SplFileObject::SKIP_EMPTY | SplFileObject::DROP_NEW_LINE);
            while (!$fileObject->eof()) {
                $data = array();
                $fileObject->seek($chunk_pointer);
                for ($i=$chunk_pointer; $i < $line_chunk_size + $chunk_pointer; $i++) { 
                    $line = $fileObject->fgetcsv();
                    if (db_is_null($line)) {
                        $handle = fopen('log.txt', 'a');
                        fwrite($handle, "Riga: $i \n Linea: $line");
                        fclose($handle);
                    } else {
                        $data[] = $line;
                    }
                }
                $chunk_pointer = $fileObject->key();
                $success = uploadTariffeTravalco($data);
                if (!$success) {
                    $fileObject = null;
                    return $success;
                }
            }
            $fileObject = null;
            break;

        case 'tariffe-rmht':
            // Controllo sulle tariffe: devo utilizzare
            // un chunk per leggere i dati visto che il
            // file è molto grande. Di conseguenza carico
            $line_chunk_size = 20000;
            $chunk_pointer = 1;
            $fileObject = new SplFileObject($filename);
            while (!$fileObject->eof()) {
                $data = array();
                $fileObject->seek($chunk_pointer);
                for ($i=$chunk_pointer; $i < $line_chunk_size + $chunk_pointer; $i++) { 
                    $data[] = $fileObject->fgetcsv();
                }
                $chunk_pointer = $fileObject->key();
                $success = uploadTariffeRMHT($data);
                if (!$success) {
                    $fileObject = null;
                    return $success;
                }
            }
            $fileObject = null;
            break;

        default:
            setMessage('error', 'Non è stato selezionato un tipo valido');
            setStatusOfRequest(0);
            $success = false;
            break;
    }

    return $success;

}

/**
 * Carica il file e restituisce un messaggio di ritorno
 * 
 */
function uploadFile()
{
    global $messages;
    $response = null;
    if (isset($_POST) && !db_is_null($_POST)) {
        $file_name = basename($_FILES['file']['name']);
        $file_extension = pathinfo($file_name, PATHINFO_EXTENSION);
        //Validazione del tipo di estensione
        if ($file_extension != 'txt' && $file_extension != 'csv') {
            setMessage('error', 'Non e\' stato selezionato alcun file o il file caricato non ha un\' estensione valida');
        }

        if ($_FILES['file']['error'] == UPLOAD_ERR_OK && is_uploaded_file($_FILES['file']['tmp_name'])) {
            $filename = $_FILES['file']['tmp_name'];
            $success = updateDatabase($_POST['tipo'], $filename);
            if ($success) {
                setMessage('success', 'Valori inseriti correttamente');
                //setStatusOfRequest(0);
            } else {
                setMessage('error', 'Si è presentato un problema nell\'elaborazione del file');
            }
        }
    } else {
        // Non è stata inviata una request corretta
        setMessage('error', 'Non è stata inviata una richiesta corretta al server');
    }
}

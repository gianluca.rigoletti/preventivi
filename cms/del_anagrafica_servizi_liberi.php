<?php

require '../Librerie/connect.php';

$Titolo = "Annnullamento Servizi";
$Tavola= "servizi_liberi";

$indietro = "vis_anagrafica_servizi_liberi.php";

if (isset($_GET['id'])) {
    db_delete($Tavola, $_GET['id']);
    header("Location: $indietro");
    exit;
}
header("Location: $indietro");
exit;
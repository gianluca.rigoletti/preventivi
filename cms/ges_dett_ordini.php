<?php
$m = 'ordine_dett';

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';
require 'utilities_preventivo.php';

$c_files = new files();
$tavola = 'ordine_dett';
$indietro = 'vis_ordini.php';
$page_number = 0;
$current_date = '' ;
$current_next_date = '';
$post_data = array();



if (isset($_GET['datein']) && !db_is_null($_GET['datein'])) {
    $current_begin_date = $_GET['datein'];
}
if (isset($_GET['dateout']) && !db_is_null($_GET['dateout'])) {
    $current_end_date = $_GET['dateout'];
}
if (isset($_GET['back'])) {
    $indietro = $_GET['back'].'.php';
}
if (isset($_GET['id_ordine'])) {
    $cur_rec['IDORDINE'] = $_GET['id_ordine'];
}
if (isset($_GET['add_riga'])) {

}
if (isset($_GET['p_upd']) && $_GET['p_upd'] == 1) {
    $funzione = 'Update';
    $disabilita_chiave = 'disabled';
    $titolo = 'Modifica preventivo';
} else {
    $funzione = 'Insert';
    $titolo = 'Configura preventivo';
}
if (isset($_GET['p_upd']) && $_GET['p_upd'] == 1) {
    $risultato = db_query_mod($tavola, $_GET['p_id']);
    $cur_rec = mysql_fetch_assoc($risultato);
}
if (isset($_POST['Insert']) || isset($_POST['Update'])) {
    $cur_rec['ID']                = $_POST['ID'];
    $cur_rec['IDORDINE']          = $_POST['IDORDINE'];
    $cur_rec['IDTIPO_SERVIZIO']   = $_POST['IDTIPO_SERVIZIO'];
    $cur_rec['IDSERVIZIO']        = $_POST['IDSERVIZIO'];
    $cur_rec['IDTARIFFA']         = $_POST['IDTARIFFA'];
    $cur_rec['DESCRIZIONE']       = $_POST['DESCRIZIONE'];
    $cur_rec['BEGIN']             = $_POST['BEGIN'];
    $cur_rec['END']               = $_POST['END'];
    $cur_rec['PREZZO']            = $_POST['PREZZO'];
    $cur_rec['TIPO_SISTEMAZIONE'] = $_POST['TIPO_SISTEMAZIONE'];
    $cur_rec['PAX_SISTEMATI']     = $_POST['PAX_SISTEMATI'];

    if (!$c_err->is_errore()) {
        if (isset($_POST['Insert'])) {
            db_insert($tavola, $_POST);
        } else {
            db_update($tavola, $_POST['ID'], $_POST);
        }
        header('Location: vis_ordini.php');
        exit;
    }
}
if (isset($_POST['Return'])) {
    header('Location: '.$indietro);
    exit;
}

//Inserimento di un nuovo ordine nel db
if (isset($_POST['New'])) {
    $post_data = parse_post_data($_POST);
    //die(var_dump($post_data));
    $dates = $_POST['reservation'];
    $dates_and_days = split_into_pages($dates);
    $num_pax = get_number_pax($post_data);
    $current_date = $dates_and_days[0]['date'];
    $current_next_date = $dates_and_days[1]['date'];

    $sql = "INSERT INTO ordine VALUES
    (null, '$post_data[desc]', '$post_data[begin_date]', '$post_data[end_date]',
    $num_pax, '$post_data[int]', '$post_data[mail]', '$post_data[phone]', '$post_data[markup]',
    '$post_data[markup_type]', '$post_data[note]', '$post_data[confermato]','$post_data[cambio]')";
    //die($sql);
    mysql_query($sql);
    if (mysql_error()) {
        die("Errore nell'inserimento dell'ordine nel db: ".mysql_error());
    }

    $cur_rec['IDORDINE'] = db_get_id();
    foreach ($post_data['rooms'] as $room) {
        $childage = implode(';', $room['childs']);
        $sql_camere = "INSERT INTO ordine_camere VALUES
        (null, $cur_rec[IDORDINE], $room[type], '$childage')";
        mysql_query($sql_camere);
        if (mysql_error()) {
            die("Errore nell'inserimento dell'ordine nel db: ".mysql_error());
        }
    }
} else {
    $id = $_GET['id_ordine'];
    $post_data = parse_post_data_from_db($id);
}

require '../Librerie/ges_html_top.php';
$c_err->mostra();
?>
<div class="row">
    <div class="col-xs-12 text-center">
        <div class="col-xs-12 col-sm-4">
            <h2> Descrizione </h2>
            <p><?php echo $post_data['desc']; ?></p>
        </div>
        <div class="col-xs-6 col-sm-4 text-center">
            <h2> Data partenza </h2>
            <span><?php echo $post_data['begin_date']; ?></span>
        </div>
        <div class="col-xs-6 col-sm-4 text-center">
            <h2> Data arrivo </h2>
            <span><?php echo $post_data['end_date']; ?></span>
        </div>
    </div>
</div>
<div class="row" style="height:50px;"></div>
<form id="formG" class="form-horizontal form-label-left"  action="" method="post" novalidate>
<input type="hidden" name="ID" value="<?php if (isset($cur_rec['ID'])) echo $cur_rec['ID']; ?>" >
 <div class="form-group">
 


<label class="control-label col-md-3 col-sm-3 col-xs-12"  for="reservation"> Date partenza e arrivo</label>

    <div class="control-group col-md-6 col-sm-6 col-xs-12" style="display:table; margin:0 auto;">
        <div class="controls">
            <div class=" col-xs-12 xdisplay_inputx form-group has-feedback">
                <input type="text" class="form-control has-feedback-left active" id="date" placeholder="Date" aria-describedby="inputSuccess2Status">
                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                <span id="inputSuccess2Status" class="sr-only">(success)</span>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
<label class="control-label col-md-3 col-sm-3 col-xs-12"  for="first-name">Città
</label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <input type="text" name="citta" id="citta" required="required" class="form-control col-md-7 col-xs-12" value="<?php if (isset($citta_id)) echo $citta_id; ?>">
        <input type="hidden" id="codice-citta" name="codice-citta" value="">
    </div>
</div>

<div class="camere">
    <div class="item form-group">
        <label for="numero-camere" class="control-label col-md-3">Numero camere</label>
        <div class="col-xs-12 col-md-6">
            <select id="numero-camere" name="numero-camere" class="form-control" required="">
                <option value="0">Default</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
            </select>
        </div>
    </div>
    <div class="item form-group col-xs-12 col-md-9"></div>
    <div class="item form-group tipo-camere col-xs-12"></div>
    <div class="item form-group numero-bambini col-xs-12"></div>
</div>

<div class="form-group text-center">
    <?php if (isset($_GET['add_riga'])): ?>
         <a class="btn btn-primary btn-center" href="vis_dettaglio_ordini.php?p_upd=1&p_id=<?php echo $_GET['id_ordine']; ?>"> Indietro </a>
     <?php elseif (isset($cur_rec['IDORDINE'])): ?>
         <a class="btn btn-primary btn-center" href="vis_dettaglio_ordini.php?p_upd=1&p_id=<?php echo $cur_rec['IDORDINE'] ?>"> Riepilogo </a>
     <?php endif ?> 
    <button type="submit" class="btn btn-primary btn-center" id="cerca-citta">Cerca</button>
</div>

<div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-md-offset-3">
      <!-- <button class="cancel btn btn-primary" type="submit" name="Return" value="Return">Indietro</button>
      <button type="submit"  class="btn btn-success" name="<?php echo $funzione ?>" value="Salva">Salva</button> -->
  </div>
</div>
</form>

<!-- <form class="form-horizontal col-md-12 hotels" action="" method="POST"> -->
<div class="hotels">
    <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-6">
            <?php
                // if (isset($_GET['add_riga'])) {
                //     echo '<button value="salva" class="btn btn-primary submit">Salva</button>';
                // } else {
                //     echo '<button value="next" class="btn btn-primary submit">Giorno successivo</button>';
                // }
            ?>
        </div>
    </div>
</div>

<!-- </form> -->


<script>
    $(document).ready(function() {
        /**
         * Gestione delle camere
         */
        var numeroCamere, colLength;
        // Genera il numero di camere
        $('#numero-camere').change(function () {
            numeroCamere = $('#numero-camere').val();
            $('.camera-form-group').remove();
            $('.bambini-form-group').remove();
            for (var i = 1; i <= numeroCamere; i++) {
                colLength = 12/numeroCamere;
                var templateCamere = "\
                <div class=\"camera-form-group form-group col-xs-12 col-md-4\">\
                    <label for=\"camera-" + i + "\">\
                        Tipo camera\
                    </label>\
                    <select id=\"camera-" + i + "\" name=\"camera-" + i + "\" class=\"camera-select form-control\" required=\"\">\
                        <option value=\"1\"> singola </option>\
                        <option value=\"2\"> doppia </option>\
                        <option value=\"3\"> tripla </option>\
                        <option value=\"4\"> quadrupla </option>\
                    </select>\
                </div>\
                <div class=\"col-xs-12 col-md-4 bambini-form-group form-group\" id=\"bambini-camera-" + i + "\">\
                </div>\
                <div class=\"col-xs-12 col-md-4 bambini-form-group form-group\" id=\"eta-bambini-camera-camera-" + i + "\">\
                </div>\
                <div class=\"clearfix\"></div>\
                ";
                $('.tipo-camere').append(templateCamere);
            }
        });

        $('.camere').on('change', '.camera-select', function (event) {
            var idCamera = event.target.id;
            var tipoCamera = $(event.target).val();

            $('#bambini-' + idCamera).empty();

            var templateBambini = "\
                <label for=\"bambini-camera-" + i + "\">\
                    Numero bambini\
                </label>\
                <select id=\"bambini-camera-" + idCamera + "\" name=\"bambini-camera-" + idCamera + "\" class=\"bambini-select form-control\" required=\"\">";
            for (var i = 0; i <= tipoCamera - 1; i++) {
                templateBambini += "<option value=\"" + i + "\"> " + i + " </option>";
            }
            templateBambini += "\
                </select>";

            $('#bambini-' + idCamera).append(templateBambini);
        });

        $('.camere').on('change', '.bambini-select', function (event) {
            var idBambini = event.target.id;
            var numeroBambini = $(event.target).val();
            $('#eta-' + idBambini).empty();
            colLength = 12 / numeroBambini;

            for (var i = 1; i <= numeroBambini ; i++) {
                var templateEta = "\
                <div class=\"col-xs-" + colLength + "\">\
                    <label for=\"" + i + "-" + idBambini + "\">\
                        Età\
                    </label>\
                    <select id=\"eta-" + i + "-" + idBambini + "\" name=\"eta-" + i + "-" + idBambini + "\" class=\"bambini form-control\" required=\"\">";
                for (var eta = 1; eta <= 18; eta++) {
                    templateEta += "<option value=\"" + eta + "\"> " + eta + " </option>";
                }
                templateEta += "\
                    </select>\
                </div>";
                $('#eta-' + idBambini).append(templateEta);
            }
        });

        /**
         * Funzione che restituisce un array delle camere
         * e del numero di bambini, con la stessa struttura
         * che avrebbe l'array info
         * @return {object} dettaglio delle camere
         */
        function getDettaglioCamere() {
            var dettaglioCamere = {};
            dettaglioCamere['rooms_number'] = $('#numero-camere').val();
            var rooms = [];
            // Ciclo sul numero di stanze generate
            $.each($('.camera-select'), function(index, value) {
                // Per ogni stanza prendo il valore selezionato
                var idCamera = 'camera-' + (index + 1);
                var tipoCamera = $('#' + idCamera + ' option:selected').val();
                //dettaglioCamere[idCamera] = tipoCamera;
                // Controllo quanti bambini ci sono per ogni camera
                var numeroBambini = $('#bambini-camera-' + idCamera + ' option:selected').val();
                console.log('Numero Bambini:  ' + numeroBambini);
                // Controllo che effettivamente ci siano dei bambini
                if (numeroBambini) {
                    var childs = [];
                    //dettaglioCamere['bambini-camera-' + idCamera] = numeroBambini;
                    // Prendo i dati relativi alle età dei bambini
                    for (var c = 1; c <= numeroBambini; c++) {
                        var idBambino = 'eta-'+c+'-bambini-camera-'+idCamera;
                        var etaBambino = $('#' + idBambino).val();
                        childs.push(etaBambino);
                        //dettaglioCamere[idBambino] = etaBambino;
                    }
                    rooms.push({
                        type: tipoCamera,
                        child_number: numeroBambini,
                        childs: childs
                    });
                } else {
                    rooms.push({
                        type: tipoCamera,
                        child_number: 0
                    });
                }

                dettaglioCamere['rooms'] = rooms;
            });
            console.debug(dettaglioCamere);    
            return dettaglioCamere;
        }

         /**
          * Gestione delle città e degli hotel
          */
        var actionValue = '<?php
            if (isset($_GET['add_riga'])) {
                echo 'salva';
            } else {
                echo 'next';
            }
        ?>';
        var date = '<?php echo $current_date; ?>';
        var nextDate = '<?php echo $current_next_date; ?>';
        var current_date, current_next_date, current_begin_date, current_end_date,
            invalidBeginDate, invalidEndDate;
        <?php if (isset($_GET['datein']) && isset($_GET['dateout'])): ?>
            current_begin_date = '<?php echo $_GET['datein']; ?>';
            current_end_date = '<?php echo $_GET['dateout']; ?>';
        <?php endif ?>
        if (date) {
            current_begin_date = date;
        }
        if (nextDate) {
            current_end_date = nextDate;
        }
        var info = <?php echo json_encode($post_data); ?>;
        var end_date = info.end_date;
        var id_ordine = <?php echo $cur_rec['IDORDINE']; ?>;
        $('#date').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                },
                startDate: current_begin_date,
                endDate: current_end_date,
                calender_style: "picker_1",
                <?php if (isset($_GET['add_riga'])): ?>
                    startDate: '<?php echo $_GET['datein']; ?>',
                    endDate: '<?php echo $_GET['dateout']; ?>',
                    isInvalidDate: function (date) {
                        invalidBeginDate = new Date('<?php echo $_GET['datein']; ?>');
                        invalidEndDate = new Date('<?php echo $_GET['dateout']; ?>');
                        return (date._d.getTime() + 86400000 < invalidBeginDate.getTime() 
                            || date._d.getTime() >= invalidEndDate.getTime());
                    },
                <?php else: ?>
                    startDate: current_begin_date,
                    endDate: current_end_date,
                    isInvalidDate: function (date) {
                        invalidBeginDate = new Date('<?php echo $post_data['begin_date']; ?>');
                        invalidEndDate = new Date('<?php echo $post_data['end_date']; ?>');
                        return (date._d.getTime() + 86400000 < invalidBeginDate.getTime() 
                            || date._d.getTime() >= invalidEndDate.getTime());
                    },
                <?php endif ?>
                opens: 'left'
                //singleDatePicker: true
            }, function(start, end, label) {
                //implementare data inizio e fine
                current_begin_date = start.format('YYYY-MM-DD');
                console.debug('Begin date: '+current_begin_date);
                current_end_date   = end.format('YYYY-MM-DD');
                console.debug('End date: '+current_end_date);
                current_date = start.format('YYYY-MM-DD');
        });

        // Autocomplete delle città
        $('#citta').autocomplete({
            dataType: 'json',
            paramName: 'term',
            params: {
                tavola: 'citta',
                campo: 'DESCRIZIONE'
            },
            serviceUrl: 'autocomplete.php',
            onSelect: function (suggestion) {
                $('#codice-citta').val(suggestion.data);
            }
        });

        $('#cerca-citta').click(function (event) {
            event.preventDefault();
            var citta = $('#codice-citta').val();
            var roomsChanged = ($('#numero-camere option:selected').val() == 0)? false: true;
            // Se le stanze sono state modificate devo cambiare l'array info
            if (roomsChanged) {
                newRooms = getDettaglioCamere();
                info.rooms_number = newRooms.rooms_number;
                info.rooms = newRooms.rooms;
            }
            console.log(info);
            var data = {
                    info: info,
                    action: 'search',
                    citta: citta,
                    current_begin_date: current_begin_date,
                    current_end_date: current_end_date,
                    current_date: current_date
                }
            $.ajax({
                url: 'controller_preventivo.php',
                method: 'POST',
                data: data,
                success :function (data, text) {
                  console.log(data);
                },
                error: function (request, status, error) {
                     console.log(request);
                     console.log(status);
                     console.log(error);
                }                
            })
            .done(function ( data ) {
                //Svuota l'html degli hotel ma non la citta, le date e le camere
                cleanAll(false, false, false);
                var html;
                if (data == 0) {
                    html = visualizzaNotFound();
                } else {
                    html = visualizzaHotel(data);
                }
                $('.hotels').append(html);
            });
        });

        function templateCamere(hotel) {
            keys = Object.keys(hotel);
            var template = '';
            var title = '';
            var write = false;
            var price = '';
            for (key in keys) {
                var attributes = keys[key];
                switch (attributes) {
                    case 'PREZZO_1':
                        title = 'Singola';
                        price = hotel.PREZZO_1;
                        write = true;
                        break;
                    case 'PREZZO_2':
                        title = 'Doppia';
                        price = hotel.PREZZO_2;
                        write = true;
                        break;
                    case 'PREZZO_3':
                        title = 'Tripla';
                        price = hotel.PREZZO_3;
                        write = true;
                        break;
                    case 'PREZZO_4':
                        title = 'Quadrupla';
                        price = hotel.PREZZO_4;
                        write = true;
                        break;
                    default:
                        write: false;
                }
                if (write) {
                    template += '\
                    <div class="col-md-12">\
                        <h3>' + title + '</h3>\
                        <div class="col-md-2">' + price +'</div>\
                    </div>\
                    ';
                } else {
                    template += '';
                }
            }

            return template;
        }

        function visualizzaHotel(data) {
            var hotels = data.results;
            var template = '';
            for (var hotel in hotels) { //Sto iterando sulle chiavi
                var camere = templateCamere(hotels[hotel]);
                template += '\
                <div class="x_panel hotel-component"> \
                    <div class="x_title">\
                        <h3> ' + hotels[hotel].NOME + ' </h3>\
                        <h5> ' + hotels[hotel].STARRANK + ' Stelle </h5>\
                        <h5> Fornitore : ' + hotels[hotel].FORNITORE + ' </h5>\
                    </div>\
                    <div class="x_content">\
                        <form method="post" class="form-hotels">\
                            <label for="' + hotels[hotel].IDTARIFFA +'" class="col-md-2">\
                            Seleziona questo hotel\
                            </label>\
                            <div class="col-md-2">\
                                <input type="hidden" id="' + hotels[hotel].IDTARIFFA + '" value="' + hotels[hotel].IDTARIFFA + '" name="hotel-name">\
                                <input type="hidden" id="action" name="action" value="'+ actionValue + '">\
                                <button class="btn btn-primary button-hotel">\ Scegli questo hotel </button>\
                            </div>\
                        </form>\
                        <div class="divider"></div>\
                        <div class="col-md-12">\
                            ' + camere + '\
                        </div>\
                    </div>\
                </div>\
                ';
            }
            return template;
        }

        function visualizzaNotFound() {
            var html = '<div class="col-xs-12"><h3 class="text-center">\
            Nessun Hotel trovato \
            </h3></div>';
            return html;
        }

        function cleanAll(cleanDate = true, cleanCitta = true, cleanCamere = true) {
            if (cleanCitta) {
                $('#citta').val('');
            }
            $('.hotel-component').remove();
            if (cleanDate) {
                $('#date').val('');
            }
            // Clean dell'opzione camere
            if (cleanCamere) {
                $('.bambini-form-group').remove();
                $('.camera-form-group').remove();
                $('#numero-camere').val(0);
            }
        }

        $(document).on('submit', '.form-hotels',function (event) {
            event.preventDefault();
            console.log('Form: ' + $(this));
            var form = $(this);
            var selectedHotel = form.find('input[name=hotel-name]').val();
            var idTariffa = form.find('input[name="hotel-name"]').val();
            var action = $('#action').attr('value');
            console.log(action);
            console.log('Sto inviando i seguenti dati: ' + selectedHotel + 'Data fine: ' + idTariffa);
            console.log('Sto inviando i seguenti dati1: ' + info + 'Data fine: ' + id_ordine);
            console.log('Sto inviando i seguenti dati2: ' + current_begin_date + 'Data fine: ' + current_end_date);
            console.log('Sto inviando i seguenti dati3: ' + current_date);
            $.ajax({
                url: 'controller_preventivo.php',
                method: 'POST',
                data: {
                    action: action,
                    info: info,
                    id_tariffa: idTariffa,
                    id_ordine: id_ordine,
                    current_begin_date: current_begin_date,
                    current_end_date: current_end_date,
                    current_date:   current_date
                },
                success: function (text) {
                  //  alert(text);
                },                  
                error: function (request, status, error) {
                   //  console.log(request);
                   //  console.log(status);
                   //  console.log(error);
                }                    
            })
            .done(function ( data ) {
                if (data.redirect) {
                    window.location.href = 'vis_dettaglio_ordini.php?p_id=' + id_ordine;
                }
                cleanAll();
                current_begin_date = data;
                console.debug('Current begin date: ' + data);
                current_end_date_object = new Date(data);
                console.debug('Current end date object: ' + current_end_date_object);
                current_end_date_object.setDate(current_end_date_object.getDate() + 1);
                current_end_date = current_end_date_object.toISOString().substring(0, 10);
                console.debug('Current end date string: ' + current_end_date);
                $('#date').data('daterangepicker').setStartDate(current_begin_date);
                $('#date').data('daterangepicker').setEndDate(current_end_date);

            });
        });
    });
</script>

<?php require '../Librerie/ges_html_bot.php'; ?>

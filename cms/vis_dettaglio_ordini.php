<?php
$m="dettaglio_ordini";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';
require 'utilities_preventivo.php';

$titolo    = "Dettaglio Preventivo";
$tavola    = "ordine_dett";
$id_ordine = '';
if (isset($_GET['p_id']) && !db_is_null($_GET['p_id'])) {
  $id_ordine = $_GET['p_id'];
} else {
  echo "
  <script> window.location.href = 'vis_ordini.php'; </script>
  ";
}
$ordine = db_query_mod('ordine', $id_ordine);
$res_ordine = mysql_fetch_assoc($ordine);
$query = db_query_generale($tavola, " IDORDINE = $id_ordine ", 'BEGIN');


require '../Librerie/ges_html_top.php';
?>

<div class="col-md-12 col-sm-12 col-xs-12">
  <?php if (!dates_ok($id_ordine)): ?>
    <div class="alert alert-danger"> <h4> Controlla i seguenti intervalli di date! </h4>
    <ul>
      <?php
        $errors = manage_mismatching_dates($id_ordine);
        foreach ($errors as $error):
           $econt =   get_begin_end_date_arr($error['content']);
           $b = $econt[0];
           $e = $econt[1];
      ?>
        <li>
          <ul class="list-unstyled"> 
            <li><strong>Tipo: </strong><?php echo $error['type']; ?></li>
            <li><strong>Date: </strong><?php echo $b->format('Y-m-d'); ?> - <?php echo $e->format('Y-m-d'); ?></li>
          </ul>
        </li>
      <?php
        endforeach;
      ?>
    </ul>
    </div>
    
  <?php endif ?>
  <div class="x_panel">
    <div class="x_title">
      <h2> Visualizzazione Preventivo </h2>
      <ul class="nav navbar-right panel_toolbox">
        <button class="btn btn-round btn-primary" type="button" onclick="location.href='ges_dett_ordini.php?add_riga=true&servizi_liberi=1&id_ordine=<?php echo $res_ordine['ID']; ?>&datein=<?php echo $res_ordine['DATAIN']; ?>&dateout=<?php echo $res_ordine['DATAOUT']; ?>'">Aggiungi Riga</button>
        <a class="btn btn-round btn-primary" type="button" href="ges_servizi_liberi.php?id_ordine=<?php echo $res_ordine['ID']; ?>&datein=<?php echo $res_ordine['DATAIN']; ?>&dateout=<?php echo $res_ordine['DATAOUT']; ?>">Aggiungi Servizio Libero</a>
      </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_title">
      <h3> <?php echo $res_ordine['NOME_INT']; ?> </h3>
        <div class="clearfix"></div>
    </div>
    <div class="x-content" style="min-height: 8rem; ">
      <div class="col-md-12">
          <?php echo $res_ordine['DESCRIZIONE']; ?>
      </div>
      <div class="col-md-2">
        <h2> Data arrivo </h2>
        <span> <?php echo $res_ordine['DATAIN']; ?> </span>
      </div>
      <div class="col-md-2">
        <h2> Data partenza </h2>
        <span> <?php echo $res_ordine['DATAOUT']; ?> </span>
      </div>
      <div class="col-md-2">
        <h2> Prezzo  </h2>
        <span> <?php
                     $prezzo = calcola_prezzo($res_ordine['ID']);
                     echo db_visimporti($prezzo['prezzo']); ?> </span>
      </div>
      <div class="col-md-2">
        <h2> Tipo markup </h2>
        <span>
            <?php
                if ($res_ordine['MARKUPTYPE'] == 1) {
                    echo "Percentuale";
                } else {
                    echo "Assoluto";
                }
            ?>
        </span>
      </div>
      <div class="col-md-2">
        <h2> Markup </h2>
        <span> <?php echo db_visimporti($res_ordine['MARKUP']); ?> </span>
      </div>
      <div class="col-md-2">
        <h2> Totale </h2>
        <span> <?php
                 echo db_visimporti($prezzo['prezzo_totale']); ?> </span>
      </div>
      <div class="col-md-12" style="margin:10px;">
 
      </div>
   
      <div class="col-md-2">
        <strong>Valori in Euro</strong> 
      </div>
      <div class="col-md-2">
          cambio : <?php
                     echo db_visnumeric($res_ordine['CAMBIO'],4); ?> 
      </div>
      <div class="col-md-2">
        <span> <?php
                     echo db_visimporti($prezzo['prezzo']*$res_ordine['CAMBIO']); ?> </span>
      </div>
      <div class="col-md-2">
      </div>
      <div class="col-md-2">
        <span> <?php echo db_visimporti($res_ordine['MARKUP']*$res_ordine['CAMBIO']); ?> </span>
      </div>
      <div class="col-md-2">
        <span> <?php
                 echo db_visimporti($prezzo['prezzo_totale']*$res_ordine['CAMBIO']); ?> </span>
      </div>      
    </div>
      <div class="col-md-12" style="margin:10px;">
 
      </div>    
    <div class="x_content">
      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
          <tr>
          <th  width="5%"> &nbsp;</th>
          <th width="15%"> Data In </th>
          <th width="15%"> Data Out </th>
          <th width="15%"> Descrizione </th>
          <th width="5%"> Prezzo </th>
          <th width="5%"> Euro </th>
          <th width="5%"> Quantità </th>
          <th width="15%"> Città </th>
          
          <th width="15%"> Fornitore </th>
          <th width="45%"> Note </th>
          
        </thead>
        <tbody>
        <?php
        while ($cur_rec = mysql_fetch_assoc($query)) {
           
            $quantita = ($cur_rec['PAX_SISTEMATI'] != 0) ? $cur_rec['PAX_SISTEMATI'] : '' ;
            
             
            
            $f = "";$citta = '';
            if (!db_is_null($cur_rec['IDTARIFFA'])) {    
                $fornitore = mysql_fetch_assoc(db_query_mod('servizi_hotel', $cur_rec['IDSERVIZIO']));
                if (isset($fornitore['IDCITTA'])) {
                  $res_citta = mysql_fetch_assoc(db_query_mod('citta', $fornitore['IDCITTA']));
                  $citta = $res_citta['DESCRIZIONE'];
                } 
            $fornitoreS = mysql_fetch_assoc(db_query_mod('servizi_hotel_tariffe', $cur_rec['IDTARIFFA']));
            $f =  isset($fornitoreS['FORNITORE']) ? $fornitoreS['FORNITORE'] :  "";
            }
            echo "
            <tr >
              <td ><a href=\"Javascript:ut_delete_rec(".$cur_rec['ID'].",'del_dettaglio_ordini', ".$res_ordine['ID'].")\"><i class=\"fa fa-trash\"></i></a>
              <td >".$cur_rec['BEGIN']." </td>
              <td >".$cur_rec['END']." </td>
              <td >".$cur_rec['DESCRIZIONE']." </td>
              <td >".db_visimporti($cur_rec['PREZZO'])." </td>
              <td >".db_visimporti($cur_rec['PREZZO']*$res_ordine['CAMBIO'])." </td>
              <td >".$quantita." </td>
              <td >".$citta." </td>  
                     
              
              <td >".$f." </td>       
              <td >".$cur_rec['NOTE']." </td>              
             
            </tr> ";
        }
        ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="ln_solid"></div>
<div class="form-group">
    <div class="col-md-6 col-md-offset-3">
      <a href="vis_ordini.php" class="cancel btn btn-primary" type="submit" name="Return" value="Return">Indietro</a>
    </div>
</div>

<script>
  $(document).ready(function() {
    $('#datatable-responsive').DataTable({
      "bFilter":true,
      "iDisplayLength": 50,
      "aaSorting": [[ 2, "asc" ]],
      "bStateSave":true,
      "aoColumns": [
        { "bSortable": false },
        { "bSortable": false },
        { "bSortable": false },
        { "bSortable": false },
        { "bSortable": false },
        { "bSortable": false },
        { "bSortable": false },
        { "bSortable": false },
        { "bSortable": false },
        { "bSortable": false }
      ]
    });
  });
</script>

<?php require '../Librerie/ges_html_bot.php'; ?>

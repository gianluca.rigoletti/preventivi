<?php
$m = 'citta';

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';

$c_files = new files();
$tavola = 'citta_destinazione';
$indietro = 'vis_citta_destinazione.php?id=';


if ($_GET['p_upd'] == 1) {
    $funzione = 'Update';
    $disabilita_chiave = 'disabled';
    $titolo = 'Modifica Città';
} else {
    $funzione = 'Insert';
    $titolo = 'Nuova Città';
}
if ($_GET['p_upd'] == 1) {
    $risultato = db_query_mod($tavola, $_GET['p_id']);
    $cur_rec = mysql_fetch_assoc($risultato);
    $_GET['id_padre'] = $cur_rec['IDCITTAFROM'];
}

if (isset($_GET['id_padre'])) {
    $indietro .= $_GET['id_padre'];
}

if (isset($_POST['Insert']) || isset($_POST['Update'])) {
    $cur_rec['ID'] = $_POST['ID'];
    $cur_rec['IDCITTAFROM'] = $_POST['IDCITTAFROM'];
    $cur_rec['IDCITTATO'] = $_POST['IDCITTATO'];
    $cur_rec['ATLANTE'] = $_POST['ATLANTE'];

   
    if (isset($_POST['Insert']) && db_dup_key($tavola, $_POST) > 0) {
         $c_err->add("Città Gi&agrave; Codificato", "ID");
    }
    if (!$c_err->is_errore()) {
        if (isset($_POST['Insert'])) {
            db_insert($tavola, $_POST);
        } else {
            db_update($tavola, $_POST['ID'], $_POST);
        }
        header('Location: '.$indietro);
        exit;
    }
}
if (isset($_POST['Return'])) {
    header('Location: '.$indietro);
    exit;
}

require '../Librerie/ges_html_top.php';
$c_err->mostra();
?>

<form id="formG" class="form-horizontal form-label-left"  action="" method="post" novalidate>
<input type="hidden" name="ID" value="<?php if (isset($cur_rec)) echo $cur_rec['ID']; ?>" >
<input type="hidden" name="IDCITTAFROM" value="<?php  echo $_GET['id_padre']; ?>" >



<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Destinazione *
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
  <select id="IDCITTATO" name="IDCITTATO" required="true" class="form-control col-md-7 col-xs-12"/>
        <?php
        if (isset($cur_rec['IDCITTATO'])) {
            db_html_select_cod('citta', $cur_rec['IDCITTATO'], 'ID', 'DESCRIZIONE', true, null);
        } else {
            db_html_select_cod('citta', '', 'ID', 'DESCRIZIONE', true, null);
        }
        ?>
  </select>
  </div>
</div>


<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Atlante *
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <textarea  required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("ATLANTE");?> name="ATLANTE"  id="ATLANTE" ><?php if (isset($cur_rec)) echo $cur_rec['ATLANTE'];?></textarea>
  </div>
</div>
                        
<div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-md-offset-3">
      <button class="cancel btn btn-primary" type="submit" name="Return" value="Return">Indietro</button>
      <button type="submit"  class="btn btn-success" name="<?php echo $funzione ?>" value="Salva">Salva</button>
  </div>
</div>
</form>
<?php require '../Librerie/ges_html_bot.php'; ?>

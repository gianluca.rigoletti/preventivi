<?php
$m = 'ordine_dett';

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';
require 'utilities_preventivo.php';

$c_files = new files();
$tavola = 'ordine_dett';
$indietro = 'vis_dettaglio_ordini.php?p_upd=1&p_id='.$_GET['id_ordine'];

if (isset($_GET['back'])) {
    $indietro = $_GET['back'].'.php';
}
if (isset($_GET['p_upd']) && $_GET['p_upd'] == 1) {
    $funzione = 'Update';
    $disabilita_chiave = 'disabled';
    $titolo = 'Modifica Servizio Libero';
} else {
    $funzione = 'Insert';
    $titolo = 'Nuovo Servizio Libero';
}
if (isset($_GET['p_upd']) &&$_GET['p_upd'] == 1) {
    $risultato = db_query_mod($tavola, $_GET['p_id']);
    $cur_rec = mysql_fetch_assoc($risultato);
}
if (isset($_GET['id_ordine'])) {
    $cur_rec['IDORDINE'] = $_GET['id_ordine'];
}
if (isset($_GET['datein'])) {
    $cur_rec['BEGIN'] = $_GET['datein'];
}
if (isset($_GET['dateout'])) {
    $cur_rec['END'] = $_GET['dateout'];
}
if (isset($_GET[''])) {
    $cur_rec['IDORDINE'] = $_GET['id_ordine'];
}
if (isset($_GET[''])) {
    $cur_rec['IDORDINE'] = $_GET['id_ordine'];
}
if (isset($_GET[''])) {
    $cur_rec['IDORDINE'] = $_GET['id_ordine'];
}

if (isset($_POST['Insert']) || isset($_POST['Update'])) {
    $dates = array();
    $dates = parse_date_from_post($_POST['DATE']);
    $servizio = mysql_fetch_assoc(db_query_mod('servizi_liberi', $_POST['IDSERVIZIO']));
    $_POST['BEGIN'] = $dates['BEGIN'];
    $_POST['END'] = $dates['END'];
    $cur_rec['ID'] = $_POST['ID'];
    $cur_rec['IDORDINE'] = $_POST['IDORDINE'];
   
    $cur_rec['IDSERVIZIO'] = $_POST['IDSERVIZIO'];
    //$cur_rec['IDTARIFFA'] = $_POST['IDTARIFFA'];
    $cur_rec['DESCRIZIONE'] = $_POST['DESCRIZIONE'];
    $cur_rec['BEGIN'] = $_POST['BEGIN'];
    $cur_rec['END'] = $_POST['END'];
    


    $current_begin_date = new DateTime($_POST['BEGIN']);
    $current_end_date = new DateTime($_POST['END']);
    $days_number = $current_end_date->diff($current_begin_date)->format('%a');
    

    
    

    $cur_rec['PREZZO'] = $_POST['PREZZO'];
    //$cur_rec['TIPO_SISTEMAZIONE'] = $_POST['TIPO_SISTEMAZIONE'];
    $cur_rec['PAX_SISTEMATI'] = $_POST['PAX_SISTEMATI'];

    if (isset($_POST['CODICE'])) {
        $cur_rec['CODICE'] = $_POST['CODICE'];
    }
    if (isset($_POST['DESCRIZIONE'])) {
        $cur_rec['DESCRIZIONE'] = $_POST['DESCRIZIONE'];
    }
    if (isset($_POST['Insert']) && db_dup_key($tavola, $_POST) > 0) {
         $c_err->add("Città Gi&agrave; Codificato", "ID");
    }
    if (!$c_err->is_errore()) {
        
        $_POST['DESCRIZIONE'] =  $servizio['DESCRIZIONE']." - ".$_POST['DESCRIZIONE'];
        if (db_is_null($_POST['PREZZO']))   $_POST['PREZZO'] = db_visimporti($servizio['PREZZO']); 
        $_POST['PREZZO'] = db_visimporti(db_convnum($_POST['PREZZO'])*$_POST['PAX_SISTEMATI']*$days_number);
        $_POST['IDTIPO_SERVIZIO'] =  $servizio['IDTIPOSERVIZIO'];
        
        if (isset($_POST['Insert'])) {
            db_insert($tavola, $_POST);
        } else {
            db_update($tavola, $_POST['ID'], $_POST);
        }
        header('Location: vis_dettaglio_ordini.php?p_upd=1&p_id='.$_POST['IDORDINE']);
        exit;
    }
}
if (isset($_POST['Return'])) {
    header('Location: '.$indietro);
    exit;
}

require '../Librerie/ges_html_top.php';
$c_err->mostra();
?>
<?php 
  $ordine = mysql_fetch_assoc(db_query_generale('ordine', 'ID = '.$cur_rec['IDORDINE']));
  $descrizione = $ordine['DESCRIZIONE'];
?>
<div class="row">
    <div class="col-xs-12 text-center">
        <div class="col-xs-12 col-sm-4">
            <h2> Descrizione </h2>
            <p><?php echo $descrizione; ?></p>
        </div>
        <div class="col-xs-6 col-sm-4 text-center">
            <h2> Data partenza </h2>
            <span><?php echo $cur_rec['BEGIN']; ?></span>
        </div>
        <div class="col-xs-6 col-sm-4 text-center">
            <h2> Data arrivo </h2>
            <span><?php echo $cur_rec['END']; ?></span>
        </div>
    </div>
</div>
<form id="formG" class="form-horizontal form-label-left"  action="" method="post" novalidate>
<input type="hidden" name="ID" value="<?php if (isset($cur_rec['ID'])) echo $cur_rec['ID']; ?>" >
<input type="hidden" name="IDORDINE" value="<?php if (isset($cur_rec)) echo $cur_rec['IDORDINE']; ?>">

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="reservation"> Date partenza e arrivo</label>

  <div class="control-group col-md-6 col-sm-6 col-xs-12" style="display:table; margin:0 auto;">
      <div class="controls ">
          <div class=" col-xs-12 xdisplay_inputx form-group has-feedback">
              <input type="text" class="form-control has-feedback-left active" name="DATE" id="date" placeholder="Date" aria-describedby="inputSuccess2Status">
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
              <span id="inputSuccess2Status" class="sr-only">(success)</span>
          </div>
      </div>
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Descrizione <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <textarea required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("DESCRIZIONE");?> name="DESCRIZIONE"  id="DESCRIZIONE" maxlength="200"><?php if (isset($cur_rec['DESCRIZIONE'])) echo $cur_rec['DESCRIZIONE'];?></textarea>
  </div>
</div>



<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Servizio Libero  <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
  <select id="IDSERVIZIO" name="IDSERVIZIO" required="true" class="form-control col-md-7 col-xs-12"/>
        <?php
        if (isset($cur_rec['IDSERVIZIO'])) {
            db_html_select_cod('servizi_liberi', $cur_rec['IDSERVIZIO'], 'ID', 'DESCRIZIONE', true, null);
        } else {
            db_html_select_cod('servizi_liberi', '', 'ID', 'DESCRIZIONE', true, null);
        }
        ?>
  </select>
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Pax/Quantità <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="number" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("PAX_SISTEMATI");?> name="PAX_SISTEMATI"  id="PAX_SISTEMATI" value="<?php if (isset($cur_rec['PAX_SISTEMATI'])) echo $cur_rec['PAX_SISTEMATI'];?>" size="55" maxlength="6"><br />
  </div>
</div>


<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Prezzo Unitario
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input  importoeuro="true"  class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("PREZZO");?> name="PREZZO"  id="PREZZO" value="<?php if (isset($cur_rec['PREZZO'])) echo db_visimporti($cur_rec['PREZZO']);?>" size="55" maxlength="10"><br />
  </div>
</div>

<div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-md-offset-3">
      <button class="cancel btn btn-primary" type="submit" name="Return" value="Return">Indietro</button>
      <button type="submit"  class="btn btn-success" name="<?php echo $funzione ?>" value="Salva">Salva</button>
  </div>
</div>
</form>

<script>
  var beginDate = '<?php echo $cur_rec['BEGIN']; ?>';
  var endDate = '<?php echo $cur_rec['END']; ?>';
  $('#date').daterangepicker({
          locale: {
              format: 'YYYY-MM-DD'
          },
          startDate: beginDate,
          endDate: endDate,
          calender_style: "picker_1",
          opens: 'left',
          isInvalidDate: function (date) {
              invalidBeginDate = new Date(beginDate);
              invalidEndDate = new Date(endDate);
              return (date._d.getTime() + 86400000 < invalidBeginDate.getTime() 
                  || date._d.getTime() >= invalidEndDate.getTime());
          },
      }, function(start, end, label) {
          //implementare data inizio e fine
          current_begin_date = start.format('YYYY-MM-DD');
          console.debug('End date: '+current_begin_date);
          current_end_date   = end.format('YYYY-MM-DD');
          console.debug('End date: '+current_end_date);
          current_date = start.format('YYYY-MM-DD');
  });
</script>
<?php require '../Librerie/ges_html_bot.php'; ?>

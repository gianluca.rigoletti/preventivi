<?php

require '../Librerie/connect.php';

$Titolo = "Annullamento Servizio Hotel";
$Tavola= "servizi_hotel";

$indietro = "vis_servizi_hotel.php";

if (isset($_GET['id'])) {
    db_delete($Tavola, $_GET['id']);
    header("Location: $indietro");
    exit;
}
header("Location: $indietro");
exit;

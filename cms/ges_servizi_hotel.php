<?php
$m = 'servizi_hotel';

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';

$c_files = new files();
$tavola = 'servizi_hotel';
$indietro = 'vis_servizi_hotel.php';

if (isset($_GET['back'])) {
    $indietro = $_GET['back'].'.php';
}
if ($_GET['p_upd'] == 1) {
    $funzione = 'Update';
    $disabilita_chiave = 'disabled';
    $titolo = 'Modifica Servizio Hotel';
} else {
    $funzione = 'Insert';
    $titolo = 'Nuova Servizio Hotel';
}
if ($_GET['p_upd'] == 1) {
    $risultato = db_query_mod($tavola, $_GET['p_id']);
    $cur_rec = mysql_fetch_assoc($risultato);
}
if (isset($_POST['Insert']) || isset($_POST['Update'])) {
    $cur_rec['ID']              = $_POST['ID'];
    $cur_rec['IDFORNITORE']     = $_POST['IDFORNITORE'];
    $cur_rec['IDVENDOR']        = $_POST['IDVENDOR'];
    $cur_rec['VENDORNAME']      = $_POST['VENDORNAME'];
    $cur_rec['CODICE']          = $_POST['CODICE'];
    $cur_rec['NOME']            = $_POST['NOME'];
    $cur_rec['DESCRIZIONE']     = $_POST['DESCRIZIONE'];
    $cur_rec['STARRANK']        = $_POST['STARRANK'];
    $cur_rec['IDNAZIONE']       = $_POST['IDNAZIONE'];
    $cur_rec['IDCITTA']         = $_POST['IDCITTA'];
    $cur_rec['INDIRIZZO']       = $_POST['INDIRIZZO'];
    $cur_rec['ZIP']             = $_POST['ZIP'];
    $cur_rec['PHONE']           = $_POST['PHONE'];
    $cur_rec['FAX']             = $_POST['FAX'];
    $cur_rec['MAIL']            = $_POST['MAIL'];
    $cur_rec['IMG']             = $_POST['IMG'];
    $cur_rec['LAT']             = $_POST['LAT'];
    $cur_rec['LANG']            = $_POST['LANG'];
    $cur_rec['CHILDAGE']        = $_POST['CHILDAGE'];
    $cur_rec['ROOMTYPE']        = $_POST['ROOMTYPE'];
    $cur_rec['MEALPLAN']        = $_POST['MEALPLAN'];
    $cur_rec['FAMILYPLAN']      = $_POST['FAMILYPLAN'];
    $cur_rec['MAXOCC']          = $_POST['MAXOCC'];
    $cur_rec['NONRIMBORSABILE'] = $_POST['NONRIMBORSABILE'];
    $cur_rec['FEE']             = $_POST['FEE'];
    $cur_rec['FEETYPE']         = $_POST['FEETYPE'];

    if (isset($_POST['Insert']) && db_dup_key($tavola, $_POST) > 0) {
         $c_err->add("Servizio Hotel Gi&agrave; Codificato", "ID");
    }
    if (isset($_POST['NOME']) && ($_POST['NOME'] == null || $_POST['NOME'] == " ")) {
        $c_err->add("Campo Codice NOME Obbligatorio", "NOME");
    }
    if (isset($_POST['IDNAZIONE']) && ($_POST['IDNAZIONE'] == null || $_POST['IDNAZIONE'] == " ")) {
        $c_err->add("Campo Codice IDNAZIONE Obbligatorio", "IDNAZIONE");
    }
    if (isset($_POST['IDCITTA']) && ($_POST['IDCITTA'] == null || $_POST['IDCITTA'] == " ")) {
        $c_err->add("Campo Codice IDCITTA Obbligatorio", "IDCITTA");
    }
    if (isset($_POST['INDIRIZZO']) && ($_POST['INDIRIZZO'] == null || $_POST['INDIRIZZO'] == " ")) {
        $c_err->add("Campo Codice INDIRIZZO Obbligatorio", "INDIRIZZO");
    }
    if (isset($_POST['MAXOCC']) && ($_POST['MAXOCC'] == null || $_POST['MAXOCC'] == " ")) {
        $c_err->add("Campo Codice MAXOCC Obbligatorio", "MAXOCC");
    }
    if (isset($_POST['NONRIMBORSABILE']) && ($_POST['NONRIMBORSABILE'] == null || $_POST['NONRIMBORSABILE'] == " ")) {
        $c_err->add("Campo Codice NONRIMBORSABILE Obbligatorio", "NONRIMBORSABILE");
    }
    if (isset($_POST['FFETYPE']) && ($_POST['FFETYPE'] == null || $_POST['FFETYPE'] == " ")) {
        $c_err->add("Campo Codice FFETYPE Obbligatorio", "FFETYPE");
    }

    if (!$c_err->is_errore()) {
        if (isset($_POST['Insert'])) {
            db_insert($tavola, $_POST);
        } else {
            db_update($tavola, $_POST['ID'], $_POST);
        }
        header('Location: vis_servizi_hotel.php');
        exit;
    }
}
if (isset($_POST['Return'])) {
    header('Location: '.$indietro);
    exit;
}

require '../Librerie/ges_html_top.php';
$c_err->mostra();
?>

<form id="formG" class="form-horizontal form-label-left"  action="" method="post" novalidate>
<input type="hidden" name="ID" value="<?php if (isset($cur_rec)) echo $cur_rec['ID']; ?>" >

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Fornitore</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <select id="IDFORNITORE" name="IDFORNITORE" required="true" class="form-control col-md-7 col-xs-12"/>
    <?php
    if (isset($cur_rec['IDFORNITORE'])) {
        db_html_select_cod('fornitori', $cur_rec['IDFORNITORE'], 'ID', 'CODICE', true, null);
    } else {
        db_html_select_cod('fornitori', '', 'ID', 'CODICE', true, null);
    }
    ?>
    </select>
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Id Vendor</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("NOME");?> name="NOME"  id="NOME" value="<?php if (isset($cur_rec)) echo $cur_rec['NOME'];?>" size="55" maxlength="50"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Vendor Name</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("VENDORNAME");?> name="VENDORNAME"  id="VENDORNAME" value="<?php if (isset($cur_rec)) echo $cur_rec['VENDORNAME'];?>" size="55" maxlength="200"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Codice</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("CODICE");?> name="CODICE"  id="CODICE" value="<?php if (isset($cur_rec)) echo $cur_rec['CODICE'];?>" size="55" maxlength="50"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nome</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("NOME");?> name="NOME"  id="NOME" value="<?php if (isset($cur_rec)) echo $cur_rec['NOME'];?>" size="55" maxlength="200"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Descrizione <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <textarea class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("DESCRIZIONE");?> name="DESCRIZIONE"  id="DESCRIZIONE" maxlength="2000"><?php if (isset($cur_rec)) echo $cur_rec['DESCRIZIONE'];?></textarea>
  </div>
</div>
            
<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Star Rank</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="number" min="1" max="5" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("STARRANK");?> name="STARRANK"  id="STARRANK" value="<?php if (isset($cur_rec)) echo $cur_rec['STARRANK'];?>" size="55"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nazione<i class="fa fa-question" data-toggle="tooltip" data-placement="bottom" title="Nazione dell'hotel" required="true"></i>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <select id="IDNAZIONE" name="IDNAZIONE" required="true" class="form-control col-md-7 col-xs-12"/>
    <?php
    if (isset($cur_rec['IDNAZIONE'])) {
        db_html_select_cod('nazioni', $cur_rec['IDNAZIONE'], 'ID', 'CODICE', true, null);
    } else {
        db_html_select_cod('nazioni', '', 'ID', 'CODICE', true, null);
    }
    ?>
    </select>
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Città <i class="fa fa-question" data-toggle="tooltip" data-placement="bottom" title="Città dell'hotel" required="true"></i>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <select id="IDCITTA" name="IDCITTA" required="true" class="form-control col-md-7 col-xs-12"/>
    <?php
    if (isset($cur_rec['IDCITTA'])) {
        db_html_select_cod('citta', $cur_rec['IDCITTA'], 'ID', 'CODICE', true, null);
    } else {
        db_html_select_cod('citta', '', 'ID', 'CODICE', true, null);
    }
    ?>
    </select>
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Indirizzo</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("INDIRIZZO");?> name="INDIRIZZO"  id="INDIRIZZO" value="<?php if (isset($cur_rec)) echo $cur_rec['INDIRIZZO'];?>" size="55" maxlength="255"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Zip</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="number" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("ZIP");?> name="ZIP"  id="ZIP" value="<?php if (isset($cur_rec)) echo $cur_rec['ZIP'];?>" size="55" maxlength="255"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Telefono</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="number" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("PHONE");?> name="PHONE"  id="PHONE" value="<?php if (isset($cur_rec)) echo $cur_rec['PHONE'];?>" size="55" maxlength="255"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Fax</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="number" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("FAX");?> name="FAX"  id="FAX" value="<?php if (isset($cur_rec)) echo $cur_rec['FAX'];?>" size="55" maxlength="255"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Email</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="email" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("MAIL");?> name="MAIL"  id="MAIL" value="<?php if (isset($cur_rec)) echo $cur_rec['MAIL'];?>" size="55" maxlength="255"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Immagine<i class="fa fa-question" data-toggle="tooltip" data-placement="bottom" title="Url dell'immagine" required="true"></i></label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("IMG");?> name="IMG"  id="IMG" value="<?php if (isset($cur_rec)) echo $cur_rec['IMG'];?>" size="55" maxlength="255"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Latitudine</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="number" minlength="10" maxlength="10" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("LAT");?> name="LAT"  id="LAT" value="<?php if (isset($cur_rec)) echo $cur_rec['LAT'];?>" size="55"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Longitudine</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="number" minlength="10" maxlength="10" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("LANG");?> name="LANG"  id="LANG" value="<?php if (isset($cur_rec)) echo $cur_rec['LANG'];?>" size="55"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Età bambini</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="number" minlength="1" maxlength="2" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("CHILDAGE");?> name="CHILDAGE"  id="CHILDAGE" value="<?php if (isset($cur_rec)) echo $cur_rec['CHILDAGE'];?>" size="55" maxlength="255"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Tipo di stanza</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("ROOMTYPE");?> name="ROOMTYPE"  id="ROOMTYPE" value="<?php if (isset($cur_rec)) echo $cur_rec['ROOMTYPE'];?>" size="55" maxlength="255"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Meal Plan</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("MEALPLAN");?> name="MEALPLAN"  id="MEALPLAN" value="<?php if (isset($cur_rec)) echo $cur_rec['MEALPLAN'];?>" size="55" maxlength="255"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Family Plan</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("MEALPLAN");?> name="MEALPLAN"  id="MEALPLAN" value="<?php if (isset($cur_rec)) echo $cur_rec['MEALPLAN'];?>" size="55" maxlength="255"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Family Plan <i class="fa fa-question" data-toggle="tooltip" data-placement="bottom" title="Se applicabile o meno il piano famiglia"></i>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <select id="FAMILYPLAN" required="true" name="FAMILYPLAN" class="form-control col-md-7 col-xs-12"/>
        <?php
            $valore = 0;
            if (isset($cur_rec['FAMILYPLAN'])) $valore = $cur_rec['FAMILYPLAN'];
            $valori = array(
                '0' => 'Non applicabile',
                '1' => 'Applicabile'
            );
            foreach ($valori as $key => $value) {
                $selected = "";
                if ($key == $valore) $selected = "selected=\"selected\"";
                echo "<option value=\"$key\" $selected>$value</option>";
            }
        ?>
    </select>
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Max OCC<i class="fa fa-question" data-toggle="tooltip" data-placement="bottom" title="Numero max pax per stanza" required="true"></i></label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("MAXOCC");?> name="MAXOCC"  id="MAXOCC" value="<?php if (isset($cur_rec)) echo $cur_rec['MAXOCC'];?>" size="55" min="1" max="9"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Non rimborsabile</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <select id="NONRIMBORSABILE" required="true" name="NONRIMBORSABILE" class="form-control col-md-7 col-xs-12"/>
        <?php
            $valore = 0;
            if (isset($cur_rec['NONRIMBORSABILE'])) $valore = $cur_rec['NONRIMBORSABILE'];
            $valori = array(
                '0' => 'Rimborsabile',
                '1' => 'Non rimborsabile'
            );
            foreach ($valori as $key => $value) {
                $selected = "";
                if ($key == $valore) $selected = "selected=\"selected\"";
                echo "<option value=\"$key\" $selected>$value</option>";
            }
        ?>
    </select>
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Tassa</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="number" maxlength="7" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("FEE");?> name="FEE"  id="FEE" value="<?php if (isset($cur_rec)) echo $cur_rec['FEE'];?>" size="55" maxlength="255"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Tipo di tassa</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <select id="FEETYPE" required="true" name="FEETYPE" class="form-control col-md-7 col-xs-12"/>
        <?php
            $valore = 0;
            if (isset($cur_rec['FEETYPE'])) $valore = $cur_rec['FEETYPE'];
            $valori = array(
                '0' => 'Inclusa',
                '1' => 'Per giorno',
                '2' => 'Per persona',
                '3' => 'Per stanza',
                '4' => '+ tassa per giorno',
                '5' => '+ tassa per persona',
                '6' => '+ tassa per stanza',
                '9' => 'Omaggiata'
            );
            foreach ($valori as $key => $value) {
                $selected = "";
                if ($key == $valore) $selected = "selected=\"selected\"";
                echo "<option value=\"$key\" $selected>$value</option>";
            }
        ?>
    </select>
  </div>
</div>
                        
<div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-md-offset-3">
      <button class="cancel btn btn-primary" type="submit" name="Return" value="Return">Indietro</button>
      <button type="submit"  class="btn btn-success" name="<?php echo $funzione ?>" value="Salva">Salva</button>
  </div>
</div>
</form>
<?php require '../Librerie/ges_html_bot.php'; ?>

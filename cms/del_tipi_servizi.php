<?php

require '../Librerie/connect.php';

$Titolo = "Annullamento Tipo Servizio";
$Tavola= "tipi_servizi";

$indietro = "vis_tipi_servizi.php";

if (isset($_GET['id'])) {
    db_delete($Tavola, $_GET['id']);
    header("Location: $indietro");
    exit;
}
header("Location: $indietro");
exit;

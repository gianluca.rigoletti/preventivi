<?php
$m="zone";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';

$titolo = "Gestione Zone";
$tavola= "zone";
$risultato = db_query_vis($tavola, 'ID');

require '../Librerie/ges_html_top.php';
?>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Gestione Zone </h2>
      <ul class="nav navbar-right panel_toolbox">
        <button class="btn btn-round btn-primary" type="button" onclick="location.href='ges_zone.php?p_upd=0'">Nuovo</button>
      </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
          <tr>
          <th width="20%"> Codice </th>
          <th width="20%"> Descrizione </th>
          <th  width="5%"> &nbsp;</th>
          <th  width="5%"> &nbsp;</th>
        </thead>  
        <tbody> 
        <?php
        while ($cur_rec = mysql_fetch_assoc($risultato)) {
            echo " 
            <tr >
            <td >".$cur_rec['CODICE']." </td>
            <td >".$cur_rec['DESCRIZIONE']." </td>
            <td ><a href=\"ges_zone.php?p_upd=1&p_id=".$cur_rec['ID']."\"><i class=\"fa fa-edit\"></i></a></td>
            <td ><a href=\"Javascript:ut_delete_rec(".$cur_rec['ID'].",'del_zone')\"><i class=\"fa fa-trash\"></i></a>
            </tr> ";
        }
        ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#datatable-responsive').DataTable({
      "bFilter":true,
      "iDisplayLength": 50,
      "aaSorting": [[ 2, "asc" ]], 
      "bStateSave":true,                 
      "aoColumns": [
        { "bSortable": false },
        { "bSortable": false },
        null,
        null
      ]         
    });
  });
</script>  

<?php require '../Librerie/ges_html_bot.php'; ?>

<?php
$m = 'carica_file';

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';

$c_files = new files();
$tavola = 'citta';
$indietro = 'vis_citta.php';

if (isset($_GET['back'])) {
  $indietro = $_GET['back'].'.php';
}

require '../Librerie/ges_html_top.php';
$c_err->mostra();
?>

<div class="col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h1> Gestione caricamento file </h1>
    </div>
    <div class="x_content">
      <div class="row">
        <div class="col-xs-12">
          <div class="progress">
            <div class="progress-bar progress-bar-success progress-bar-striped active" data-transitiongoal="0" aria-valuenow="1" style="width: 0%;"></div>
          </div>          
        </div>
      </div>
    </div>
    <div class="x_content popovers">
    </div>
    <div class="x_content">
      <form id="upload-file" enctype="multipart/form-data">
        <label for="tipo">Seleziona il tipo di file da caricare</label>
        <select id="tipo" name="tipo" class="form-control" required="true">
          <option value="citta">Carica città</option>
          <option value="servizi">Carica fornitori servizi</option>
          <option value="tariffe">Carica tariffe</option>
          <option value="tariffe-alaska">Carica tariffe alaska</option>
          <option value="tariffe-rmht">Carica tariffe RMHT</option>
          <option value="tariffe-travalco">Carica tariffe Travalco
          </option>
        </select>
        <br>
        <label for="file">Carica il file</label>
        <input type="file" name="file" id="file">
        <br>
      </form>
      <a value="upload" id="submit-form" name="submit-form" class="btn btn-success">Invia</a>
    </div>
  </div>
</div> 

<script>
$(document).ready(function() {
  var interval;
  $('#submit-form').click(function () {
    var $btn = $(this).button('loading');
    var formData = new FormData($('#upload-file')[0]);
    $.ajax({
      url: 'ges_carica_file_prima_ottimizzazione.php',
      type: 'POST',
      data: formData,
      cache: false,
      beforeSend: startProgressBar,
      processData: false,
      contentType: false
    }).done(function(response) {
      $.each(response, function(key, value) {
        $('.popovers').append(
          '<div class="alert alert-'+ value.type + ' alert-dismissible fade in"' +
          'role="alert">' +
          '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
          value.content +
          '</div>'
        );
        $btn.button('reset');
        if (value.type != 'error') {
          $('.progress-bar').attr('data-transitiongoal', '100');
          $('.progress-bar').css('width', '100%');
        }
        setTimeout(function () {
          $('.alert').alert('close');
          $('.progress-bar').addClass('no-transition');
          $('.progress-bar').css('width', '0%');
        }, 60000);
        clearInterval(interval);
      });
    });
  });

  function startProgressBar() {
    $('.progress-bar').removeClass('no-transition');
    interval = setInterval(function() {
      $.getJSON('status.json', function (data) {
        console.log(data);
        if (data == 'done') {
          
        }
        $('.progress-bar').attr('data-transitiongoal', data);
        $('.progress-bar').css('width', data + '%');
      });
    }, 2000);
  }
});

</script>

<?php require '../Librerie/ges_html_bot.php'; ?>


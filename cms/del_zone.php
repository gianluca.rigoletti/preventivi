<?php

require '../Librerie/connect.php';

$Titolo = "Annullamento Zona";
$Tavola= "zone";

$indietro = "vis_zone.php";

if (isset($_GET['id'])) {
    db_delete($Tavola, $_GET['id']);
    header("Location: $indietro");
    exit;
}
header("Location: $indietro");
exit;

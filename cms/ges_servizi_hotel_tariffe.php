<?php
$m = 'servizi_hotel_tariffe';

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';

$c_files = new files();
$tavola = 'servizi_hotel_tariffe';
$indietro = 'vis_servizi_hotel_tariffe.php';

if (isset($_GET['back'])) {
    $indietro = $_GET['back'].'.php';
}
if ($_GET['p_upd'] == 1) {
    $funzione = 'Update';
    $disabilita_chiave = 'disabled';
    $titolo = 'Modifica Tariffa Servizio Hotel';
} else {
    $funzione = 'Insert';
    $titolo = 'Nuova Tariffa Servizio Hotel';
}
if ($_GET['p_upd'] == 1) {
    $risultato = db_query_mod($tavola, $_GET['p_id']);
    $cur_rec = mysql_fetch_assoc($risultato);
}
if (isset($_POST['Insert']) || isset($_POST['Update'])) {
    $cur_rec['ID']           = $_POST['ID'];
    $cur_rec['IDHOTEL']      = $_POST['IDHOTEL'];
    $cur_rec['CHILDAGE']     = $_POST['CHILDAGE'];
    $cur_rec['FAMILYPLAN']   = $_POST['FAMILYPLAN'];
    $cur_rec['DATABEGIN']    = $_POST['DATABEGIN'];
    $cur_rec['DATAEND']      = $_POST['DATAEND'];
    $cur_rec['BOOKINGBEGIN'] = $_POST['BOOKINGBEGIN'];
    $cur_rec['BOOKINGEND']   = $_POST['BOOKINGEND'];
    //die(print_r($_POST['DAYAPPL']));
    $_POST['DAYAPPL']        = implode(';', $_POST['DAYAPPL']);
    $cur_rec['DAYAPPL']      = $_POST['DAYAPPL'];
    $cur_rec['DESCRIZIONE']  = $_POST['DESCRIZIONE'];
    $cur_rec['CANCPOLICY']   = $_POST['CANCPOLICY'];
    $cur_rec['PREZZO_1']     = $_POST['PREZZO_1'];
    $cur_rec['PREZZO_2']     = $_POST['PREZZO_2'];
    $cur_rec['PREZZO_3']     = $_POST['PREZZO_3'];
    $cur_rec['PREZZO_4']     = $_POST['PREZZO_4'];
    if (isset($_POST['Insert']) && db_dup_key($tavola, $_POST) > 0) {
         $c_err->add("Tariffa Servizio Hotel Gi&agrave; Codificato", "ID");
    }
    if (isset($_POST['IDHOTEL']) && ($_POST['IDHOTEL'] == null || $_POST['IDHOTEL'] == " ")) {
        $c_err->add("Campo Codice IDHOTEL Obbligatorio", "IDHOTEL");
    }
    if (isset($_POST['DATABEGIN']) && ($_POST['DATABEGIN'] == null || $_POST['DATABEGIN'] == " ")) {
        $c_err->add("Campo Codice DATABEGIN Obbligatorio", "DATABEGIN");
    }
    if (isset($_POST['DATAEND']) && ($_POST['DATAEND'] == null || $_POST['DATAEND'] == " ")) {
        $c_err->add("Campo Codice DATAEND Obbligatorio", "DATAEND");
    }
    if (isset($_POST['PREZZO_1']) && ($_POST['PREZZO_1'] == null || $_POST['PREZZO_1'] == " ")) {
        $c_err->add("Campo Codice PREZZO_1 Obbligatorio", "PREZZO_1");
    }
    if (isset($_POST['PREZZO_2']) && ($_POST['PREZZO_2'] == null || $_POST['PREZZO_2'] == " ")) {
        $c_err->add("Campo Codice PREZZO_2 Obbligatorio", "PREZZO_2");
    }
    if (isset($_POST['PREZZO_3']) && ($_POST['PREZZO_3'] == null || $_POST['PREZZO_3'] == " ")) {
        $c_err->add("Campo Codice PREZZO_3 Obbligatorio", "PREZZO_3");
    }
    if (isset($_POST['PREZZO_4']) && ($_POST['PREZZO_4'] == null || $_POST['PREZZO_4'] == " ")) {
        $c_err->add("Campo Codice PREZZO_4 Obbligatorio", "PREZZO_4");
    }

    if (!$c_err->is_errore()) {
        if (isset($_POST['Insert'])) {
            db_insert($tavola, $_POST);
        } else {
            db_update($tavola, $_POST['ID'], $_POST);
        }
        header('Location: vis_servizi_hotel_tariffe.php');
        exit;
    }
}
if (isset($_POST['Return'])) {
    header('Location: '.$indietro);
    exit;
}

require '../Librerie/ges_html_top.php';
$c_err->mostra();
?>

<form id="formG" class="form-horizontal form-label-left"  action="" method="post" novalidate>
<input type="hidden" name="ID" value="<?php if (isset($cur_rec)) echo $cur_rec['ID']; ?>" >

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Hotel</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" id="IDHOTEL" name="IDHOTEL" required="true" maxlength="11" value="<?php if (isset($cur_rec)) echo $cur_rec['CHILDAGE'];?>" class="form-control col-md-7 col-xs-12"/>
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Età bambini</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="number" minlength="1" maxlength="2" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("CHILDAGE");?> name="CHILDAGE"  id="CHILDAGE" value="<?php if (isset($cur_rec)) echo $cur_rec['CHILDAGE'];?>" size="55"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Family Plan</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("MEALPLAN");?> name="MEALPLAN"  id="MEALPLAN" value="<?php if (isset($cur_rec)) echo $cur_rec['MEALPLAN'];?>" size="55" maxlength="255"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Data Inizio</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="date" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("DATABEGIN");?> name="DATABEGIN"  id="DATABEGIN" required="true" value="<?php if (isset($cur_rec)) echo $cur_rec['DATABEGIN'];?>" size="55" maxlength="50"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Data Fine</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="date" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("DATAEND");?> name="DATAEND"  id="DATAEND" required="true" value="<?php if (isset($cur_rec)) echo $cur_rec['DATAEND'];?>" size="55" maxlength="50"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Data Inizio Booking</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="date" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("BOOKINGBEGIN");?> name="BOOKINGBEGIN"  id="BOOKINGBEGIN" required="true" value="<?php if (isset($cur_rec)) echo $cur_rec['BOOKINGBEGIN'];?>" size="55" maxlength="50"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Data Fine Booking</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="date" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("BOOKINGEND");?> name="BOOKINGEND"  id="BOOKINGEND" required="true" value="<?php if (isset($cur_rec)) echo $cur_rec['BOOKINGEND'];?>" size="55" maxlength="50"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Giorni Tariffa<i class="fa fa-question" data-toggle="tooltip" data-placement="bottom" title="Giorni della settimana in cui si applica la tariffa"></i></label>
  <div class="col-md-6 col-sm-6 col-xs-12">
      <div class="checkbox">
        <?php
        $giorni = array(
            '0' => 'Dom',
            '1' => 'Lun',
            '2' => 'Mar',
            '3' => 'Mer',
            '4' => 'Gio',
            '5' => 'Ven',
            '6' => 'Sab'
          );

        $selected = array();
        if (isset($cur_rec['DAYAPPL'])) {
            $selected = (explode(';', $cur_rec['DAYAPPL']));
            //die(print_r($selected));
        }
        foreach ($giorni as $key => $value) {
            $checked = (in_array($key, $selected)) ? 'checked="checked"' : '' ;
            echo '
                <label>
                  <div class="icheckbox_flat-green '.$checked.'">
                    <input type="checkbox" name="DAYAPPL[]" class="flat" value="'.$key.'" '.$checked.'>
                    <ins class="iCheck-helper"></ins>
                  </div>
                  '.$value.'
                </label>
            ';
        }
        ?>
      </div>
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Descrizione</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <textarea class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("DESCRIZIONE");?> name="DESCRIZIONE"  id="DESCRIZIONE" maxlength="200"><?php if (isset($cur_rec)) echo $cur_rec['DESCRIZIONE'];?></textarea>
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Policy Cancellazione<i class="fa fa-question" data-toggle="tooltip" data-placement="bottom" title="Policy di cancellazione"></i></label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("CANCPOLICY");?> name="CANCPOLICY"  id="CANCPOLICY" value="<?php if (isset($cur_rec)) echo $cur_rec['CANCPOLICY'];?>" size="55" maxlength="200"><br />
  </div>
</div>
            
<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Prezzo 1</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" maxlength="10"  required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("PREZZO_1");?> name="PREZZO_1"  id="PREZZO_1" value="<?php if (isset($cur_rec)) echo db_visimporti($cur_rec['PREZZO_1']);?>"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Prezzo 2</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" maxlength="10"  required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("PREZZO_2");?> name="PREZZO_2"  id="PREZZO_2" value="<?php if (isset($cur_rec)) echo db_visimporti($cur_rec['PREZZO_2']);?>"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Prezzo 3</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" maxlength="10"  required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("PREZZO_3");?> name="PREZZO_3"  id="PREZZO_3" value="<?php if (isset($cur_rec)) echo db_visimporti($cur_rec['PREZZO_3']);?>"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Prezzo 4</label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" maxlength="10"  required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("PREZZO_4");?> name="PREZZO_4"  id="PREZZO_4" value="<?php if (isset($cur_rec)) echo db_visimporti($cur_rec['PREZZO_4']);?>"><br />
  </div>
</div>
                        
<div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-md-offset-3">
      <button class="cancel btn btn-primary" type="submit" name="Return" value="Return">Indietro</button>
      <button type="submit"  class="btn btn-success" name="<?php echo $funzione ?>" value="Salva">Salva</button>
  </div>
</div>
</form>
<?php require '../Librerie/ges_html_bot.php'; ?>

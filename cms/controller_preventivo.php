<?php
header('Content-Type: application/json');
 
require '../Librerie/connect.php';
require 'utilities_preventivo.php';

if (isset($_POST['action']) && $_POST['action'] === 'search') {
    $data = $_POST;
    $sql = set_query($data['info']);

    if ($data['current_begin_date'] === $data['current_end_date']) {
        $day = date('w', strtotime($data['current_date']));
        $sql .= " c.CODICE = '{$data['citta']}' AND
        (t.DATABEGIN <= '{$data['info']['begin_date']}' OR t.DATABEGIN = '0000-00-00')
        AND (t.DATAEND >= '{$data['info']['end_date']}' OR t.DATAEND = '0000-00-00')
        AND (t.BOOKINGBEGIN <= '{$data['info']['begin_date']}' OR t.BOOKINGBEGIN = '0000-00-00')
        AND (t.BOOKINGEND <= '{$data['info']['end_date']}' OR t.BOOKINGEND = '0000-00-00')
        AND (DAYAPPL like '%$day%' OR DAYAPPL = '')
        AND (PREZZO_1 <> 0 AND PREZZO_2 <> 0 AND PREZZO_3 <> 0 AND PREZZO_4 <> 0)
        LIMIT 100";
    } else {
        // serve fare un like combinato nellaquery per i vari giorni di DAYAPPL
        $begin_day = new DateTime($data['current_begin_date']);
        $end_day = new DateTime($data['current_end_date']);
        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($begin_day, $interval, $end_day);
        $days_sql_string = "(";
        foreach ($daterange as $date) {
            $days_sql_string .= " DAYAPPL like '%{$date->format('w')}%' AND";
        }
        $days_sql_string = substr($days_sql_string, 0, -3);
        $days_sql_string .= ")";
        $sql .= " c.CODICE = '{$data['citta']}' AND
        (t.DATABEGIN <= '{$data['info']['begin_date']}' OR t.DATABEGIN = '0000-00-00')
        AND (t.DATAEND >= '{$data['info']['end_date']}' OR t.DATAEND = '0000-00-00')
        AND (t.BOOKINGBEGIN <= '{$data['info']['begin_date']}' OR t.BOOKINGBEGIN = '0000-00-00')
        AND (t.BOOKINGEND <= '{$data['info']['end_date']}' OR t.BOOKINGEND = '0000-00-00')
        AND ($days_sql_string OR DAYAPPL = '')
        AND (PREZZO_1 <> 0 AND PREZZO_2 <> 0 AND PREZZO_3 <> 0 AND PREZZO_4 <> 0)
        LIMIT 100";
    }

    $handle = fopen('log.txt', 'a');
    fwrite($handle, $sql);
    fclose($handle);
    $query = mysql_query($sql);
    $results = array( 'results' => array() );
    if (mysql_num_rows($query)) {
        while ($result = mysql_fetch_assoc($query)) {
            $results['results'][] = $result;
        }
        echo json_encode($results);
    } else {
        echo json_encode(0);
    }
}

if (isset($_POST['action']) && ($_POST['action'] == 'next' || $_POST['action'] == 'salva')) {
    $data = $_POST['info'];

    $sql_servizio = "SELECT IDHOTEL, DESCRIZIONE, CHILDAGE,
    PREZZO_1, PREZZO_2, PREZZO_3, PREZZO_4 FROM servizi_hotel_tariffe
    WHERE ID = $_POST[id_tariffa]";
    $sql_ordine_camere = "SELECT CHILDAGE FROM ordine_camere
    WHERE IDORDINE = $_POST[id_ordine]";

    $res = mysql_fetch_assoc(mysql_query($sql_servizio));
    $res_ordine_camere = mysql_fetch_assoc(mysql_query($sql_ordine_camere));
    $eta_bambini = explode(';', $res_ordine_camere['CHILDAGE']);

    $id_servizio = $res['IDHOTEL'];
    $descrizione = $res['DESCRIZIONE'];
    $prezzo_1 = $res['PREZZO_1'];
    $prezzo_2 = $res['PREZZO_2'];
    $prezzo_3 = $res['PREZZO_3'];
    $prezzo_4 = $res['PREZZO_4'];
    $childage = $res['CHILDAGE'];
    $numero_pax = 0;
    $prezzo = 0;

    $current_begin_date = new DateTime($_POST['current_begin_date']);
    $current_end_date = new DateTime($_POST['current_end_date']);
    $days_number = $current_end_date->diff($current_begin_date)->format('%a');
   // $date = new DateTime($_POST['current_date']);
   // $current_date = $date->format('Y-m-d');
    $next_date = $current_end_date->format('Y-m-d');
    $notes = '';
    $rooms_names = array(
        1 => 'Singola',
        2 => 'Doppia',
        3 => 'Tripla',
        4 => 'Quadrupla'
    );

    //Cicla sul numero di giorni e sul numero di stanze
    for ($i=0; $i < $days_number; $i++) { 
        foreach ($data['rooms'] as $room) {
            $room_price = ${'prezzo_'.$room['type']};
            $prezzo += $room_price;
        }
    }
    // Creo il dettaglio note da inserire nel riepilogo
    foreach ($data['rooms'] as $room) {
        $room_price = ${'prezzo_'.$room['type']};
        $notes .= "Camera {$rooms_names[$room['type']]}, Prezzo $room_price \n";
    }
    
    $sql = "INSERT INTO ordine_dett VALUES
    (null, $_POST[id_ordine], 1, $id_servizio, $_POST[id_tariffa],
    '$descrizione', '{$current_begin_date->format('Y-m-d')}', '{$current_end_date->format('Y-m-d')}', $prezzo, '', $numero_pax, '$notes')";
    mysql_query($sql);
    if (mysql_error()) {
        die('Errore nell\'inserimento dettaglio ordini: '.mysql_error());
    }
    if ($next_date == $data['end_date'] || $_POST['action'] == 'salva') {
        echo json_encode(array( 'redirect' => true ));
    } else {
        echo json_encode($next_date);
    }
}

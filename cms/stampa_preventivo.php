<?php
$m="ordini";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';
require 'utilities_preventivo.php';

$titolo    = "Gestione Preventivi";
$tavola    = "ordine";
$id = (isset($_GET['id']) && !db_is_null($_GET['id'])) ? $_GET['id'] : null;
if ($id) {
    $preventivo = crea_array_preventivo($id);
} else {
    header('Location: vis_ordini.php');
}
require '../Librerie/ges_html_top.php';
?>

<div class="page-title">
    <div class="title_left">
        <h3>Preventivo <small>Dettaglio preventivo</small></h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="clearfix"></div>

<div class="row">
    <div class="col-md-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo $preventivo['tipo']; ?>
                    <small>Dettaglio    <?php echo $preventivo['tipo']; ?></small>
                </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">

                <section class="content invoice">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-xs-12 invoice-header">
                            <h1>
                                Preventivo
                                <small><?php echo $preventivo['id']; ?></small>
                                <small class="pull-right">Data: <?php echo date('d-m-Y'); ?></small>
                            </h1>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">

                        <div class="col-sm-4">
                            <h3>Descrizione</h3>
                            <p><?php echo $preventivo['descrizione']; ?></p>
                            <h3>Anagrafica</h3>
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th style="width:50%">Nome:</th>
                                                <td><?php echo $preventivo['nome_int']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Email</th>
                                                <td><?php echo $preventivo['mail_int']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Telefono</th>
                                                <td><?php echo $preventivo['tel_int']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Note</th>
                                                <td><?php echo $preventivo['note']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Data check in</th>
                                                <td><?php echo $preventivo['datain']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Data check out</th>
                                                <td><?php echo $preventivo['dataout']; ?></td>
                                            </tr>
                                            <tr>
                                                <th>Prezzo totale</th>
                                                <td>
                                                    <strong><?php echo db_visimporti($preventivo['prezzo']['prezzo_totale']); ?>€</strong>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <h3>Dettaglio</h3>
                    <div class="row">
                        <div class="col-xs-12 table">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Descrizione</th>
                                        <th>Prezzo</th>
                                        <th>Data inizio</th>
                                        <th>Data fine</th>
                                        <th>Città</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($preventivo['dettaglio'] as $riga): ?>
                                        <tr>
                                            <td><?php echo $riga['descrizione']; ?></td>
                                            <td><?php echo db_visimporti($riga['prezzo']); ?>€</td>
                                            <td><?php echo $riga['begin']; ?></td>
                                            <td><?php echo $riga['end']; ?></td>
                                            <td>
                                                <?php echo $riga['citta']; ?>
                                            </td>
                                        </tr>
                                        <?php if (isset($riga['atlante']) && !db_is_null($riga['atlante'])): ?>
                                            <tr>
                                                <td><?php echo $riga['atlante']; ?></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        <?php endif; ?>
                                        <?php if (isset($riga['servizi_liberi']) && !db_is_null($riga['servizi_liberi'])): ?>
                                        <?php foreach ($riga['servizi_liberi'] as $servizio_libero): ?>
                                            <tr>
                                                <td><?php echo $servizio_libero['descrizione']; ?></td>
                                                <td><?php echo db_visimporti($servizio_libero['prezzo']*$servizio_libero['quantita']); ?>€</td>
                                                <td><?php echo $servizio_libero['begin']; ?></td>
                                                <td><?php echo $servizio_libero['end']; ?></td>
                                                <td></td>
                                            </tr>
                                        <?php endforeach ?>
                                        <?php endif ?>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->
                </section>
            </div>
        </div>
    </div>
</div>

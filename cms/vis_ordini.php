<?php
$m="ordini";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';

$titolo    = "Gestione Preventivi";
$tavola    = "ordine";
$risultato = db_query_vis($tavola, 'ID desc');


require '../Librerie/ges_html_top.php';
?>

<div class="col-md-12 col-sm-12 col-xs-12">
  <div class="x_panel">
    <div class="x_title">
      <h2>Gestione Preventivi </h2>
      <ul class="nav navbar-right panel_toolbox">
        <button class="btn btn-round btn-primary" type="button" onclick="location.href='ges_ordini.php?p_upd=0'">Nuovo Preventivo</button>
      </ul>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        <thead>
          <tr>
          <th width="8%"> Id </th>
          <th  width="5%"> &nbsp;</th>
          <th  width="5%"> &nbsp;</th>
          <th  width="5%"> &nbsp;</th>
          <th  width="5%"> &nbsp;</th>
          <th width="20%"> Nome Intestatario </th>
          <th width="20%"> Descrizione </th> 
          <th width="20%"> Data Inizio </th>
          <th width="20%"> Data Fine </th>                   
        </thead>  
        <tbody> 
        <?php
        while ($cur_rec = mysql_fetch_assoc($risultato)) {
            echo " 
            <tr >
              <td >".$cur_rec['ID']." </td>
              <td ><a href=\"ges_ordini.php?p_upd=1&p_id=".$cur_rec['ID']."\"><i class=\"fa fa-edit\"></i></a></td>
              <td ><a href=\"vis_dettaglio_ordini.php?p_upd=1&p_id=".$cur_rec['ID']."\"><i class=\"fa fa-book\"></i></a></td>
              <td ><a href=\"stampa_preventivo.php?id=".$cur_rec['ID']."\"><i class=\"fa fa-print\"></i></a></td>
              <td ><a href=\"Javascript:ut_delete_rec(".$cur_rec['ID'].",'del_ordini')\"><i class=\"fa fa-trash\"></i></a>
              <td >".$cur_rec['NOME_INT']." </td>
              <td >".$cur_rec['DESCRIZIONE']." </td>
              <td >".$cur_rec['DATAIN']." </td>
              <td >".$cur_rec['DATAOUT']." </td>

            </tr> ";
        }
        ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('#datatable-responsive').DataTable({
      "bFilter":true,
      "iDisplayLength": 50,
      "aaSorting": [[ 0, "desc" ]], 
      "bStateSave":true,                 
      "aoColumns": [
        { "bSortable": false },
        { "bSortable": false },
        { "bSortable": false },
        { "bSortable": false },
        { "bSortable": true },
        { "bSortable": true },
        { "bSortable": true },
        { "bSortable": false },
        { "bSortable": false }
      ]         
    });
  });
</script>  

<?php require '../Librerie/ges_html_bot.php'; ?>

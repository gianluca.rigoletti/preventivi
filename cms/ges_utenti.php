<?php
$m="utenti";
require '../Librerie/connect.php';
$tavola= "amministratori";
$Errori= array();

if ($_GET['p_upd']==1) {
    $Funzione = "Update";
    $Disabilita_chiave = "disabled";
    $Titolo = "Modifica UTENTE";
} else {
    $Funzione = "Insert";
    $Disabilita_chiave = "";
    $Titolo = "Nuovo UTENTE";
}

$readonly = "";
if (!$_SESSION['amministratore'])  $readonly = "disabled";

// se richiamato in update allora devo popolare il form
if ($_GET['p_upd']==1) {
    $risultato = db_query_mod($tavola, $_GET['p_id']);
    $cur_rec = mysql_fetch_assoc($risultato);
}

// confermo
if (isset($_POST['Insert']) || isset($_POST['Update'])) {
    $cur_rec['ID'] = $_POST['ID'];
    if (isset($_POST['UTENTE'])) $cur_rec['UTENTE']   = $_POST['UTENTE'];
    if (isset($_POST['NOME'])) $cur_rec['NOME']       = $_POST['NOME'];
    if (isset($_POST['MAIL'])) $cur_rec['MAIL']       = $_POST['MAIL'];
    if (isset($_POST['LIVELLO'])) $cur_rec['LIVELLO'] = $_POST['LIVELLO'];

    $cur_rec['NOTIFICAOPE']        = $_POST['NOTIFICAOPE'];
    $cur_rec['NOTIFICASCA']        = $_POST['NOTIFICASCA'];
    $cur_rec['REMINDERSCA']        = $_POST['REMINDERSCA'];
    $cur_rec['DEFAULTREMINDERSCA'] = $_POST['DEFAULTREMINDERSCA'];

    if (isset($_POST['UTENTE']) && ($_POST['UTENTE'] == null || $_POST['UTENTE'] == " ")) {
        $c_err->add("Campo Codice Utente Obbligatorio", "UTENTE");
    }

    if (isset($_POST['NOME']) && ($_POST['NOME'] == null || $_POST['NOME'] == " ")) {
        $c_err->add("Campo NOME Obbligatorio", "NOME");
    }
       
   if ( $Funzione == "Update" ) {
     if ( ($_POST['PASSWORD'] == null || $_POST['PASSWORD'] == " ") &&
          ($_POST['PASSWORD1'] != null && $_POST['PASSWORD1'] != " ")) {
         $c_err->add("Campo Password Obbligatorio","PASSWORD");
     } 
     if ( ($_POST['PASSWORD'] != null && $_POST['PASSWORD'] != " ") &&
          ($_POST['PASSWORD1'] == null || $_POST['PASSWORD1'] == " ")) {
         $c_err->add("Campo Conferma Password Obbligatorio","PASSWORD1");
     }   
     
     /*if ( $_POST['PASSWORD'] == null || $_POST['PASSWORD'] == " ") {
           $_POST['PASSWORD'] = $cur_rec['PASSWORD'];
           $_POST['PASSWORD1'] = $cur_rec['PASSWORD'];
     } */   
   } else { 
     if ( $_POST['PASSWORD'] == null || $_POST['PASSWORD'] == " ") {
         $c_err->add("Campo Password Obbligatorio","PASSWORD");
     }   
     if ( $_POST['PASSWORD1'] == null || $_POST['PASSWORD1'] == " ") {
        $c_err->add("Campo Conferma Password Obbligatorio","PASSWORD1");
     }   
   }  

     if ( //( $_POST['PASSWORD'] == null || $_POST['PASSWORD'] == " ") && 
          $_POST['PASSWORD1'] != $_POST['PASSWORD'])  {
          $c_err->add("Campo Conferma Password diverso da Password","PASSWORD");
     } 
      
   // controllo dup-Val
   if ( isset($_POST['Insert']) && db_dup_key($tavola,$_POST) > 0 )  {
        $c_err->add("Utente Gi&agrave; Codificato","User");
   } 

   if ( !$c_err->is_errore() ) {
   
       if ($_POST['PASSWORD'] != null && $_POST['PASSWORD'] != " ") $_POST['Pword'] = $_POST['PASSWORD'];
   
       if ( isset($_POST['Insert'])) {
	    db_insert($tavola,$_POST);  
       }  else {
	    db_update($tavola,$_POST['ID'],$_POST);
       }
       header('Location: vis_utenti.php');
       exit; 
   }
}

// torno indietro
$indietro = "vis_utenti.php";
if (isset($_POST['Return'])) {
   header("Location: vis_utenti.php");
   exit;
}
        
require '../Librerie/ges_html_top.php';

?>
        

       <form id="formG" class="form-horizontal form-label-left"  action="" method="post" novalidate>

            
            <input type="hidden" name="ID" value="<?php if (isset($cur_rec)) echo $cur_rec['ID']; ?>" >
            


            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Utente <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("UTENTE");?> name="UTENTE"  id="UTENTE" value="<?php if (isset($cur_rec)) echo $cur_rec['UTENTE'];?>" size="55"><br />
            </div>
            </div>
            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nome <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("NOME");?> name="NOME"  id="NOME" value="<?php if (isset($cur_rec)) echo $cur_rec['NOME'];?>" size="55"><br />
            </div>
            </div>
            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Password 
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="password" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("PASSWORD");?> name="PASSWORD"  id="PASSWORD" value="<?php if (isset($cur_rec)) echo $cur_rec['PASSWORD'];?>" size="55"><br />
            </div>
            </div>   
            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Conferma Password <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="password" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("PASSWORD1");?> name="PASSWORD1"  id="PASSWORD1" value="<?php if (isset($cur_rec)) echo $cur_rec['PASSWORD'];?>" size="55"><br />
            </div>
            </div>                       
            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Mail <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                  <input type="text" required="true" email="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("MAIL");?> name="MAIL"  id="MAIL" value="<?php if (isset($cur_rec)) echo $cur_rec['MAIL'];?>" size="55" maxlength="255"><br />
            </div>
            </div>
            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Notifica Operazioni <i class="fa fa-question" data-toggle="tooltip" data-placement="bottom" title="Ricevi una mail quando qualcuno ti assegna un operazione"></i>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
               <select id="NOTIFICAOPE" required="true" name="NOTIFICAOPE" class="form-control col-md-7 col-xs-12"/>
               <?php  
                      $valore = 0;
                      if (isset($cur_rec['NOTIFICAOPE'])) $valore = $cur_rec['NOTIFICAOPE'];
                        $valori = array('0' => 'No',
                        '1' => 'Si');
                        
                        foreach ($valori as $key => $value) {
                              $selected = "";
                              if ($key == $valore ) $selected = "selected=\"selected\"";
                              echo "<option value=\"$key\" $selected>$value</option>";
                        } 
               ?>
               </select>
            </div>
            </div>   
            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Notifica Scandenze <i class="fa fa-question" data-toggle="tooltip" data-placement="bottom" title="Ricevi una mail quando qualcuno ti assegna una scandeza"></i>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
               <select id="NOTIFICASCA" required="true" name="NOTIFICASCA" class="form-control col-md-7 col-xs-12"/>
               <?php  
                      $valore = 0;
                      if (isset($cur_rec['NOTIFICASCA'])) $valore = $cur_rec['NOTIFICASCA'];
                        $valori = array('0' => 'No',
                        '1' => 'Si');
                        
                        foreach ($valori as $key => $value) {
                              $selected = "";
                              if ($key == $valore ) $selected = "selected=\"selected\"";
                              echo "<option value=\"$key\" $selected>$value</option>";
                        } 
               ?>
               </select>
            </div>
            </div>  
            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Promemoria Scadenze <i class="fa fa-question" data-toggle="tooltip" data-placement="bottom" title="Ricevi una mail di promemoria quando si avvicina una scadenza"></i>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
               <select id="REMINDERSCA" required="true" name="REMINDERSCA" class="form-control col-md-7 col-xs-12"/>
               <?php  
                      $valore = 0;
                      if (isset($cur_rec['REMINDERSCA'])) $valore = $cur_rec['REMINDERSCA'];
                        $valori = array('0' => 'No',
                        '1' => 'Si');
                        
                        foreach ($valori as $key => $value) {
                              $selected = "";
                              if ($key == $valore ) $selected = "selected=\"selected\"";
                              echo "<option value=\"$key\" $selected>$value</option>";
                        } 
               ?>
               </select>
            </div>
            </div>    
            
            <div class="item form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Giorni Anticipo Promemoria <i class="fa fa-question" data-toggle="tooltip" data-placement="bottom" title="Numero di giorni di anticipo per Promemoria scadenze"></i>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
               <select id="DEFAULTREMINDERSCA" required="true" name="DEFAULTREMINDERSCA" class="form-control col-md-7 col-xs-12"/>
               <?php  
                      $valore = 0;
                      if (isset($cur_rec['DEFAULTREMINDERSCA'])) $valore = $cur_rec['DEFAULTREMINDERSCA'];

                        
                        for ($i=1;$i<21;$i++) {
                              $selected = "";
                              if ($i == $valore ) $selected = "selected=\"selected\"";
                              echo "<option value=\"$i\" $selected>$i</option>";
                        } 
               ?>
               </select>
            </div>
            </div>                                                                     
                 

            <?php if ($_SESSION['amministratore'])    {
                $select0 = "";
                $select1 = "";
                $select2 = "";
                if (isset($cur_rec) && $cur_rec['LIVELLO']==1) $select1=" selected ";
                if (isset($cur_rec) && $cur_rec['LIVELLO']==0) $select0=" selected ";
                if (isset($cur_rec) && $cur_rec['LIVELLO']==2) $select2=" selected ";
                echo "            <div class=\"item form-group\">
              <label class=\"control-label col-md-3 col-sm-3 col-xs-12\" for=\"\">Livello<span class=\"required\">*</span>
              </label>    <div class=\"col-md-6 col-sm-6 col-xs-12\">
                      <select class=\"form-control col-md-7 col-xs-12\" required=\"true\" name=\"LIVELLO\" id=\"LIVELLO\">
                         <option value=\"\"></option>
                         <option value=\"0\" $select0>Utente</option>
                         <option value=\"1\" $select1 >Amministratore</option>";
                  if ($_SESSION['superuser']) echo " <option value=\"2\" $select2 >Superuser</option>";       
                echo  "     </select>
            </div>
            </div>";                       
            
            
              } ?>

          
            <div class="ln_solid"></div>
            <div class="form-group">
              <div class="col-md-6 col-md-offset-3">
               <button class="cancel btn btn-primary" type="submit" name="Return" value="Return">Indietro</button>
               <button type="submit"  class="btn btn-success" name="<?php echo $Funzione ?>" value="Salva">Salva</button>
             </div>
        </div>
        </form>

        
        
<?php require '../Librerie/ges_html_bot.php';


?>


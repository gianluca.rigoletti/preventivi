<?php

require '../Librerie/connect.php';

$Titolo = "Annnullamento ordine preventivo";
$Tavola= "ordine";

$indietro = "vis_ordini.php";

if (isset($_GET['id'])) {
    db_delete($Tavola, $_GET['id']);
    header("Location: $indietro");
    exit;
}
header("Location: $indietro");
exit;

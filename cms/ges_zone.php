<?php
$m = 'zone';

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';

$c_files = new files();
$tavola = 'zone';
$indietro = 'vis_zone.php';

if (isset($_GET['back'])) {
    $indietro = $_GET['back'].'.php';
}
if ($_GET['p_upd'] == 1) {
    $funzione = 'Update';
    $disabilita_chiave = 'disabled';
    $titolo = 'Modifica Zona';
} else {
    $funzione = 'Insert';
    $titolo = 'Nuova Zona';
}
if ($_GET['p_upd'] == 1) {
    $risultato = db_query_mod($tavola, $_GET['p_id']);
    $cur_rec = mysql_fetch_assoc($risultato);
}
if (isset($_POST['Insert']) || isset($_POST['Update'])) {
    $cur_rec['ID'] = $_POST['ID'];
    if (isset($_POST['CODICE'])) {
        $cur_rec['CODICE'] = $_POST['CODICE'];
    }
    if (isset($_POST['DESCRIZIONE'])) {
        $cur_rec['DESCRIZIONE'] = $_POST['DESCRIZIONE'];
    }
    if (isset($_POST['Insert']) && db_dup_key($tavola, $_POST) > 0) {
         $c_err->add("Zona Gi&agrave; Codificato", "ID");
    }
    if (!$c_err->is_errore()) {
        if (isset($_POST['Insert'])) {
            db_insert($tavola, $_POST);
        } else {
            db_update($tavola, $_POST['ID'], $_POST);
        }
        header('Location: vis_zone.php');
        exit;
    }
}
if (isset($_POST['Return'])) {
    header('Location: '.$indietro);
    exit;
}

require '../Librerie/ges_html_top.php';
$c_err->mostra();
?>

<form id="formG" class="form-horizontal form-label-left"  action="" method="post" novalidate>
<input type="hidden" name="ID" value="<?php if (isset($cur_rec)) {
    echo $cur_rec['ID'];
} ?>" >

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Codice <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("CODICE");?> name="CODICE"  id="CODICE" value="<?php if (isset($cur_rec)) echo $cur_rec['CODICE'];?>" size="55"><br />
  </div>
</div>
            
<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Descrizione <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <textarea required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("DESCRIZIONE");?> name="DESCRIZIONE"  id="DESCRIZIONE" maxlength="200"><?php if (isset($cur_rec)) echo $cur_rec['DESCRIZIONE'];?></textarea>
  </div>
</div>
                        
<div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-md-offset-3">
      <button class="cancel btn btn-primary" type="submit" name="Return" value="Return">Indietro</button>
      <button type="submit"  class="btn btn-success" name="<?php echo $funzione ?>" value="Salva">Salva</button>
  </div>
</div>
</form>
<?php require '../Librerie/ges_html_bot.php'; ?>

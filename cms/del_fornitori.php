<?php

require '../Librerie/connect.php';

$Titolo = "Annullamento Fornitore";
$Tavola= "fornitori";

$indietro = "vis_fornitori.php";

if (isset($_GET['id'])) {
    db_delete($Tavola, $_GET['id']);
    header("Location: $indietro");
    exit;
}
header("Location: $indietro");
exit;

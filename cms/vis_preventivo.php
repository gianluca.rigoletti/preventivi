<?php
$m="preventivi";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/configurazione.php';
require '../Librerie/ges_html_top.php';

?>

<div class="col-xs-12 preventivo">
    <div class="x_panel">
        <div class="x_title">
            <h1> Nuovo preventivo </h1>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <i class="fa fa-wrench"></i>
                    </a>
                </li>
                <li>
                    <a class="close-link"><i class="fa fa-close"></i></a>
                </li>
            </ul>
        </div>
        <div class="x_content">
            <br>
            <form id="form-preventivo" method="POST" action="controller_preventivo.php" data-parsley-validate class="form-horizontal form-label-left" novalidate>
                <div class="control-group">
                    <div class="controls">
                        <div class="input-prepend input-group">
                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                            <input type="text" style="width: 600px" name="reservation" id="reservation" class="form-control" value="03/18/2013 - 03/23/2013" />
                        </div>
                    </div>
                </div>
                <div class="form-group col-xs-6 col-md-3">
                    <label for="numero-camere">Numero camere</label>
                    <select id="numero-camere" name="numero-camere" class="form-control" required="">
                        <option value="0">Seleziona..</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                    </select>
                </div>
                <div class="form-group col-xs-12 col-md-9"></div>
                <div class="form-group tipo-camere col-xs-12"></div>
                <div class="form-group numero-bambini col-xs-12"></div>
                <div class="col-xs-12">
                    <button type="submit" class="btn btn-success"> Cerca Preventivo</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="ln_solid"></div>
<div class="form-group">
    <div class="col-md-6 col-md-offset-3">
      <button class="cancel btn btn-primary" type="submit" name="Return" value="Return">Indietro</button>
      <button type="submit"  class="btn btn-success" name="<?php echo $funzione ?>" value="Salva">Salva</button>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#reservation').daterangepicker({
            'opens': 'left'
        }, function(start, end, label) {
          console.log(start.toISOString(), end.toISOString(), label);
        });

        var numeroCamere, colLength;
        // Genera il numero di camere
        $('#numero-camere').change(function () {
            numeroCamere = $('#numero-camere').val();
            $('.camera-form-group').remove();
            $('.bambini-form-group').remove();
            for (var i = 1; i <= numeroCamere; i++) {
                colLength = 12/numeroCamere;
                var templateCamere = "\
                <div class=\"camera-form-group form-group col-xs-12 col-md-4\">\
                    <label for=\"camera-" + i + "\">\
                        Tipo camera\
                    </label>\
                    <select id=\"camera-" + i + "\" name=\"camera-" + i + "\" class=\"camera-select form-control\" required=\"\">\
                        <option value=\"1\"> singola </option>\
                        <option value=\"2\"> doppia </option>\
                        <option value=\"3\"> tripla </option>\
                        <option value=\"4\"> quadrupla </option>\
                    </select>\
                </div>\
                <div class=\"col-xs-12 col-md-4 bambini-form-group form-group\" id=\"bambini-camera-" + i + "\">\
                </div>\
                <div class=\"col-xs-12 col-md-4 bambini-form-group form-group\" id=\"eta-bambini-camera-camera-" + i + "\">\
                </div>\
                <div class=\"clearfix\"></div>\
                ";
                $('.tipo-camere').append(templateCamere);
            }
        });

        $('.preventivo').on('change', '.camera-select', function (event) {
            var idCamera = event.target.id;
            var tipoCamera = $(event.target).val();

            $('#bambini-' + idCamera).empty();

            var templateBambini = "\
                <label for=\"bambini-camera-" + i + "\">\
                    Numero bambini\
                </label>\
                <select id=\"bambini-camera-" + idCamera + "\" name=\"bambini-camera-" + idCamera + "\" class=\"bambini-select form-control\" required=\"\">";
            for (var i = 0; i <= tipoCamera - 1; i++) {         
                templateBambini += "<option value=\"" + i + "\"> " + i + " </option>";
            }
            templateBambini += "\
                </select>";

            $('#bambini-' + idCamera).append(templateBambini);
        });

        $('.preventivo').on('change', '.bambini-select', function (event) {
            var idBambini = event.target.id;
            var numeroBambini = $(event.target).val();
            $('#eta-' + idBambini).empty();
            colLength = 12 / numeroBambini;

            for (var i = 1; i <= numeroBambini ; i++) {
                var templateEta = "\
                <div class=\"col-xs-" + colLength + "\">\
                    <label for=\"" + i + "-" + idBambini + "\">\
                        Età\
                    </label>\
                    <select id=\"eta-" + i + "-" + idBambini + "\" name=\"eta-" + i + "-" + idBambini + "\" class=\"bambini form-control\" required=\"\">";
                for (var eta = 1; eta <= 18; eta++) {         
                    templateEta += "<option value=\"" + eta + "\"> " + eta + " </option>";
                }
                templateEta += "\
                    </select>\
                </div>";
                $('#eta-' + idBambini).append(templateEta);
            }
        });

        // Il form  non gestisce gli elementi dinamici,
        // devo confogurarlo manualmente
        $('#form-preventivo').on('submit', function(e) {
            //prevent the default submithandling
            e.preventDefault();
            //send the data of 'this' (the matched form) to yourURL
            $.post('controller_preventivo.php', $(this).serialize());
        });
    });
</script>
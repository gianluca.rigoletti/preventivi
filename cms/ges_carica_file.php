<?php

require '../Librerie/connect.php';

header('content-type:application/json');


$messages = array();
uploadFile();
echo json_encode($messages);

/**
 * Scrive in status.json il valore 
 * 
 * @param integer $value percentuale di completamento
 */
function setStatusOfRequest($value)
{
    if ($value === 100) {
        $value = 'done';
    }
    $request_progress = fopen('status.json', 'w');
    fwrite($request_progress, json_encode($value));
    fclose($request_progress);
}

/**
 * Aggiunge le slashes le escape dei caratteri
 * per ogni valore dell'array
 * @param  array $array l'array a cui aggiungere gli slas
 *
 * @return array 
 */
function addSlashesToArray($array)
{
    $slashed_array = array();
    foreach ($array as $key => $value) {
        $slashed_array[] = addslashes($value);
    }
    return $slashed_array;
}

/**
 * Inserisce un messaggio nell'array di messaggi.
 * L'array viene modificato epr referenza.
 * 
 * @param string $type     il tipo di messaggio: error, success, info
 * @param string $content  il contenuto del messaggio
 *
 * @return json oggetto json di risposta
 */
function setMessage($type, $content)
{
    global $messages;
    $push = array(
        'type' => $type, 
        'content' => $content
    );
    array_push($messages, $push);
}

/**
 * Prende una colonna, toglie gli 
 * spazi bianchi da ogni riga.
 *
 * @param  array $data  I dati su cui filtrare
 * @param  integer $number_of_column la colonna da filtrare
 * @param boolean $remove_duplicates se true rimuove i valori 
 * @return array  la colonna filtrata
 */
function filterColumn($data, $number_of_column, $remove_empty_values = false)
{
    if ($remove_empty_values) {
        return array_filter(array_map('trim', array_column($data, $number_of_column)));
    }
    return array_map('trim', array_column($data, $number_of_column));
}

/**
 * Inserisce i valori non duplicati delle zone 
 * e delle citta nel db
 * @param  array $column colonna di valori unici da inserire
 * @return boolean
 */
function uploadNazioniZone($column, $type)
{
    for ($i=0; $i < sizeof($column); $i++) { 

        if (abs(sizeof($column))/8 - $i < 2) setStatusOfRequest(3);
        if (abs(sizeof($column))/4 - $i < 2) setStatusOfRequest(6);
        if (abs(3*sizeof($column))/8 - $i < 2) setStatusOfRequest(9);
        if (abs(sizeof($column))/2 - $i < 2) setStatusOfRequest(12);
        if (abs(5*(sizeof($column))/8 - $i) < 2) setStatusOfRequest(15);
        if (abs(3*(sizeof($column))/4 - $i) < 2) setStatusOfRequest(18);
        if (abs(7*(sizeof($column))/8 - $i) < 2) setStatusOfRequest(21);
        if (abs(sizeof($column)) - $i < 2) setStatusOfRequest(24);
        // Controllo se il valore esiste già (es un altro file di importazione o un caricamento manuale)
        $sql = "SELECT ID FROM ".$type." WHERE CODICE = '".$column[$i]."'";
        $query = mysql_query($sql);
        if (mysql_num_rows($query) > 0) {
            $id = mysql_fetch_assoc($query)['ID'];
            $sql_update = "UPDATE ".$type." SET CODICE = '".$column[$i]."', DESCRIZIONE='".$column[$i]."'
            WHERE ID = ".$id;
            mysql_query($sql_update);
            if (mysql_error()) {
                setMessage('error', 'Errore in aggiornamento di '.$type.': '.mysql_error());
                return false;
            }
        } else {
            // Inserimento di un nuovo record
            $sql = "INSERT INTO ".$type." ( ID, CODICE, DESCRIZIONE )
            VALUES (null, '".$column[$i]."', '".$column[$i]."' )";
            mysql_query($sql);
            if (mysql_error()) {
                setMessage('error', 'Errore in inserimento di '.$type.': '.mysql_error());
                return false;
            }
        }        
    }
    return true;
}

/**
 * Carica nel db i fornitori nella relativa tabella
 * @param  array $id_fornitori valori unici degli id da caricare
 * @return boolean             success/fail
 */
function uploadFornitori($fornitori)
{
    $count = 0;
    foreach ($fornitori as $vendor_id => $vendor_name) { 

        if ((sizeof($fornitori))/8 - $count < 2) setStatusOfRequest(27);
        if ((sizeof($fornitori))/4 - $count < 2) setStatusOfRequest(30);
        if (3*(sizeof($fornitori))/8 - $count < 2) setStatusOfRequest(33);
        if ((sizeof($fornitori))/2 - $count < 2) setStatusOfRequest(36);
        if (5*(sizeof($fornitori))/8 - $count < 2) setStatusOfRequest(39);
        if (3*(sizeof($fornitori))/4 - $count < 2) setStatusOfRequest(42);
        if (7*(sizeof($fornitori))/8 - $count < 2) setStatusOfRequest(45);
        if ((sizeof($fornitori)) - $count < 2) setStatusOfRequest(48);

        // Esistono dei record con 0 => ''. Li gestisco come dei campi vuoti
        // Ricordarsi dell'escape dei caratteri visto che le sono stringhe
        $vendor_name = addslashes($vendor_name);
        // Controllo se il valore esiste già (es un altro file di importazione o un caricamento manuale)
        $sql = "SELECT ID FROM fornitori WHERE ID = ".$vendor_id." OR CODICE ='".$vendor_name."'";
        $query = mysql_query($sql);
        if (mysql_num_rows($query) > 0) {
            $sql_update = "UPDATE fornitori SET CODICE = '".$vendor_name."', 
            DESCRIZIONE='".$vendor_name."',
            IDTIPOSERVIZIO = 1
            WHERE ID = ".$vendor_id;
            mysql_query($sql_update);
            if (mysql_error()) {
                setMessage('error', 'Errore in aggiornamento di fornitori: '.mysql_error()."\n Chiave:".$vendor_id."\n Valore: ".$vendor_name);
                return false;
            }
        } else {
            // Inserimento di un nuovo record
            $sql = "INSERT INTO fornitori ( ID, CODICE, DESCRIZIONE, IDTIPOSERVIZIO )
            VALUES (".$vendor_id.", '".$vendor_name."', '".$vendor_name."', 1)";
            mysql_query($sql);
            if (mysql_error()) {
                setMessage('error', 'Errore in inserimento di fornitori: '.mysql_error()."\n Chiave:".$vendor_id."\n Valore: ".$vendor_name.
                    " Query eseguita:".$sql);
                return false;
            }
        }
        $count++;        
    }
    return true;
}

/**
 * Carica in servizi_hotel il file associato.
 *
 * @param  array &$data I dati da caricare
 * @return boolean  Se il caricamento e' avvenuto correttamente
 */
function uploadServizi(&$data)
{
    setMessage('info', 'Arrivato dentro servizi');
    // Prima cosa devo filtrare le colonne:
    $id_fornitore_column     = array_column($data, 0);
    setMessage('info', 'Id fornitore fatto');
    $vendor_id_column        = array_column($data, 0); // Sono uguali fra di loro
    $vendor_name_column      = addSlashesToArray(array_column($data, 1));
    $starrank_column         = array_column($data, 2);
    $codice_column           = array_column($data, 3);
    $nome_column             = addSlashesToArray(array_column($data, 4));
    $descrizione_column      = addSlashesToArray(array_column($data, 5));
    $nazioni_column          = array_column($data, 6);
    $citta_column            = addSlashesToArray(array_column($data, 7));
    $indirizzo_1_column      = addSlashesToArray(array_column($data, 8));
    $indirizzo_2_column      = addSlashesToArray(array_column($data, 9));
    $zip_column              = array_column($data, 10);
    $phone_column            = array_column($data, 11);
    $fax_column              = array_column($data, 12);
    $img_column              = array_column($data, 13);
    $lat_column              = array_column($data, 14);
    $lang_column             = array_column($data, 15);
    $childage_column         = array_column($data, 16);
    $roomtype_column         = addSlashesToArray(array_column($data, 17));
    $mealplan_column         = addSlashesToArray(array_column($data, 18));
    $familyplan_column       = array_column($data, 19);
    $maxocc_column           = array_column($data, 20);
    $product_type_column     = array_column($data, 21);
    $state_column            = array_column($data, 21);
    $state_column            = array_column($data, 22);
    $perm_room_code_column   = array_column($data, 23);
    $non_rimborsabile_column = array_column($data, 24);
    $fee_column              = array_column($data, 25);
    $fee_type_column         = array_column($data, 25);

    setMessage('info', 'dati da caricare num: '.sizeof($data));

    // // Carico le citta nuove che trovo nei record in caso non esistano
    // $nazioni_filtered = array_values(filterColumn($nazioni_column, 1, true)); // Array con valori unici
    // $upload_nazioni_ok = uploadNazioniZone($nazioni_filtered, 'nazioni');
    // setMessage('success', 'Nazioni caricate correttamente');

    // // Carico i fornitori che non sono già stati caricati. Devo passare un array associativo id => codice
    // $fornitori = array_unique(array_map('trim', array_combine($vendor_id_column, $vendor_name_column)));
    // $upload_fornitori_ok = uploadFornitori($fornitori);
    // setMessage('success', 'Fornitori caricati correttamente');

    // if (false) {
    //     setMessage('error', 'Problema nella gestione delle citta e dei fornitori');
    //     return false;
    // }

    setMessage('info', 'Inizio inserimento servizi');

    // Metodo alternativo di ottimizzazione
    $sql = 

    // Inserimento dei servizi
    // for ($i=0; $i < 200; $i++) {

    //     if (abs($i - sizeof($data)/13) <= 2) setStatusOfRequest(52);
    //     if (abs($i - 2*sizeof($data)/13) <= 2) setStatusOfRequest(56);
    //     if (abs($i - 3*sizeof($data)/13) <= 2) setStatusOfRequest(60);
    //     if (abs($i - 4*sizeof($data)/13) <= 2) setStatusOfRequest(64);
    //     if (abs($i - 5*sizeof($data)/13) <= 2) setStatusOfRequest(68);
    //     if (abs($i - 6*sizeof($data)/13) <= 2) setStatusOfRequest(72);
    //     if (abs($i - 7*sizeof($data)/13) <= 2) setStatusOfRequest(76);
    //     if (abs($i - 8*sizeof($data)/13) <= 2) setStatusOfRequest(80);
    //     if (abs($i - 9*sizeof($data)/13) <= 2) setStatusOfRequest(84);
    //     if (abs($i - 10*sizeof($data)/13) <= 2) setStatusOfRequest(88);
    //     if (abs($i - 11*sizeof($data)/13) <= 2) setStatusOfRequest(92);
    //     if (abs($i - 12*sizeof($data)/13) <= 2) setStatusOfRequest(96);
    //     if (abs($i - 13*sizeof($data)/13) <= 2) setStatusOfRequest(98);

    //     if ($i === 200) setMessage('info', 'Primi 200 dati elaborati');
    //     if ($i === 400) setMessage('info', 'Primi 400 dati elaborati');
    //     if ($i === 600) setMessage('info', 'Primi 600 dati elaborati');
    //     if ($i === 800) setMessage('info', 'Primi 800 dati elaborati');
    //     if ($i === 1000) setMessage('info', 'Primi 1000 dati elaborati');
    //     if ($i === 1200) setMessage('info', 'Primi 1200 dati elaborati');
    //     if ($i === 1400) setMessage('info', 'Primi 1400 dati elaborati');
    //     if ($i === 1600) setMessage('info', 'Primi 1600 dati elaborati');
    //     if ($i === 1800) setMessage('info', 'Primi 1800 dati elaborati');

    //     // Inserire id nazione e id citta
    //     // Se IDNAZIONE è nulla la metto uguale a 0 per ora
    //     $sql_nazioni = " SELECT ID from nazioni WHERE CODICE = '".$nazioni_column[$i]."'";
    //     $id_nazione = mysql_fetch_assoc(mysql_query($sql_nazioni))['ID'];

    //     $sql_citta = " SELECT ID from citta WHERE CODICE = '".$citta_column[$i]."'";
    //     $id_citta = mysql_fetch_assoc(mysql_query($sql_citta))['ID'];

    //     $id_nazione = (empty($id_nazione)) ? '0' : $id_nazione;
    //     $id_citta = (empty($id_citta)) ? '0' : $id_citta;

    //     // Controllo se esistono duplicati
    //     $sql = "SELECT ID FROM servizi_hotel WHERE CODICE='".$codice_column[$i]."'";
    //     $query = mysql_query($sql);
    //     if (mysql_num_rows($query) > 0) {
    //         $id = mysql_fetch_assoc($query)['ID'];
    //         $sql_update = "UPDATE servizi_hotel SET 
    //         IDFORNITORE     = ".$id_fornitore_column[$i].",
    //         IDVENDOR        = '".$vendor_id_column[$i]."',
    //         VENDORNAME      = '".$vendor_name_column[$i]."',
    //         CODICE          = '".$codice_column[$i]."',
    //         NOME            = '".$nome_column[$i]."',
    //         DESCRIZIONE     = '".$descrizione_column[$i]."',
    //         STARRANK        = '".$starrank_column[$i]."',
    //         IDNAZIONE       = '".$id_nazione."',
    //         IDCITTA         = '".$id_citta."',
    //         INDIRIZZO       = '".$indirizzo_1_column[$i]."',
    //         INDIRIZZO2      = '".$indirizzo_2_column[$i]."',
    //         ZIP             = '".$zip_column[$i]."',
    //         PHONE           = '".$phone_column[$i]."',
    //         FAX             = '".$fax_column[$i]."',
    //         MAIL            = '',
    //         IMG             = '".$img_column[$i]."',
    //         LAT             = '".$lat_column[$i]."',
    //         LANG            = '".$lang_column[$i]."',
    //         CHILDAGE        = '".$childage_column[$i]."',
    //         ROOMTYPE        = '".$roomtype_column[$i]."',
    //         MEALPLAN        = '".$mealplan_column[$i]."',
    //         FAMILYPLAN      = '".$familyplan_column[$i]."',
    //         MAXOCC          = '".$maxocc_column[$i]."',
    //         NONRIMBORSABILE = '".$non_rimborsabile_column[$i]."',
    //         FEE             = '".$fee_column[$i]."',
    //         FEETYPE         = '".$fee_type_column[$i]."'
    //         WHERE ID        = ".$id;
            
    //         setMessage('info', 'sql update: '.$sql_update); 
    //         mysql_query($sql_update);
    //         if (mysql_error()) {
    //             setMessage('error', 'Errore aggiornamento del record in servizi hotel: '.mysql_error().'\n
    //                 Query eseguita: '.$sql_update);
    //             return false;
    //         }

    //     } else {
    //         $sql = "INSERT INTO servizi_hotel (ID, IDFORNITORE, IDVENDOR, VENDORNAME, CODICE, NOME, DESCRIZIONE,
    //         STARRANK, IDNAZIONE, IDCITTA, INDIRIZZO, INDIRIZZO2, ZIP, PHONE, FAX, MAIL, IMG, LAT, LANG,
    //         CHILDAGE, ROOMTYPE, MEALPLAN, FAMILYPLAN, MAXOCC, NONRIMBORSABILE, FEE, FEETYPE)
    //         VALUES ( null, ".$id_fornitore_column[$i].", ".$vendor_id_column[$i].", '".$vendor_name_column[$i]."', '".$codice_column[$i]."', 
    //         '".$nome_column[$i]."', '".$descrizione_column[$i]."', '".$starrank_column[$i]."', ".$id_nazione.", ".$id_citta.", 
    //         '".$indirizzo_1_column[$i]."', '".$indirizzo_2_column[$i]."', '".$zip_column[$i]."', '".$phone_column[$i]."', '".$fax_column[$i]."',
    //         '', '".$img_column[$i]."', '".$lat_column[$i]."', '".$lang_column[$i]."', '".$childage_column[$i]."', '".$roomtype_column[$i]."',
    //         '".$mealplan_column[$i]."', '".$familyplan_column[$i]."', '".$maxocc_column[$i]."', '".$non_rimborsabile_column[$i]."', '".$fee_column[$i]."',
    //         '".$fee_type_column[$i]."' )";
    //         $query = mysql_query($sql);

    //         setMessage('info', 'sql insert: '.$sql); 

    //         if (mysql_error()) {
    //             setMessage('error', 'Errore inserimento del record in servizi hotel: '.mysql_error().'\n
    //                 Query eseguita: '.$sql);
    //             return false;
    //         }

    //     }

    // }

    setMessage('success', 'Servizi caricati correttamente');
    setStatusOfRequest(100);
    return true;

}

/**
 * Carica le nazioni, le zone e le citta nel db.
 * Se già presente un record lo aggiorna
 * @param  array &$data i dati letti dal csv
 * @return boolean        se il caricamento è avvenuto o meno
 */
function uploadCitta(&$data)
{
    // Gestione delle colonne delle zone e delle nazioni
    $nazioni_column =filterColumn($data, 2);
    $nazioni_filtered = array_values(array_filter(array_map('trim', array_unique($nazioni_column)))); // Array con valori unici

    $zone_column = filterColumn($data, 3);
    $zone_filtered = array_values(array_filter(array_map('trim', array_unique($zone_column))));

    $cod_citta_column = filterColumn($data, 0);
    $desc_citta_column = filterColumn($data, 1);

    // Se riesce a caricare i valori prosegue

    $upload_citta_ok = uploadNazioniZone($nazioni_filtered, 'nazioni');
    setStatusOfRequest(25);
    $upload_zone_ok  = uploadNazioniZone($zone_filtered, 'zone'); 
    setStatusOfRequest(50);

    if ($upload_citta_ok && $upload_zone_ok) {
        // Inserimento nella tabella citta
        for ($i=0; $i < sizeof($data); $i++) {
            // Recupero id_zona e id_nazione per la riga

            if (abs($i - sizeof($data)/2) < 1) setStatusOfRequest(75);

            $sql_zone = " SELECT ID from zone WHERE CODICE = '".$zone_column[$i]."'";
            $id_zona = mysql_fetch_assoc(mysql_query($sql_zone))['ID'];

            $sql_nazioni = " SELECT ID from nazioni WHERE CODICE = '".$nazioni_column[$i]."'";
            $id_nazione = mysql_fetch_assoc(mysql_query($sql_nazioni))['ID'];

            $id_zona = (empty($id_zona)) ? 'null' : $id_zona;
            $id_nazione = (empty($id_nazione)) ? 'null' : $id_nazione;

            // Controllo presenza di un codice esistente
            $sql = "SELECT ID FROM citta WHERE CODICE = '".$data[$i][0]."'";
            $query = mysql_query($sql);
            if (mysql_num_rows($query) > 0) {
                $id = mysql_fetch_assoc($query)['ID'];
                $sql_update = "UPDATE citta SET 
                CODICE = '".$cod_citta_column[$i]."', 
                DESCRIZIONE ='".$desc_citta_column[$i]."',
                IDNAZIONE = ".$id_nazione.",
                IDZONA = ".$id_zona."
                WHERE ID = ".$id;
                
                mysql_query($sql_update);
                if (mysql_error()) {
                    setMessage('error', 'Errore aggiornamento del record in citta: '.mysql_error());
                    return false;
                }
            } else {
                $sql = "INSERT INTO citta (ID, CODICE, DESCRIZIONE, IDNAZIONE, IDZONA)
                VALUES ( null, '".$cod_citta_column[$i]."', '".$desc_citta_column[$i]."', ".$id_nazione.", ".$id_zona." )";
                $query = mysql_query($sql);
                if (mysql_error()) {
                    setMessage('error', 'Errore inserimento del record in citta: '.mysql_error());
                    return false;
                }
            }
        }
        return true;
    }

    return false;
}

/**
 * Inserisce o aggiorna i file nel database
 * @param  string $type citta, servizi, tariffe
 * @param  file $file file di lettura da caricare
 * @return boolean se tutto è andato a buon fine
 */
function updateDatabase($type, $file)
{
    $header  = array();
    $data    = array();
    $success = false;
    $i       = 0;

    // Lettura dei file con intestazione
    while(!feof($file)) {
        if ($i === 0) {
            $header = fgetcsv($file, 1024);
        }
        $data[] = fgetcsv($file, 1024);
        $i++;
    }

    setMessage('info', 'tipo '.$type);

    switch ($type) {
        case 'citta':
            // Controllo minimamente l'intestazione per vedere se è corretta
            if (sizeof($header) !== 4) {
                setMessage('error', 'Il file caricato non ha un\' intestazione corretta');
                return $success;
            }
            $success = uploadCitta($data);
            break;

        case 'servizi':
            // Controllo sull'intestazion
            if (sizeof($header) !== 27) {
                setMessage('error', 'Il file caricato non ha un\' intestazione corretta');
                return $success;
            }
            setMessage('info', 'Selezionati  servizi');
            $success = uploadServizi($data);
    
        default:
            setMessage('error', 'Non è stato selezionato un tipo valido');
            setStatusOfRequest(0);
            $success = false;
            break;
    }

    return $success;

}

/**
 * Carica il file e restituisce un messaggio di tirono
 * 
 */
function uploadFile()
{
    $response = null;
    if (isset($_POST) && !db_is_null($_POST)) {
        $file_name = basename($_FILES['file']['name']);
        $file_extension = pathinfo($file_name, PATHINFO_EXTENSION);
        //Validazione del tipo di estensione
        if ($file_extension != 'txt' && $file_extension != 'csv') {
            setMessage('error', 'Non e\' stato selezionato alcun file o il file caricato non ha un\' estensione valida');
        }

        if ($_FILES['file']['error'] == UPLOAD_ERR_OK && is_uploaded_file($_FILES['file']['tmp_name'])) {
            $file    = fopen($_FILES['file']['tmp_name'], 'r');
            $success = updateDatabase($_POST['tipo'], $file);
            fclose($file);

            if ($success) {
                setMessage('info', 'Valori inseriti correttamente');
                setStatusOfRequest(0);
            }
        }
    } else {
        // Non è stata inviata una request corretta
        setMessage('error', 'Non è stata inviata una richiesta corretta al server');
    }
}

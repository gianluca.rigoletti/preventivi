<?php

require '../Librerie/connect.php';

$Titolo = "Annullamento Amministratore";
$Tavola= "amministratori";

$indietro = "vis_amministratori.php";

if (isset($_GET['id'])) {
    db_delete($Tavola, $_GET['id']);
    header("Location: $indietro");
    exit;
}
header("Location: $indietro");
exit;

<?php

require '../Librerie/connect.php';

$Titolo = "Annullamento Dettaglio Ordine";
$Tavola= "ordine_dett";

$indietro = "vis_dettaglio_ordini.php?p_id=".$_GET['id_padre'];

if (isset($_GET['id'])) {
    db_delete($Tavola, $_GET['id']);
    header("Location: $indietro");
    exit;
}
header("Location: $indietro");
exit;

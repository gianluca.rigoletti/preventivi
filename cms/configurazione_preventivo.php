<?php
$m="visualizzazione_preventivo";

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/configurazione.php';
require '../Librerie/ges_html_top.php';
require 'utilities_preventivo.php';

$post_data = parse_post_data($_POST);
$dates = $data['reservation'];
$dates_and_days = split_into_pages($dates);
$page_number = 0;


if (!isset($_GET['page'])) {
    echo "
    <script> window.location.href = 'vis_preventivo.php'; </script>
    ";
} else {
    $page_number = $_GET['page'];
}

$current_date = (!empty($dates_and_days[$page])) ? $dates_and_days[$page] : '' ;

?>

<div class="x_panel col-md-12">
    <div class="x_title">
        <h1> Gestione preventivo </h1>
    </div>
    <div class="x_content">
        <div class="well">
            <form class="form-horizontal find-hotel" method="GET" action="">
                <label for="reservation"> Date partenza e arrivo</label>
                <fieldset>
                    <div class="control-group" style="display:table; margin:0 auto;">
                        <div class="controls">
                            <div class="col-md-11 xdisplay_inputx form-group has-feedback">
                                                            <input type="text" class="form-control has-feedback-left active" id="date" placeholder="First Name" aria-describedby="inputSuccess2Status">
                                                            <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                                                            <span id="inputSuccess2Status" class="sr-only">(success)</span>
                                                          </div>
                        </div>
                    </div>
                </fieldset>
                <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Città
                </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <input type="text" name="citta" id="citta" required="required" class="form-control col-md-7 col-xs-12" value="<?php if (isset($citta_id)) echo $citta_id; ?>">
                    </div>
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary btn-center" id="cerca-citta">Cerca</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="x_panel">
    <div class="x_title">
        <h3> {{ Hotel }} </h3>
    </div>
    <div class="x_content">
        <div class="col-md-12">
            <h3>Quadrupla</h3>
            <div class="col-md-2"> Prezzo 1</div>
            <div class="radio">
                <label class="">
                    <div class="iradio_flat-green checked" style="position: relative;"><input type="radio" class="flat" checked="" name="iCheck-1" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> Checked
                </label>
            </div>
            <div class="col-md-2"> Prezzo 1</div>
            <div class="radio">
                <label class="">
                    <div class="iradio_flat-green checked" style="position: relative;"><input type="radio" class="flat" checked="" name="iCheck-1" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> Checked
                </label>
            </div>
        </div>
        <div class="col-md-12">
            <h3>Quadrupla</h3>
            <div class="col-md-2"> Prezzo 1</div>
            <div class="radio">
                <label class="">
                    <div class="iradio_flat-green checked" style="position: relative;"><input type="radio" class="flat" checked="" name="iCheck-1" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> Checked
                </label>
            </div>
            <div class="col-md-2"> Prezzo 1</div>
            <div class="radio">
                <label class="">
                    <div class="iradio_flat-green checked" style="position: relative;"><input type="radio" class="flat" checked="" name="iCheck-1" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> Checked
                </label>
            </div>
        </div>
    </div>
</div>
<div class="x_panel">
    <div class="x_title">
        <h3> {{ Hotel }} </h3>
    </div>
    <div class="x_content">
        <div class="col-md-12">
            <h3>Quadrupla</h3>
            <div class="col-md-2"> Prezzo 1</div>
            <div class="radio">
                <label class="">
                    <div class="iradio_flat-green checked" style="position: relative;"><input type="radio" class="flat" checked="" name="iCheck-1" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> Checked
                </label>
            </div>
            <div class="col-md-2"> Prezzo 1</div>
            <div class="radio">
                <label class="">
                    <div class="iradio_flat-green checked" style="position: relative;"><input type="radio" class="flat" checked="" name="iCheck-1" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> Checked
                </label>
            </div>
        </div>
        <div class="col-md-12">
            <h3>Quadrupla</h3>
            <div class="col-md-2"> Prezzo 1</div>
            <div class="radio">
                <label class="">
                    <div class="iradio_flat-green checked" style="position: relative;"><input type="radio" class="flat" checked="" name="iCheck-1" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> Checked
                </label>
            </div>
            <div class="col-md-2"> Prezzo 1</div>
            <div class="radio">
                <label class="">
                    <div class="iradio_flat-green checked" style="position: relative;"><input type="radio" class="flat" checked="" name="iCheck-1" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div> Checked
                </label>
            </div>
        </div>
    </div>
</div>
<div class="x_panel text-center">
    <button class="btn btn-success btn-center">Giorno successivo</button>
</div>


<script>
    $(document).ready(function() {
        var current_date = <?php echo $current_date; ?>
        console.log(current_date);
        $('#date').daterangepicker({
                format: 'dd/mm/yyyy',
                startDate: current_date,
                endDate: current_date,
                calender_style: "picker_1",
                singleDatePicker: true
            }, function(start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });

        $('#cerca-citta').click(function (event) {
            event.preventDefault();
            var citta = $('#citta').val();
            var data = {
                    info: <?php echo json_encode($post_data); ?>,
                    action: 'search',
                    citta: citta,
                    current_date: current_date
                }

            console.log(data);
            $.ajax({
                url: 'controller_preventivo.php',
                method: 'POST',
                data: {
                    info: <?php echo json_encode($post_data); ?>,
                    action: 'search',
                    citta: citta,
                    current_date: current_date
                }
            });
        });
    });
</script>
<?php
$m = 'pax';

require '../Librerie/connect.php';
require '../Librerie/html.php';
require '../Librerie/files.php';
require '../Librerie/configurazione.php';

$c_files = new files();
$tavola = 'pax';
$indietro = 'vis_pax.php';

if (isset($_GET['back'])) {
    $indietro = $_GET['back'].'.php';
}
if ($_GET['p_upd'] == 1) {
    $funzione = 'Update';
    $disabilita_chiave = 'disabled';
    $titolo = 'Modifica Fornitore';
} else {
    $funzione = 'Insert';
    $titolo = 'Nuova Fornitore';
}
if ($_GET['p_upd'] == 1) {
    $risultato = db_query_mod($tavola, $_GET['p_id']);
    $cur_rec = mysql_fetch_assoc($risultato);
}
if (isset($_POST['Insert']) || isset($_POST['Update'])) {
    $cur_rec['ID']        = $_POST['ID'];
    $cur_rec['NOME']      = $_POST['NOME'];
    $cur_rec['COGNOME']   = $_POST['COGNOME'];
    $cur_rec['ETA']       = $_POST['ETA'];
    $cur_rec['TIPOLOGIA'] = $_POST['TIPOLOGIA'];
    $cur_rec['EMAIL']     = $_POST['EMAIL'];
    $cur_rec['TELEFONO']  = $_POST['TELEFONO'];
    $cur_rec['INDIRIZZO'] = $_POST['INDIRIZZO'];
    $cur_rec['CITTA']     = $_POST['CITTA'];
    $cur_rec['CAP']       = $_POST['CAP'];
    $cur_rec['PROVINCIA'] = $_POST['PROVINCIA'];

    if (isset($_POST['NOME']) && ($_POST['NOME'] == null || $_POST['NOME'] == " ")) {
        $c_err->add("Campo Codice NOME Obbligatorio", "NOME");
    }
    if (isset($_POST['COGNOME']) && ($_POST['COGNOME'] == null || $_POST['COGNOME'] == " ")) {
        $c_err->add("Campo Codice COGNOME Obbligatorio", "COGNOME");
    }
    if (isset($_POST['ETA']) && ($_POST['ETA'] == null || $_POST['ETA'] == " ")) {
        $c_err->add("Campo Codice ETA Obbligatorio", "ETA");
    }
    if (isset($_POST['TIPOLOGIA']) && ($_POST['TIPOLOGIA'] == null || $_POST['TIPOLOGIA'] == " ")) {
        $c_err->add("Campo Codice TIPOLOGIA Obbligatorio", "TIPOLOGIA");
    }
    if (isset($_POST['EMAIL']) && ($_POST['EMAIL'] == null || $_POST['EMAIL'] == " ")) {
        $c_err->add("Campo Codice EMAIL Obbligatorio", "EMAIL");
    }
    if (isset($_POST['TELEFONO']) && ($_POST['TELEFONO'] == null || $_POST['TELEFONO'] == " ")) {
        $c_err->add("Campo Codice TELEFONO Obbligatorio", "TELEFONO");
    }
    if (isset($_POST['INDIRIZZO']) && ($_POST['INDIRIZZO'] == null || $_POST['INDIRIZZO'] == " ")) {
        $c_err->add("Campo Codice INDIRIZZO Obbligatorio", "INDIRIZZO");
    }
    if (isset($_POST['CITTA']) && ($_POST['CITTA'] == null || $_POST['CITTA'] == " ")) {
        $c_err->add("Campo Codice CITTA Obbligatorio", "CITTA");
    }
    if (isset($_POST['CAP']) && ($_POST['CAP'] == null || $_POST['CAP'] == " ")) {
        $c_err->add("Campo Codice CAP Obbligatorio", "CAP");
    }
    if (isset($_POST['PROVINCIA']) && ($_POST['PROVINCIA'] == null || $_POST['PROVINCIA'] == " ")) {
        $c_err->add("Campo Codice PROVINCIA Obbligatorio", "PROVINCIA");
    }

    if (isset($_POST['Insert']) && db_dup_key($tavola, $_POST) > 0) {
         $c_err->add("Fornitore Gi&agrave; Codificato", "ID");
    }
    if (!$c_err->is_errore()) {
        if (isset($_POST['Insert'])) {
            db_insert($tavola, $_POST);
        } else {
            db_update($tavola, $_POST['ID'], $_POST);
        }
        header('Location: vis_pax.php');
        exit;
    }
}
if (isset($_POST['Return'])) {
    header('Location: '.$indietro);
    exit;
}

require '../Librerie/ges_html_top.php';
$c_err->mostra();
?>

<form id="formG" class="form-horizontal form-label-left"  action="" method="post" novalidate>
<input type="hidden" name="ID" value="<?php if (isset($cur_rec)) echo $cur_rec['ID']; ?>" >

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Nome <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("NOME");?> name="NOME"  id="NOME" value="<?php if (isset($cur_rec)) echo $cur_rec['NOME'];?>" size="55" maxlength="200"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Cognome <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("COGNOME");?> name="COGNOME"  id="COGNOME" value="<?php if (isset($cur_rec)) echo $cur_rec['COGNOME'];?>" size="55" maxlength="200"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Età <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="number" required="true" min="1" max="150" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("ETA");?> name="ETA"  id="ETA" value="<?php if (isset($cur_rec)) echo $cur_rec['ETA'];?>" size="55" maxlength="20"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> Tipologia <span class="required">*</span><i class="fa fa-question" data-toggle="tooltip" data-placement="bottom" title="Tipologia"></i>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <select id="TIPOLOGIA" required="true" name="TIPOLOGIA" required="true" class="form-control col-md-7 col-xs-12"/>
        <?php
            $valore = 0;
            if (isset($cur_rec['TIPOLOGIA'])) $valore = $cur_rec['TIPOLOGIA'];
            $valori = array(
                '0' => '',
                '1' => 'Infant',
                '2' => 'Child'
            );
            foreach ($valori as $key => $value) {
                $selected = "";
                if ($key == $valore) $selected = "selected=\"selected\"";
                echo "<option value=\"$key\" $selected>$value</option>";
            }
        ?>
    </select>
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Email <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="email" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("EMAIL");?> name="EMAIL"  id="EMAIL" value="<?php if (isset($cur_rec)) echo $cur_rec['EMAIL'];?>" size="55" ><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Telefono <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="number" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("TELEFONO");?> name="TELEFONO"  id="TELEFONO" value="<?php if (isset($cur_rec)) echo $cur_rec['TELEFONO'];?>" size="55" maxlength="200"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Indirizzo <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("INDIRIZZO");?> name="INDIRIZZO"  id="INDIRIZZO" value="<?php if (isset($cur_rec)) echo $cur_rec['INDIRIZZO'];?>" size="55" maxlength="255"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Città <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("CITTA");?> name="CITTA"  id="CITTA" value="<?php if (isset($cur_rec)) echo $cur_rec['CITTA'];?>" size="55" maxlength="255"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">CAP <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="number" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("CAP");?> name="CAP"  id="CAP" value="<?php if (isset($cur_rec)) echo $cur_rec['CAP'];?>" size="55" maxlength="5" minlength="5"><br />
  </div>
</div>

<div class="item form-group">
  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="">Provincia <span class="required">*</span>
  </label>
  <div class="col-md-6 col-sm-6 col-xs-12">
    <input type="text" required="true" class="form-control col-md-7 col-xs-12" <?php $c_err->tooltip("PROVINCIA");?> name="PROVINCIA"  id="PROVINCIA" value="<?php if (isset($cur_rec)) echo $cur_rec['PROVINCIA'];?>" size="55" maxlength="2"><br />
  </div>
</div>
                        
<div class="ln_solid"></div>
  <div class="form-group">
    <div class="col-md-6 col-md-offset-3">
      <button class="cancel btn btn-primary" type="submit" name="Return" value="Return">Indietro</button>
      <button type="submit"  class="btn btn-success" name="<?php echo $funzione ?>" value="Salva">Salva</button>
  </div>
</div>
</form>
<?php require '../Librerie/ges_html_bot.php'; ?>

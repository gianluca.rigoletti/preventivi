-- phpMyAdmin SQL Dump
-- version 4.6.0
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Creato il: Ott 03, 2016 alle 01:26
-- Versione del server: 5.5.49
-- Versione PHP: 5.5.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `preventivi`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `amministratori`
--

CREATE TABLE `amministratori` (
  `ID` int(11) NOT NULL,
  `UTENTE` varchar(200) COLLATE utf8_bin NOT NULL,
  `PASSWORD` varchar(2000) COLLATE utf8_bin NOT NULL,
  `NOME` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DATAINS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `LIVELLO` int(11) NOT NULL DEFAULT '1',
  `MAIL` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `NOTIFICAOPE` int(1) NOT NULL DEFAULT '1',
  `NOTIFICASCA` int(1) NOT NULL DEFAULT '1',
  `REMINDERSCA` int(1) NOT NULL DEFAULT '1',
  `DEFAULTREMINDERSCA` int(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `citta`
--

CREATE TABLE `citta` (
  `ID` int(11) NOT NULL,
  `CODICE` varchar(20) COLLATE utf8_bin NOT NULL,
  `DESCRIZIONE` varchar(200) COLLATE utf8_bin NOT NULL,
  `IDNAZIONE` int(11) DEFAULT NULL,
  `IDZONA` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `fornitori`
--

CREATE TABLE `fornitori` (
  `ID` int(11) NOT NULL,
  `CODICE` varchar(255) COLLATE utf8_bin NOT NULL,
  `DESCRIZIONE` varchar(200) COLLATE utf8_bin NOT NULL,
  `IDTIPOSERVIZIO` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `nazioni`
--

CREATE TABLE `nazioni` (
  `ID` int(11) NOT NULL,
  `CODICE` varchar(20) COLLATE utf8_bin NOT NULL,
  `DESCRIZIONE` varchar(200) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine`
--

CREATE TABLE `ordine` (
  `ID` int(11) NOT NULL,
  `DESCRIZIONE` varchar(255) COLLATE utf8_bin NOT NULL,
  `DATAIN` date NOT NULL,
  `DATAOUT` date NOT NULL,
  `NUMEROPAX` int(1) NOT NULL,
  `NOME_INT` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MAIL_INT` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TEL_INT` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MARKUP` double(10,2) DEFAULT NULL,
  `MARKUPTYPE` int(1) NOT NULL DEFAULT '1' COMMENT '1 % , 2 VALORE ASSOLUTO'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine_camere`
--

CREATE TABLE `ordine_camere` (
  `ID` int(11) NOT NULL,
  `IDORDINE` int(11) NOT NULL,
  `TIPO` int(11) NOT NULL,
  `CHILDAGE` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine_dett`
--

CREATE TABLE `ordine_dett` (
  `ID` int(11) NOT NULL,
  `IDORDINE` int(11) NOT NULL,
  `IDTIPO_SERVIZIO` int(11) NOT NULL,
  `IDSERVIZIO` int(11) NOT NULL,
  `IDTARIFFA` int(11) DEFAULT NULL,
  `DESCRIZIONE` varchar(2000) COLLATE utf8_bin DEFAULT NULL,
  `BEGIN` date NOT NULL,
  `END` date NOT NULL,
  `PREZZO` double(10,2) NOT NULL,
  `TIPO_SISTEMAZIONE` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `PAX_SISTEMATI` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine_pax`
--

CREATE TABLE `ordine_pax` (
  `ID` int(11) NOT NULL,
  `IDORDINE` int(11) NOT NULL,
  `IDPAX` int(11) NOT NULL,
  `INTESTATARIO` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `pax`
--

CREATE TABLE `pax` (
  `ID` int(11) NOT NULL,
  `NOME` varchar(200) COLLATE utf8_bin NOT NULL,
  `COGNOME` varchar(200) COLLATE utf8_bin NOT NULL,
  `ETA` int(3) NOT NULL,
  `TIPOLOGIA` int(1) NOT NULL COMMENT 'Child - infant - null',
  `EMAIL` varchar(255) COLLATE utf8_bin NOT NULL,
  `TELEFONO` varchar(255) COLLATE utf8_bin NOT NULL,
  `INDIRIZZO` varchar(255) COLLATE utf8_bin NOT NULL,
  `CITTA` varchar(255) COLLATE utf8_bin NOT NULL,
  `CAP` varchar(5) COLLATE utf8_bin NOT NULL,
  `PROVINCIA` varchar(2) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `servizi_hotel`
--

CREATE TABLE `servizi_hotel` (
  `ID` int(11) NOT NULL,
  `IDFORNITORE` int(11) NOT NULL,
  `IDVENDOR` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `VENDORNAME` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `CODICE` varchar(50) COLLATE utf8_bin NOT NULL,
  `NOME` varchar(200) COLLATE utf8_bin NOT NULL,
  `DESCRIZIONE` varchar(2000) COLLATE utf8_bin DEFAULT NULL,
  `STARRANK` int(1) DEFAULT NULL,
  `IDNAZIONE` int(11) NOT NULL,
  `IDCITTA` int(11) NOT NULL,
  `INDIRIZZO` varchar(255) COLLATE utf8_bin NOT NULL,
  `INDIRIZZO2` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ZIP` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PHONE` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `FAX` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MAIL` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `IMG` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LAT` float(10,7) DEFAULT NULL,
  `LANG` float(10,7) DEFAULT NULL,
  `CHILDAGE` int(2) DEFAULT NULL,
  `ROOMTYPE` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MEALPLAN` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `FAMILYPLAN` int(1) DEFAULT '0' COMMENT '0 non applicabile, 1 applicabile',
  `MAXOCC` int(1) NOT NULL DEFAULT '9' COMMENT 'numero max pax per stanza',
  `NONRIMBORSABILE` int(1) NOT NULL DEFAULT '0',
  `FEE` double(7,2) DEFAULT NULL,
  `FEETYPE` int(1) NOT NULL COMMENT '0 included,1 per day,2 per person,3 per room,4 +tax per day,5 +tax per person,6 +tax per room,9 omaggiata'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `servizi_hotel_tariffe`
--

CREATE TABLE `servizi_hotel_tariffe` (
  `ID` int(11) NOT NULL,
  `IDHOTEL` int(11) NOT NULL,
  `CHILDAGE` int(2) DEFAULT NULL,
  `FAMILYPLAN` int(1) DEFAULT NULL,
  `DATABEGIN` date NOT NULL,
  `DATAEND` date NOT NULL,
  `BOOKINGBEGIN` date DEFAULT NULL,
  `BOOKINGEND` date DEFAULT NULL,
  `DAYAPPL` varchar(200) COLLATE utf8_bin DEFAULT NULL COMMENT 'GIORNI IN CUI SI APPLCIA LA TARIFFA DIVISI DA ; (0 DOMENICA,1 LUNEDì ECC...)',
  `DESCRIZIONE` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `CANCPOLICY` varchar(200) COLLATE utf8_bin DEFAULT NULL,
  `PREZZO_1` double(10,2) NOT NULL,
  `PREZZO_2` double(10,2) NOT NULL,
  `PREZZO_3` double(10,2) NOT NULL,
  `PREZZO_4` double(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `servizi_liberi`
--

CREATE TABLE `servizi_liberi` (
  `ID` int(11) NOT NULL,
  `IDTIPOSERVIZIO` int(11) NOT NULL,
  `DESCRIZIONE` varchar(255) NOT NULL,
  `PREZZO` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `tipi_servizi`
--

CREATE TABLE `tipi_servizi` (
  `ID` int(11) NOT NULL,
  `CODICE` varchar(10) COLLATE utf8_bin NOT NULL,
  `DESCRIZIONE` varchar(200) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struttura della tabella `zone`
--

CREATE TABLE `zone` (
  `ID` int(11) NOT NULL,
  `CODICE` varchar(50) COLLATE utf8_bin NOT NULL,
  `DESCRIZIONE` varchar(200) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `amministratori`
--
ALTER TABLE `amministratori`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `EMAIL` (`UTENTE`);

--
-- Indici per le tabelle `citta`
--
ALTER TABLE `citta`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `CODICE` (`CODICE`);

--
-- Indici per le tabelle `fornitori`
--
ALTER TABLE `fornitori`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `CODICE` (`CODICE`);

--
-- Indici per le tabelle `nazioni`
--
ALTER TABLE `nazioni`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `CODICE` (`CODICE`);

--
-- Indici per le tabelle `ordine`
--
ALTER TABLE `ordine`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `ordine_camere`
--
ALTER TABLE `ordine_camere`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `ordine_dett`
--
ALTER TABLE `ordine_dett`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `ordine_pax`
--
ALTER TABLE `ordine_pax`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `pax`
--
ALTER TABLE `pax`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `servizi_hotel`
--
ALTER TABLE `servizi_hotel`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `CODICE` (`CODICE`);

--
-- Indici per le tabelle `servizi_hotel_tariffe`
--
ALTER TABLE `servizi_hotel_tariffe`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `servizi_liberi`
--
ALTER TABLE `servizi_liberi`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `tipi_servizi`
--
ALTER TABLE `tipi_servizi`
  ADD PRIMARY KEY (`ID`);

--
-- Indici per le tabelle `zone`
--
ALTER TABLE `zone`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `CODICE` (`CODICE`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `amministratori`
--
ALTER TABLE `amministratori`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la tabella `citta`
--
ALTER TABLE `citta`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=915;
--
-- AUTO_INCREMENT per la tabella `fornitori`
--
ALTER TABLE `fornitori`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18705;
--
-- AUTO_INCREMENT per la tabella `nazioni`
--
ALTER TABLE `nazioni`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22075;
--
-- AUTO_INCREMENT per la tabella `ordine`
--
ALTER TABLE `ordine`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT per la tabella `ordine_camere`
--
ALTER TABLE `ordine_camere`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;
--
-- AUTO_INCREMENT per la tabella `ordine_dett`
--
ALTER TABLE `ordine_dett`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT per la tabella `ordine_pax`
--
ALTER TABLE `ordine_pax`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `pax`
--
ALTER TABLE `pax`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `servizi_hotel`
--
ALTER TABLE `servizi_hotel`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24671;
--
-- AUTO_INCREMENT per la tabella `servizi_hotel_tariffe`
--
ALTER TABLE `servizi_hotel_tariffe`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98601;
--
-- AUTO_INCREMENT per la tabella `servizi_liberi`
--
ALTER TABLE `servizi_liberi`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `tipi_servizi`
--
ALTER TABLE `tipi_servizi`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `zone`
--
ALTER TABLE `zone`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=546;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
